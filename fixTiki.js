"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const mongodb_1 = require("mongodb");
const _ = require("lodash");
function run() {
    return __awaiter(this, void 0, void 0, function* () {
        var db = yield mongodb_1.MongoClient.connect("mongodb://tranduytrung:sbcbE045d17@ds011168.mlab.com:11168/storehub");
        var collection = db.collection("products");
        var query = {
            source: "tiki",
            url: /^https/i
        };
        var httpsProducts = yield collection.find(query).toArray();
        for (var index = 0; index < httpsProducts.length; index++) {
            var httpsProduct = httpsProducts[index];
            var result = yield collection.findOneAndDelete({ url: "http" + httpsProduct.url.substring(5) });
            if (!result.value)
                continue;
            var httpProduct = result.value;
            console.log(httpProduct.name);
            var prices = _.orderBy(httpsProduct.prices.concat(httpProduct.prices), ["date"], ["desc"]);
            collection.updateOne({ _id: httpsProduct["_id"] }, { $set: { prices: prices } });
        }
    });
}
run().then(function () {
    console.log("done");
}).catch(function (e) {
    console.error(e);
});
//# sourceMappingURL=fixTiki.js.map