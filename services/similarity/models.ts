import {IProduct} from "../../models/product"

export interface ISimilarityArgument {
    distance: number,
    totalWeight: number,
    product1 : IProduct,
    product2 : IProduct,
    normalizedName1 : string,
    normalizedName2 : string,
    tokens1: ArrayLike<string>,
    tokens2: ArrayLike<string>
}

export interface ISimilarityPipelineFunction {
    (arg : ISimilarityArgument, options : {})
}