"use strict";
var pipelineFunction = function (arg) {
    arg.normalizedName1 = arg.product1.name.toLowerCase();
    arg.normalizedName2 = arg.product2.name.toLowerCase();
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = pipelineFunction;
//# sourceMappingURL=lowerCase.js.map