import {ISimilarityArgument, ISimilarityPipelineFunction} from "../../models";
import {IWeightRule, IWeightRuleArgument, IWeightTester, IToken} from "./models";
import brand from "./rules/brand"
import model from "./rules/model"
import configuration from "./rules/configuration"

var pipelineFunction : ISimilarityPipelineFunction = function(arg, options?) {
    var rules = (options && <IWeightRule[]>options["rules"]) || defaultRules;
    
    var arg1: IWeightRuleArgument = {
        product: arg.product1,
        recognizedTokens: [],
        tokens: arg.tokens1,
        normalizedName: arg.normalizedName1
    };

    var arg2: IWeightRuleArgument = {
        product: arg.product2,
        recognizedTokens: [],
        tokens: arg.tokens2,
        normalizedName: arg.normalizedName2
    };

    for (var index = 0; index < rules.length; index++) {
        var rule = rules[index];
        
        arg.distance += rule.test(arg1, arg2);        
        arg.totalWeight += rule.weight;
    }
}

var defaultRules = [ brand, configuration, model ];

export default pipelineFunction;