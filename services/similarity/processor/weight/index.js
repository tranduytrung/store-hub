"use strict";
const brand_1 = require("./rules/brand");
const model_1 = require("./rules/model");
const configuration_1 = require("./rules/configuration");
var pipelineFunction = function (arg, options) {
    var rules = (options && options["rules"]) || defaultRules;
    var arg1 = {
        product: arg.product1,
        recognizedTokens: [],
        tokens: arg.tokens1,
        normalizedName: arg.normalizedName1
    };
    var arg2 = {
        product: arg.product2,
        recognizedTokens: [],
        tokens: arg.tokens2,
        normalizedName: arg.normalizedName2
    };
    for (var index = 0; index < rules.length; index++) {
        var rule = rules[index];
        arg.distance += rule.test(arg1, arg2);
        arg.totalWeight += rule.weight;
    }
};
var defaultRules = [brand_1.default, configuration_1.default, model_1.default];
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = pipelineFunction;
//# sourceMappingURL=index.js.map