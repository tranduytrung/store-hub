import {IProduct} from "../../../../models/product"

export interface IToken {
    name: string
    value: string
}

export interface IWeightRuleArgument {
    product : IProduct
    normalizedName : string
    recognizedTokens : IToken[]
    tokens : ArrayLike<string>
}

export interface IWeightTester {
    (arg1 : IWeightRuleArgument, arg2 : IWeightRuleArgument) : number
}

export interface IWeightRule {
    test:  IWeightTester
    weight: number
}