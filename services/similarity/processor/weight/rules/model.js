"use strict";
function getCandidateModels(arg) {
    var str = arg.normalizedName;
    // remove all regcognized token since it should not be model code
    for (var index = 0; index < arg.recognizedTokens.length; index++) {
        var token = arg.recognizedTokens[index];
        str = str.replace(token.value, "");
    }
    // remove redundant space and after -
    str = str.replace(/\s-\s.+/, "").replace(/\s+/g, " ");
    var models = [];
    for (var index = 0; index < modelRegexes.length; index++) {
        var regexInfo = modelRegexes[index];
        var matchResult = str.match(regexInfo.regex);
        if (matchResult !== null) {
            var model = matchResult[regexInfo.group];
            models.push(model);
            arg.recognizedTokens.push({
                name: "model",
                value: model
            });
        }
    }
    return models;
}
var rule = {
    weight: 9,
    test: function (arg1, arg2) {
        var models1 = getCandidateModels(arg1);
        var models2 = getCandidateModels(arg2);
        for (let i = 0; i < models1.length; i++) {
            for (let j = 0; j < models2.length; j++) {
                if (models1[i] === models2[j]) {
                    return 0;
                }
            }
        }
        return 9;
    }
};
var modelRegexes = [
    {
        regex: /( |^)([a-z]+( [a-z0-9]+){1,3})( |$)/,
        group: 2
    },
    {
        regex: /( |^)(([a-z]+ )?([a-z0-9]+ )?[a-z0-9]+)( |$)/,
        group: 2
    }
];
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = rule;
//# sourceMappingURL=model.js.map