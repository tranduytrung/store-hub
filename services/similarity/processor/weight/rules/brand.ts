import {IWeightRule, IWeightRuleArgument} from "../models"

function getBrand(arg : IWeightRuleArgument): string {
    for (var index = 0; index < arg.tokens.length; index++) {
        var token = arg.tokens[index];
        if (knownBrands[token]) {
            arg.recognizedTokens.push({
                name: "brand",
                value: token
            });
            return token;
        }
    }

    return null;
}

var rule: IWeightRule = {
    weight: 1,
    test: function (arg1, arg2) {
        var brand1 = getBrand(arg1);
        var brand2 = getBrand(arg2);

        return brand1 === brand2? 0 : 1;
    }
}

var knownBrands: { [brand: string]: boolean } = {
    "apple": true,
    "samsung": true,
    "oppo": true,
    "asus": true,
    "microsoft": true,
    "hp": true,
    "mobell": true,
    "acer": true,
    "dell": true,
    "htc": true,
    "lenovo": true,
    "sony": true,
    "lg": true,
    "zte": true,
    "huawei": true,
    "mobiistar": true,
    "motorola": true
}

export default rule;