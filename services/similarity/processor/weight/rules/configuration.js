"use strict";
var metricRegexs = [
    {
        regex: /\b[0-9]+(\.[0-9])?(gb|inch|"|mp)\b/g,
        group: 0
    },
    {
        regex: /\b(3g|4g|wifi|lte)\b/g,
        group: 0
    }
];
function getConfigurations(arg) {
    var str = arg.normalizedName;
    for (var index = 0; index < metricRegexs.length; index++) {
        var regexInfo = metricRegexs[index];
        var regex = new RegExp(regexInfo.regex.source, regexInfo.regex.flags);
        var matchResult;
        while ((matchResult = regex.exec(str)) !== null) {
            arg.recognizedTokens.push({
                name: "configuration",
                value: matchResult[regexInfo.group]
            });
        }
    }
}
var rule = {
    weight: 0,
    test: function (arg1, arg2) {
        getConfigurations(arg1);
        getConfigurations(arg2);
        return 0;
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = rule;
//# sourceMappingURL=configuration.js.map