"use strict";
var pipelineFunction = function (arg) {
    arg.tokens1 = arg.normalizedName1.split(/\s+/g);
    arg.tokens2 = arg.normalizedName2.split(/\s+/g);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = pipelineFunction;
//# sourceMappingURL=tokenize.js.map