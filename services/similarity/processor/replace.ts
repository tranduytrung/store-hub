import {ISimilarityPipelineFunction, ISimilarityArgument} from "../models.ts"

var pipelineFunction: ISimilarityPipelineFunction = function (arg, options?) {
    var rules = (options && <IReplaceRule[]>options["rules"]) || defaultRules;

    arg.normalizedName1 = replace(arg.normalizedName1, rules);
    arg.normalizedName2 = replace(arg.normalizedName2, rules);
}

export default pipelineFunction;

interface IReplaceRule {
    pattern: RegExp,
    value: string
}

function replace(str: string, rules: IReplaceRule[]) {
    for (var index = 0; index < rules.length; index++) {
        var rule = rules[index];
        str = str.replace(rule.pattern, rule.value);
    }

    return str;
}

export var defaultRules = [
    {
        pattern: /wi-fi/,
        value: "wifi"
    },
    {
        pattern: /laptop|dtdd|ram/,
        value: ""
    },
    {
        // parenthesis and text inside
        pattern: /\(.*?\)/,
        value: ""
    },
    {
        pattern: /[+-\/]/,
        value: " "
    },
    {
        pattern: /(\d|dual) sim( (dual|\d) (sóng|standby))?/,
        value: ""
    }
];