
// Damerau-Levenshtein
export function distance(tokens1 : ArrayLike<string>, tokens2 : ArrayLike<string>) {
    // length of input
    var n1 = tokens1.length;
    var n2 = tokens2.length;
    
    // maximum posible edit distance could be.
    // it is max(n1, n2) but n1 + n2 is better performance 
    var max = n1 + n2;
    // init array for recording results
    var r0 = Array(n2 + 1), r1 = Array(n2 + 1), r2 = Array(n2 + 1);
    r0.fill(max, 0, n2);
    for (var i = 0; i <= n2; i++) {
        r1[i] = i;        
    }
    r0[-1] = max;
    r1[-1] = max;
    r2[-1] = max;

    // follow the equation of Damerau-Levenshtein
    for (var i = 0; i < n1; i++) {
        r2[0] = i + 1;
        for (var j = 0; j < n2; j++) {
            var subCost = tokens1[i] === tokens2[j] ? 0 : 1;
            var tranCost = tokens1[i] === tokens2[j - 1] && tokens1[i - 1] === tokens2[j];
            r2[j + 1] = Math.min(r2[j] + 1, r1[j + 1] + 1, r1[j] + subCost, tranCost? r0[j - 1] + 1 : max); 
        }
        
        var t = r0;
        r0 = r1;
        r1 = r2;
        r2 = t;
    }
    
    return r1[n2];
}