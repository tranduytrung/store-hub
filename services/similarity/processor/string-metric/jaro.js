"use strict";
function similarityOf(tokens1, tokens2) {
    var n1 = tokens1.length;
    var n2 = tokens2.length;
    // empty
    if (!(n1 | n2)) {
        return 1;
    }
    // one of them is empty
    if (n1 === 0 || n2 === 0) {
        return 0;
    }
    var matchingRange = ~~(Math.max(n1, n2) / 2 - 1);
    var match1 = Array(n1);
    var match2 = Array(n2);
    var nMatches = 0, nTransponsitions = 0;
    // find matches
    for (var i = 0; i < n1; i++) {
        // setup search range
        var jStart = i - matchingRange;
        jStart = jStart < 0 ? 0 : jStart;
        var jEnd = i + matchingRange + 1;
        jEnd = jEnd > n2 ? n2 : jEnd;
        for (var j = jStart; j < jEnd; j++) {
            // already matched
            if (match2[j])
                continue;
            // not match
            if (tokens1[i] !== tokens2[j])
                continue;
            // match!
            match1[i] = true;
            match2[j] = true;
            nMatches++;
            break;
        }
    }
    // no matches
    if (nMatches === 0) {
        return 0;
    }
    // find transpositions
    var j = 0;
    for (var i = 0; i < n1; i++) {
        // no match
        if (!match1[i])
            continue;
        // find coressponding match
        while (!match2[j])
            j++;
        // if the matched token is not same as the other, it is a transposition
        if (tokens1[i] !== tokens2[j])
            nTransponsitions++;
        j++;
    }
    // draw out duplications
    nTransponsitions /= 2;
    // return calculated distance
    return (nMatches / n1 + nMatches / n2 + (nMatches - nTransponsitions) / nMatches) / 3;
}
exports.similarityOf = similarityOf;
//# sourceMappingURL=jaro.js.map