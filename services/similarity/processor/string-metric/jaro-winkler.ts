import {similarityOf as jaro} from "./jaro";

// Jaro-Winkler
export function similarityOf(tokens1 : ArrayLike<string>, tokens2 : ArrayLike<string>, p? : number) {
    var d = jaro(tokens1, tokens2);
    
    p = p || 0.1;
    if (p > 0.25) p = 0.25;
    if (p < 0) p = 0;
    
    var commonPrefix = 0;
    while (commonPrefix < 4 && tokens1[commonPrefix] === tokens2[commonPrefix]) {
        commonPrefix++;
    }
    
    return d + commonPrefix*p*(1 - d);
}