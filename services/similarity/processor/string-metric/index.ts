import {ISimilarityPipelineFunction, ISimilarityArgument} from "../../models.ts"
import {distance} from "./damerau-levenshtein"

var pipelineFunction : ISimilarityPipelineFunction = function(arg) {
    arg.distance += distance(arg.tokens1, arg.tokens2);
    arg.totalWeight += Math.max(arg.tokens1.length, arg.tokens2.length);
}

export default pipelineFunction;