"use strict";
const jaro_1 = require("./jaro");
// Jaro-Winkler
function similarityOf(tokens1, tokens2, p) {
    var d = jaro_1.similarityOf(tokens1, tokens2);
    p = p || 0.1;
    if (p > 0.25)
        p = 0.25;
    if (p < 0)
        p = 0;
    var commonPrefix = 0;
    while (commonPrefix < 4 && tokens1[commonPrefix] === tokens2[commonPrefix]) {
        commonPrefix++;
    }
    return d + commonPrefix * p * (1 - d);
}
exports.similarityOf = similarityOf;
//# sourceMappingURL=jaro-winkler.js.map