"use strict";
const damerau_levenshtein_1 = require("./damerau-levenshtein");
var pipelineFunction = function (arg) {
    arg.distance += damerau_levenshtein_1.distance(arg.tokens1, arg.tokens2);
    arg.totalWeight += Math.max(arg.tokens1.length, arg.tokens2.length);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = pipelineFunction;
//# sourceMappingURL=index.js.map