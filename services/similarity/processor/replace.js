"use strict";
var pipelineFunction = function (arg, options) {
    var rules = (options && options["rules"]) || exports.defaultRules;
    arg.normalizedName1 = replace(arg.normalizedName1, rules);
    arg.normalizedName2 = replace(arg.normalizedName2, rules);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = pipelineFunction;
function replace(str, rules) {
    for (var index = 0; index < rules.length; index++) {
        var rule = rules[index];
        str = str.replace(rule.pattern, rule.value);
    }
    return str;
}
exports.defaultRules = [
    {
        pattern: /wi-fi/,
        value: "wifi"
    },
    {
        pattern: /laptop|dtdd|ram/,
        value: ""
    },
    {
        // parenthesis and text inside
        pattern: /\(.*?\)/,
        value: ""
    },
    {
        pattern: /[+-\/]/,
        value: " "
    },
    {
        pattern: /(\d|dual) sim( (dual|\d) (sóng|standby))?/,
        value: ""
    }
];
//# sourceMappingURL=replace.js.map