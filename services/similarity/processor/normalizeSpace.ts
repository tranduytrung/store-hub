import {ISimilarityPipelineFunction, ISimilarityArgument} from "../models.ts"

var pipelineFunction : ISimilarityPipelineFunction = function(arg) {
    arg.normalizedName1 = arg.normalizedName1.trim().replace(/\s{2,}/, " ");
    arg.normalizedName2 = arg.normalizedName2.trim().replace(/\s{2,}/, " ");
}

export default pipelineFunction;
