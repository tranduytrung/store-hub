import {ISimilarityPipelineFunction, ISimilarityArgument} from "../models.ts"

var pipelineFunction : ISimilarityPipelineFunction = function(arg) {
    arg.tokens1 = arg.normalizedName1.split(/\s+/g);
    arg.tokens2 = arg.normalizedName2.split(/\s+/g);
}

export default pipelineFunction;
