import {ISimilarityPipelineFunction, ISimilarityArgument} from "../models.ts"

var pipelineFunction : ISimilarityPipelineFunction = function(arg) {
    arg.normalizedName1 = arg.product1.name.toLowerCase();
    arg.normalizedName2 = arg.product2.name.toLowerCase();
}

export default pipelineFunction;