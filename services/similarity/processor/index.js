"use strict";
var lowerCase_1 = require("./lowerCase");
exports.lowerCase = lowerCase_1.default;
var tokenize_1 = require("./tokenize");
exports.tokenize = tokenize_1.default;
var replace_1 = require("./replace");
exports.replace = replace_1.default;
var normalizeSpace_1 = require("./normalizeSpace");
exports.normalizeSpace = normalizeSpace_1.default;
var weight_1 = require("./weight");
exports.weight = weight_1.default;
var string_metric_1 = require("./string-metric");
exports.stringMetric = string_metric_1.default;
//# sourceMappingURL=index.js.map