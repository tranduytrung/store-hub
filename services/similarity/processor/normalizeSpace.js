"use strict";
var pipelineFunction = function (arg) {
    arg.normalizedName1 = arg.normalizedName1.trim().replace(/\s{2,}/, " ");
    arg.normalizedName2 = arg.normalizedName2.trim().replace(/\s{2,}/, " ");
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = pipelineFunction;
//# sourceMappingURL=normalizeSpace.js.map