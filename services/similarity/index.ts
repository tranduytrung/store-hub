import {IProduct} from "../../models/product"
import {ISimilarityPipelineFunction, ISimilarityArgument} from "./models"
import * as Processor from "./processor"

interface IPipelineNode {
    func: ISimilarityPipelineFunction;
    options?: {}
}

/**
 * SimilarityTester
 */
export class ProductSimilarity {
    constructor(private pipeline?: IPipelineNode[]) {
        if (!pipeline) {
            this.pipeline = defaultPipeline
        }
    }
    
    test(product1 : IProduct, product2 : IProduct) {
        var arg : ISimilarityArgument = {
            product1: product1,
            product2: product2,
            distance: 0,
            totalWeight: 0,
            normalizedName1: null,
            normalizedName2: null,
            tokens1: null,
            tokens2: null
        }
        
        for (var index = 0; index < this.pipeline.length; index++) {
            var node = this.pipeline[index];
            node.func(arg, node.options);
        }
        
        return 1 - arg.distance/arg.totalWeight;
    }
}

var defaultPipeline: IPipelineNode[] = [
    {
        func: Processor.lowerCase
    },
    {
        func: Processor.replace
    },
    {
        func: Processor.normalizeSpace
    },
    {
        func: Processor.tokenize
    },
    {
        func: Processor.stringMetric
    },
    {
        func: Processor.weight
    }
];