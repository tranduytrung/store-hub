"use strict";
const Processor = require("./processor");
/**
 * SimilarityTester
 */
class ProductSimilarity {
    constructor(pipeline) {
        this.pipeline = pipeline;
        if (!pipeline) {
            this.pipeline = defaultPipeline;
        }
    }
    test(product1, product2) {
        var arg = {
            product1: product1,
            product2: product2,
            distance: 0,
            totalWeight: 0,
            normalizedName1: null,
            normalizedName2: null,
            tokens1: null,
            tokens2: null
        };
        for (var index = 0; index < this.pipeline.length; index++) {
            var node = this.pipeline[index];
            node.func(arg, node.options);
        }
        return 1 - arg.distance / arg.totalWeight;
    }
}
exports.ProductSimilarity = ProductSimilarity;
var defaultPipeline = [
    {
        func: Processor.lowerCase
    },
    {
        func: Processor.replace
    },
    {
        func: Processor.normalizeSpace
    },
    {
        func: Processor.tokenize
    },
    {
        func: Processor.stringMetric
    },
    {
        func: Processor.weight
    }
];
//# sourceMappingURL=index.js.map