// 3ff09ec7-cf28-487c-9141-edd25759e85f
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const Wreck = require("wreck");
const users_1 = require("../db/users");
const _ = require("lodash");
const config_1 = require("../../config");
function broadcast(message, recepients) {
    return new Promise(function (resolve, reject) {
        var payload = {
            app_id: config_1.onesignal.appId,
            headings: {
                en: message.header || "Some thinngs good for you"
            },
            contents: {
                en: message.body
            },
            data: message.data,
            include_player_ids: recepients,
            big_picture: message.bigPicture
        };
        Wreck.post("https://onesignal.com/api/v1/notifications", {
            payload: JSON.stringify(payload),
            headers: {
                "content-type": "application/json",
                "authorization": `Basic ${config_1.onesignal.appKey}`
            },
            json: true,
            timeout: 60000,
        }, function (error, reponse, payload) {
            if (error) {
                return reject(error);
            }
            resolve(payload);
        });
    });
}
exports.broadcast = broadcast;
function formatPrice(value) {
    return value.toString().replace(/\B(?=(\d\d\d)+$)/g, ",");
}
function broadcastProductUpdate(product, userIds) {
    return __awaiter(this, void 0, void 0, function* () {
        var price = _.first(product.prices).value;
        var priceUnit = product.priceUnit;
        var users = userIds ? yield users_1.getMany(userIds) : yield users_1.getForBroadcast(product._id.toString(), price);
        var ids = _(users).map("onesignalIds").flatten().valueOf();
        if (_.isEmpty(ids)) {
            return;
        }
        var result = yield broadcast({
            header: `Price Alert'`,
            body: `${product.name}'s price are now ${formatPrice(price)} ${priceUnit}'. Check this for more detail`,
            data: {
                product: {
                    _id: product["_id"],
                    groupId: product.groupId
                }
            },
            bigPicture: product.media[0] ? product.media[0].url : null
        }, ids);
        for (var index = 0; index < users.length; index++) {
            var user = users[index];
            var wishItem = _.find(user.wishlist, "productId", product["_id"]);
            yield users_1.updateWishItem(user["_id"], wishItem, true).catch(_.noop);
        }
    });
}
exports.broadcastProductUpdate = broadcastProductUpdate;
//# sourceMappingURL=index.js.map