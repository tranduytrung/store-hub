// 3ff09ec7-cf28-487c-9141-edd25759e85f

import * as Wreck from "wreck"
import {ObjectID} from "mongodb"
import {IProduct} from "../../models/product"
import {getForBroadcast, updateWishItem, getMany as getUsers} from "../db/users"
import * as _ from "lodash"
import {onesignal as onesignalConfig} from "../../config"

interface IMessage {
    header?: string,
    body: string,
    data?: any,
    bigPicture?: string
}

interface ISentResult {
    id: string,
    recipients: number
}

export function broadcast(message: IMessage, recepients: string[]) : Promise<ISentResult> {
    return new Promise<ISentResult>(function(resolve, reject) {
        var payload = {
            app_id: onesignalConfig.appId,
            headings:  {
                en: message.header || "Some thinngs good for you"
            } ,
            contents: {
                en: message.body
            },
            data: message.data,
            include_player_ids: recepients,
            big_picture: message.bigPicture
        };
        
        Wreck.post("https://onesignal.com/api/v1/notifications", {
            payload: JSON.stringify(payload),
            headers: {
                "content-type": "application/json",
                "authorization": `Basic ${onesignalConfig.appKey}`
            },
            json: true,
            timeout: 60000,
        }, function(error, reponse, payload) {
            if (error) {
                return reject(error);
            }
            
            resolve(<ISentResult>payload);
        });
    });
}

function formatPrice(value : number) {
    return value.toString().replace(/\B(?=(\d\d\d)+$)/g, ",");
}

export async function broadcastProductUpdate(product : IProduct, userIds? : string[] | ObjectID[] ) {
    var price =  _.first(product.prices).value;
    var priceUnit = product.priceUnit;
    var users = userIds? await getUsers(userIds) : await getForBroadcast(product._id.toString(), price);
    
    var ids = _(users).map("onesignalIds").flatten<string>().valueOf();
    if (_.isEmpty(ids)) {
        return;
    }
    
    var result = await broadcast({
        header: `Price Alert'`,
        body: `${product.name}'s price are now ${formatPrice(price)} ${priceUnit}'. Check this for more detail`,
        data: {
            product: {
                _id: product["_id"],
                groupId: product.groupId 
            }
        },
        bigPicture: product.media[0]? product.media[0].url : null
    }, ids);
    
    for (var index = 0; index < users.length; index++) {
        var user = users[index];
        var wishItem = _.find(user.wishlist, "productId", product["_id"]);
        
        await updateWishItem(user["_id"], wishItem, true).catch(_.noop);        
    }
}