"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const Crawler = require("../crawler");
const manager_1 = require("./manager");
const products_1 = require("../db/products");
const users_1 = require("../db/users");
const ad_1 = require("../db/ad");
const tasks_1 = require("../db/tasks");
const time_1 = require("./time");
const mongodb_1 = require("mongodb");
manager_1.registerExecutor({
    name: "importProductsFromThegioididong",
    execute: function () {
        return __awaiter(this, void 0, void 0, function* () {
            yield Crawler.getAllProductsFromThegioididong((product) => __awaiter(this, void 0, void 0, function* () {
                yield products_1.upsert(product);
            }));
        });
    }
});
manager_1.registerExecutor({
    name: "importProductsFromFptshop",
    execute: function () {
        return __awaiter(this, void 0, void 0, function* () {
            yield Crawler.getAllProductsFromFptshop((product) => __awaiter(this, void 0, void 0, function* () {
                yield products_1.upsert(product);
            }));
        });
    }
});
manager_1.registerExecutor({
    name: "importProductsFromTiki",
    execute: function () {
        return __awaiter(this, void 0, void 0, function* () {
            yield Crawler.getAllProductsFromTiki((product) => __awaiter(this, void 0, void 0, function* () {
                yield products_1.upsert(product);
            }));
        });
    }
});
manager_1.registerExecutor({
    name: "importProductsFromLazada",
    execute: function (categories, context) {
        return __awaiter(this, void 0, void 0, function* () {
            yield Crawler.getAllProductsFromLazada((product) => __awaiter(this, void 0, void 0, function* () {
                yield products_1.upsert(product);
            }), (context) => __awaiter(this, void 0, void 0, function* () {
                this.params[1] = context;
                yield tasks_1.upsert({
                    _id: this._id,
                    assignee: undefined,
                    executor: undefined,
                    nextRun: undefined,
                    params: this.params,
                    state: undefined
                });
            }), categories, context);
        });
    }
});
manager_1.registerExecutor({
    name: "updateProduct",
    execute: function (productId) {
        return __awaiter(this, void 0, void 0, function* () {
            var nextRun = yield updateProduct(productId);
            return nextRun;
        });
    }
});
manager_1.registerExecutor({
    name: "importAdsFromThegioididong",
    execute: function () {
        return __awaiter(this, void 0, void 0, function* () {
            var ads = yield Crawler.getAdsFromThegioididong();
            yield ad_1.clear("thegioididong");
            yield ad_1.insert(ads);
        });
    }
});
manager_1.registerExecutor({
    name: "importAdsFromFptshop",
    execute: function () {
        return __awaiter(this, void 0, void 0, function* () {
            var ads = yield Crawler.getAdsFromFptshop();
            yield ad_1.clear("fptshop");
            yield ad_1.insert(ads);
        });
    }
});
manager_1.registerExecutor({
    name: "importAdsFromTiki",
    execute: function () {
        return __awaiter(this, void 0, void 0, function* () {
            var ads = yield Crawler.getAdsFromTiki();
            yield ad_1.clear("tiki");
            yield ad_1.insert(ads);
        });
    }
});
manager_1.registerExecutor({
    name: "importAdsFromLazada",
    execute: function () {
        return __awaiter(this, void 0, void 0, function* () {
            var ads = yield Crawler.getAdsFromLazada();
            yield ad_1.clear("lazada");
            yield ad_1.insert(ads);
        });
    }
});
manager_1.registerCoreTask({
    taskId: "57076926ad4552011a627029",
    executor: "importProductsFromThegioididong",
    params: null,
    schedule: 24 * 60 * 60 * 1000
});
manager_1.registerCoreTask({
    taskId: "57076932ad4552011a62702a",
    executor: "importProductsFromFptshop",
    params: null,
    schedule: 24 * 60 * 60 * 1000
});
manager_1.registerCoreTask({
    taskId: "57076933ad4552011a62702b",
    executor: "importProductsFromTiki",
    params: null,
    schedule: 24 * 60 * 60 * 1000
});
manager_1.registerCoreTask({
    taskId: "571c90e5f1d4eeb95dcf9e60",
    executor: "importProductsFromLazada",
    params: [["http://www.lazada.vn/dien-thoai-di-dong"]],
    schedule: 36 * 60 * 60 * 1000
});
manager_1.registerCoreTask({
    taskId: "5768c60305f091550ee666ee",
    executor: "importProductsFromLazada",
    params: [["http://www.lazada.vn/laptop"]],
    schedule: 24 * 60 * 60 * 1000
});
manager_1.registerCoreTask({
    taskId: "5768c60405f091550ee666ef",
    executor: "importProductsFromLazada",
    params: [["http://www.lazada.vn/may-tinh-bang"]],
    schedule: 24 * 60 * 60 * 1000
});
manager_1.registerCoreTask({
    taskId: "575443c0b8217745d7ed19d9",
    executor: "importAdsFromThegioididong",
    params: null,
    schedule: 24 * 60 * 60 * 1000
});
manager_1.registerCoreTask({
    taskId: "575445e3b8217745d7ed19da",
    executor: "importAdsFromFptshop",
    params: null,
    schedule: 24 * 60 * 60 * 1000
});
manager_1.registerCoreTask({
    taskId: "575445e4b8217745d7ed19db",
    executor: "importAdsFromTiki",
    params: null,
    schedule: 24 * 60 * 60 * 1000
});
manager_1.registerCoreTask({
    taskId: "5780acbcd5adac4d2c356130",
    executor: "importAdsFromLazada",
    params: null,
    schedule: 24 * 60 * 60 * 1000
});
function updateProduct(productId) {
    return __awaiter(this, void 0, void 0, function* () {
        var id = new mongodb_1.ObjectID(productId);
        var product = (yield products_1.collection
            .find({
            _id: id
        })
            .project({
            url: true,
            source: true
        }).toArray())[0];
        var crawlFunction;
        switch (product.source) {
            case "lazada":
                crawlFunction = Crawler.getProductFromLazada;
                break;
            case "tiki":
                crawlFunction = Crawler.getProductFromTiki;
                break;
            case "fptshop":
                crawlFunction = Crawler.getProductFromFptshop;
                break;
            case "thegioididong":
                crawlFunction = Crawler.getProductFromThegioididong;
                break;
            default:
                throw new Error(`unable to determine crawler for ${id.toHexString()}`);
        }
        product = yield crawlFunction(product.url);
        yield products_1.upsert(product);
        var nSub = yield users_1.countByProductId(id);
        return time_1.getNextProductUpdateDate(nSub);
    });
}
//# sourceMappingURL=crawl.js.map