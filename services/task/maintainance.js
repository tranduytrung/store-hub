"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const products_1 = require("../db/products");
const manager_1 = require("./manager");
manager_1.registerExecutor({
    name: "deleteOutdatedProducts",
    execute: function () {
        return __awaiter(this, void 0, void 0, function* () {
            yield products_1.deleteOutdated();
        });
    }
});
manager_1.registerCoreTask({
    taskId: "57d8350f2cc1c05cb922c508",
    executor: "deleteOutdatedProducts",
    params: null,
    schedule: 24 * 60 * 60 * 1000,
});
//# sourceMappingURL=maintainance.js.map