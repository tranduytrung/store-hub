"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const tasks_1 = require("../db/tasks");
const users_1 = require("../db/users");
const time_1 = require("./time");
const task_1 = require("../../models/task");
function spwanOnDemandTask(productId) {
    return __awaiter(this, void 0, void 0, function* () {
        var task = yield tasks_1.get(productId);
        if (task && task.state === task_1.TaskState.running)
            return;
        var nSub = yield users_1.countByProductId(productId);
        if (task) {
            task.state = task_1.TaskState.scheduled;
            task.nextRun = time_1.getNextProductUpdateDate(nSub, task.lastRun);
            yield tasks_1.upsert(task);
            return;
        }
        task = {
            _id: productId,
            executor: `updateProduct`,
            nextRun: time_1.getNextProductUpdateDate(nSub),
            params: [productId.toHexString()],
            state: task_1.TaskState.scheduled,
            assignee: null
        };
        yield tasks_1.upsert(task);
    });
}
exports.spwanOnDemandTask = spwanOnDemandTask;
//# sourceMappingURL=spawn.js.map