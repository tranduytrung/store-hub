const hourMultiplier = 60 * 60 * 1000;
const minUpdatePeriod = 1 * hourMultiplier;
const maxUpdatePeriod = 12 * hourMultiplier;
const thresholdForMinUpdatePeriod = 1000;
export function getNextProductUpdateDate(subscribers : number, baseDate? : Date) {
    if (subscribers > thresholdForMinUpdatePeriod) subscribers = thresholdForMinUpdatePeriod;
    var offset = minUpdatePeriod + (1 - subscribers / thresholdForMinUpdatePeriod) * (maxUpdatePeriod - minUpdatePeriod);
    
    var date = baseDate || new Date();
    date.setTime(date.getTime() + offset);
    return date;
}