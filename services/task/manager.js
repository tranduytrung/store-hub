"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const _ = require("lodash");
const tasks_1 = require("../db/tasks");
const task_1 = require("../../models/task");
const mongodb_1 = require("mongodb");
const winston = require("winston");
const config_1 = require("../../config");
const date_1 = require("../../utils/date");
var executors = {};
function registerExecutor(executor) {
    if (executors[executor.name]) {
        throw new Error(`${executor.name} are already existed`);
    }
    executors[executor.name] = executor;
}
exports.registerExecutor = registerExecutor;
var coreTasks = {};
function registerCoreTask(task) {
    if (coreTasks[task.taskId]) {
        throw new Error(`${task.taskId} are already existed`);
    }
    coreTasks[task.taskId] = task;
}
exports.registerCoreTask = registerCoreTask;
function initialize() {
    return __awaiter(this, void 0, void 0, function* () {
        // init tasks
        for (var id in coreTasks) {
            var taskInfo = coreTasks[id];
            var task = yield tasks_1.get(taskInfo.taskId);
            if (task)
                continue;
            yield tasks_1.upsert({
                _id: new mongodb_1.ObjectID(taskInfo.taskId),
                executor: taskInfo.executor,
                params: taskInfo.params,
                nextRun: new Date(),
                state: taskInfo.schedule ? task_1.TaskState.scheduled : task_1.TaskState.suspended,
                assignee: null
            });
        }
        setInterval(taskRunner, 60000);
        taskRunner();
    });
}
exports.initialize = initialize;
var executingTaskIds = [];
var nExecutor = 0;
function taskRunner() {
    (() => __awaiter(this, void 0, void 0, function* () {
        nExecutor++;
        if (nExecutor > config_1.taskExecutor.maxConcurrency)
            return;
        var task;
        while (task = yield tasks_1.popNow(executingTaskIds)) {
            try {
                executingTaskIds.push(task._id);
                var next;
                var isError = false;
                var core = coreTasks[task._id.toHexString()];
                var execute = executors[task.executor].execute;
                var result = next = execute.apply(task, task.params);
                // if is promise-liked
                if (result && _.isFunction(result["then"])) {
                    next = yield result;
                }
                if (!_.isDate(next)) {
                    if (core) {
                        var schedule = core.schedule;
                        if (_.isNumber(schedule)) {
                            next = date_1.getRelativeDateByMiliseconds(schedule);
                        }
                        else {
                            next = schedule();
                        }
                    }
                    else {
                        next = null;
                    }
                }
            }
            catch (error) {
                winston.error("an error happened when execute a task", error);
                isError = true;
                next = null;
            }
            yield tasks_1.upsert({
                _id: task._id,
                assignee: null,
                executor: undefined,
                lastRun: undefined,
                nextRun: next,
                params: core ? core.params : undefined,
                state: isError ? task_1.TaskState.error : next ? task_1.TaskState.scheduled : task_1.TaskState.completed
            });
            _.pull(executingTaskIds, task._id);
        }
    }))().then(_.noop, function (error) {
        winston.error("an error happened in task loop", error);
    }).then(function () {
        nExecutor--;
    });
}
//# sourceMappingURL=manager.js.map