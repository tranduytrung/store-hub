import * as UUID from "node-uuid"
import * as _ from "lodash"
import {upsert as updateTask, popNow as popTask, get as getTask} from "../db/tasks"
import {ITask, TaskState} from "../../models/task"
import {ObjectID} from "mongodb"
import * as winston from "winston"
import {taskExecutor as taskExecutorConfig} from "../../config"
import {getRelativeDateByMiliseconds} from "../../utils/date"

export interface IExecutor {
    name: string
    execute: Function
}

export interface ICoreTask {
    taskId: string
    executor: string
    schedule?: (() => Date) | number
    params: any[]
}

var executors: { [name: string]: IExecutor } = {};
export function registerExecutor(executor: IExecutor) {
    if (executors[executor.name]) {
        throw new Error(`${executor.name} are already existed`);
    }

    executors[executor.name] = executor;
}

var coreTasks: { [name: string]: ICoreTask } = {};
export function registerCoreTask(task: ICoreTask) {
    if (coreTasks[task.taskId]) {
        throw new Error(`${task.taskId} are already existed`);
    }

    coreTasks[task.taskId] = task;
}

export async function initialize() {
    // init tasks
    for (var id in coreTasks) {
        var taskInfo = coreTasks[id];
        var task = await getTask(taskInfo.taskId);

        if (task) continue;
        await updateTask({
            _id: new ObjectID(taskInfo.taskId),
            executor: taskInfo.executor,
            params: taskInfo.params,
            nextRun: new Date(),
            state: taskInfo.schedule ? TaskState.scheduled : TaskState.suspended,
            assignee: null
        });
    }

    setInterval(taskRunner, 60000);
    taskRunner();
}

var executingTaskIds: ObjectID[] = [];
var nExecutor = 0;
function taskRunner() {
    (async () => {
        nExecutor++;
        if (nExecutor > taskExecutorConfig.maxConcurrency) return;
        var task: ITask;
        while (task = await popTask(executingTaskIds)) {
            try {
                executingTaskIds.push(task._id);
                var next: Date;
                var isError = false;
                var core = coreTasks[task._id.toHexString()];
                var execute: Function = executors[task.executor].execute;
                var result = next = execute.apply(task, task.params);
                // if is promise-liked
                if (result && _.isFunction(result["then"])) {
                    next = await result;
                }

                if (!_.isDate(next)) {
                    if (core) {
                        var schedule = core.schedule;
                        if (_.isNumber(schedule)) {
                            next = getRelativeDateByMiliseconds(schedule);
                        } else {
                            next = schedule();
                        }
                    } else {
                        next = null;
                    }
                }
            } catch (error) {
                winston.error("an error happened when execute a task", error);
                isError = true;
                next = null;
            }

            await updateTask({
                _id: task._id,
                assignee: null,
                executor: undefined,
                lastRun: undefined,
                nextRun: next,
                params: core ? core.params : undefined,
                state: isError ? TaskState.error : next ? TaskState.scheduled : TaskState.completed
            });

            _.pull(executingTaskIds, task._id);
        }
    })().then(_.noop, function (error) {
        winston.error("an error happened in task loop", error);
    }).then(function () {
        nExecutor--;
    });
}