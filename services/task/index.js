"use strict";
var manager_1 = require("./manager");
exports.registerExecutor = manager_1.registerExecutor;
exports.registerCoreTask = manager_1.registerCoreTask;
const manager_2 = require("./manager");
require("./crawl");
require("./maintainance");
function initialize() {
    return manager_2.initialize();
}
exports.initialize = initialize;
//# sourceMappingURL=index.js.map