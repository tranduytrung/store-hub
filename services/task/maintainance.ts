import {deleteOutdated } from "../db/products"
import {registerExecutor, registerCoreTask} from "./manager"

registerExecutor({
    name: "deleteOutdatedProducts",
    execute: async function() {
        await deleteOutdated();
    }
});

registerCoreTask({
    taskId: "57d8350f2cc1c05cb922c508",
    executor: "deleteOutdatedProducts",
    params: null,
    schedule: 24 * 60 * 60 * 1000,
});