export {registerExecutor, registerCoreTask} from "./manager"
import {initialize as initManager} from "./manager"
require("./crawl");
require("./maintainance");

export function initialize() {
    return initManager();
}