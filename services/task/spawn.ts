import {get as getTask, upsert as upsertTask} from "../db/tasks"
import {collection as products} from "../db/products"
import {countByProductId as countUsersByProductId} from "../db/users"
import {getNextProductUpdateDate} from "./time"
import {TaskState} from "../../models/task"
import {IProduct} from "../../models/product"
import {ObjectID} from "mongodb"

export async function spwanOnDemandTask(productId: ObjectID) {
    var task = await getTask(productId);
    if (task && task.state === TaskState.running) return;
    var nSub = await countUsersByProductId(productId);

    if (task) {
        task.state = TaskState.scheduled;
        task.nextRun = getNextProductUpdateDate(nSub, task.lastRun);
        await upsertTask(task);
        return;
    }

    task = {
        _id: productId,
        executor: `updateProduct`,
        nextRun: getNextProductUpdateDate(nSub),
        params: [productId.toHexString()],
        state: TaskState.scheduled,
        assignee: null
    }

    await upsertTask(task);
}