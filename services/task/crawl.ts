import * as Crawler from "../crawler"
import {registerExecutor, registerCoreTask} from "./manager"
import {upsert as upsertProduct, regroupAll as regroupAllProducts, collection as products} from "../db/products"
import {countByProductId as countUsersByProductId} from "../db/users"
import {insert as insertAds, clear as clearAds} from "../db/ad"
import {upsert as updateTask} from "../db/tasks"
import {TaskState} from "../../models/task"
import {IProduct} from "../../models/product"
import {getNextProductUpdateDate} from "./time"
import {ObjectID} from "mongodb"

registerExecutor({
    name: "importProductsFromThegioididong",
    execute: async function () {
        await Crawler.getAllProductsFromThegioididong(async (product) => {
            await upsertProduct(product);
        });
    }
});

registerExecutor({
    name: "importProductsFromFptshop",
    execute: async function () {
        await Crawler.getAllProductsFromFptshop(async (product) => {
            await upsertProduct(product);
        });
    }
});

registerExecutor({
    name: "importProductsFromTiki",
    execute: async function () {
        await Crawler.getAllProductsFromTiki(async (product) => {
            await upsertProduct(product);
        });
    }
});

registerExecutor({
    name: "importProductsFromLazada",
    execute: async function (categories: string[], context?) {
        await Crawler.getAllProductsFromLazada(
            async (product) => {
                await upsertProduct(product);
            },
            async (context) => {
                this.params[1] = context;
                await updateTask({
                    _id: this._id,
                    assignee: undefined,
                    executor: undefined,
                    nextRun: undefined,
                    params: this.params,
                    state: undefined
                });
            },
            categories,
            context
        );
    }
});

registerExecutor({
    name: "updateProduct",
    execute: async function (productId: string) {
        var nextRun = await updateProduct(productId);
        return nextRun;
    }
});

registerExecutor({
    name: "importAdsFromThegioididong",
    execute: async function () {
        var ads = await Crawler.getAdsFromThegioididong();
        await clearAds("thegioididong");
        await insertAds(ads);
    }
});

registerExecutor({
    name: "importAdsFromFptshop",
    execute: async function () {
        var ads = await Crawler.getAdsFromFptshop();
        await clearAds("fptshop");
        await insertAds(ads);
    }
});

registerExecutor({
    name: "importAdsFromTiki",
    execute: async function () {
        var ads = await Crawler.getAdsFromTiki();
        await clearAds("tiki");
        await insertAds(ads);
    }
});

registerExecutor({
    name: "importAdsFromLazada",
    execute: async function () {
        var ads = await Crawler.getAdsFromLazada();
        await clearAds("lazada");
        await insertAds(ads);
    }
});

registerCoreTask({
    taskId: "57076926ad4552011a627029",
    executor: "importProductsFromThegioididong",
    params: null,
    schedule: 24 * 60 * 60 * 1000
});

registerCoreTask({
    taskId: "57076932ad4552011a62702a",
    executor: "importProductsFromFptshop",
    params: null,
    schedule: 24 * 60 * 60 * 1000
});

registerCoreTask({
    taskId: "57076933ad4552011a62702b",
    executor: "importProductsFromTiki",
    params: null,
    schedule: 24 * 60 * 60 * 1000
});

registerCoreTask({
    taskId: "571c90e5f1d4eeb95dcf9e60",
    executor: "importProductsFromLazada",
    params: [["http://www.lazada.vn/dien-thoai-di-dong"]],
    schedule: 36 * 60 * 60 * 1000
});

registerCoreTask({
    taskId: "5768c60305f091550ee666ee",
    executor: "importProductsFromLazada",
    params: [["http://www.lazada.vn/laptop"]],
    schedule: 24 * 60 * 60 * 1000
});

registerCoreTask({
    taskId: "5768c60405f091550ee666ef",
    executor: "importProductsFromLazada",
    params: [["http://www.lazada.vn/may-tinh-bang"]],
    schedule: 24 * 60 * 60 * 1000
});

registerCoreTask({
    taskId: "575443c0b8217745d7ed19d9",
    executor: "importAdsFromThegioididong",
    params: null,
    schedule: 24 * 60 * 60 * 1000
});

registerCoreTask({
    taskId: "575445e3b8217745d7ed19da",
    executor: "importAdsFromFptshop",
    params: null,
    schedule: 24 * 60 * 60 * 1000
});

registerCoreTask({
    taskId: "575445e4b8217745d7ed19db",
    executor: "importAdsFromTiki",
    params: null,
    schedule: 24 * 60 * 60 * 1000
});

registerCoreTask({
    taskId: "5780acbcd5adac4d2c356130",
    executor: "importAdsFromLazada",
    params: null,
    schedule: 24 * 60 * 60 * 1000
});

async function updateProduct(productId: string) {
    var id = new ObjectID(productId);
    var product: IProduct = (await products
        .find({
            _id: id
        })
        .project({
            url: true,
            source: true
        }).toArray())[0];

    var crawlFunction: (url: string) => Promise<IProduct>;
    switch (product.source) {
        case "lazada":
            crawlFunction = Crawler.getProductFromLazada;
            break;
        case "tiki":
            crawlFunction = Crawler.getProductFromTiki;
            break;
        case "fptshop":
            crawlFunction = Crawler.getProductFromFptshop;
            break;
        case "thegioididong":
            crawlFunction = Crawler.getProductFromThegioididong;
            break;
        default:
            throw new Error(`unable to determine crawler for ${id.toHexString()}`);
    }

    product = await crawlFunction(product.url);
    await upsertProduct(product);

    var nSub = await countUsersByProductId(id);
    return getNextProductUpdateDate(nSub);
}