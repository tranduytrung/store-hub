"use strict";
const thegioididong_1 = require("./thegioididong");
exports.getProductFromThegioididong = thegioididong_1.getProduct;
exports.getAllProductsFromThegioididong = thegioididong_1.getAllProducts;
exports.getAdsFromThegioididong = thegioididong_1.getAds;
const fptshop_1 = require("./fptshop");
exports.getProductFromFptshop = fptshop_1.getProduct;
exports.getAllProductsFromFptshop = fptshop_1.getAllProducts;
exports.getAdsFromFptshop = fptshop_1.getAds;
const tiki_1 = require("./tiki");
exports.getProductFromTiki = tiki_1.getProduct;
exports.getAllProductsFromTiki = tiki_1.getAllProducts;
exports.getAdsFromTiki = tiki_1.getAds;
const lazada_1 = require("./lazada");
exports.getProductFromLazada = lazada_1.getProduct;
exports.getAllProductsFromLazada = lazada_1.getAllProducts;
exports.getAdsFromLazada = lazada_1.getAds;
const URI = require("urijs");
const error_1 = require("../../error");
function getProduct(url) {
    var hostname = URI(url).hostname();
    switch (hostname) {
        case "www.lazada.vn":
            return lazada_1.getProduct(url);
        case "tiki.vn":
            return tiki_1.getProduct(url);
        case "www.thegioididong.com":
            return thegioididong_1.getProduct(url);
        case "fptshop.com.vn":
            return fptshop_1.getProduct(url);
        default:
            return Promise.reject(new error_1.ArgumentError(`the hostname ${hostname} is not supported`));
    }
}
exports.getProduct = getProduct;
//# sourceMappingURL=index.js.map