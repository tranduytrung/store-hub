
import {getProduct as getProductFromThegioididong, getAllProducts as getAllProductsFromThegioididong, getAds as getAdsFromThegioididong} from "./thegioididong"
import {getProduct as getProductFromFptshop, getAllProducts as getAllProductsFromFptshop, getAds as getAdsFromFptshop} from "./fptshop"
import {getProduct as getProductFromTiki, getAllProducts as getAllProductsFromTiki, getAds as getAdsFromTiki } from "./tiki"
import {getProduct as getProductFromLazada, getAllProducts as getAllProductsFromLazada, getAds as getAdsFromLazada } from "./lazada"
import * as URI from "urijs"
import {ArgumentError} from "../../error"
import {IProduct} from "../../models/product"

export {getAdsFromFptshop, getAdsFromThegioididong, getAdsFromTiki, getAdsFromLazada,
getProductFromFptshop, getProductFromLazada, getProductFromThegioididong, getProductFromTiki,
getAllProductsFromFptshop, getAllProductsFromLazada, getAllProductsFromThegioididong, getAllProductsFromTiki}

export function getProduct(url: string): Promise<IProduct> {
    var hostname = URI(url).hostname();
    switch (hostname) {
        case "www.lazada.vn":
            return getProductFromLazada(url);
        case "tiki.vn":
            return getProductFromTiki(url);
        case "www.thegioididong.com":
            return getProductFromThegioididong(url);
        case "fptshop.com.vn":
            return getProductFromFptshop(url);
        default:
            return Promise.reject<IProduct>(new ArgumentError(`the hostname ${hostname} is not supported`));
    }
}