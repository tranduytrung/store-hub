"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const Core = require("./core");
const error_1 = require("../../error");
const _ = require("lodash");
const winston = require("winston");
const promise_1 = require("../../utils/promise");
const config_1 = require("../../config");
function getAllProducts(callback) {
    return __awaiter(this, void 0, void 0, function* () {
        for (let path in categoryMap) {
            var loaded = 1;
            do {
                var query = `http://fptshop.com.vn/Ajax/FilterProduct/ViewMore?page=${loaded}&url=http://fptshop.com.vn${path}&typeView=Hot`;
                var result = yield getLinksFromPage(query);
                yield promise_1.parallelEach(result.links, function (link) {
                    return __awaiter(this, void 0, void 0, function* () {
                        try {
                            var product = yield getProduct(link);
                            yield callback(product);
                            global.gc();
                        }
                        catch (error) {
                            var message = (_.isString(error) ? error : _.get(error, "message")) || "unknown error";
                            winston.error(message, { source: "fptshop", detail: error });
                        }
                    });
                }, { limit: config_1.jsdom.maxConcurrency });
                loaded = result.loaded;
            } while (result.next);
        }
    });
}
exports.getAllProducts = getAllProducts;
function getLinksFromPage(url) {
    return __awaiter(this, void 0, Promise, function* () {
        var window = Core.defaultWindow;
        var u = window["u"];
        var maxAttemp = 3;
        var attempt = 1;
        while (attempt <= maxAttemp) {
            var links = [];
            var next;
            var loaded;
            var error = null;
            try {
                var data = yield Core.get(url);
                var ulinks = u(data["content"]).find("a.p-link-prod,div.fshop-lplap-item >a[href]");
                ulinks.each(function (linkTag) {
                    var href = linkTag["href"];
                    links.push(href.startsWith("http") ? href : "http://fptshop.com.vn" + href);
                });
                next = data["next"];
                loaded = data["totalCurrent"];
            }
            catch (err) {
                error = err;
                winston.warn(`attempt ${attempt}: unable to process ${url}`, { source: "fptshop" });
            }
            if (!error) {
                if (links.length > 0) {
                    winston.info(`crawled ${url}`, { source: "fptshop" });
                }
                else {
                    winston.warn(`no link at ${url}`, { source: "fptshop" });
                }
                return {
                    links: links,
                    next: next,
                    loaded: loaded
                };
            }
            if (error instanceof Core.RequestError) {
                var requestError = error;
                if (requestError.code === 504) {
                    maxAttemp++;
                    winston.warn(`code=504 ${url}`, { source: "fptshop" });
                }
            }
            yield promise_1.wait(5000);
            attempt++;
        }
        throw new error_1.OperationError("Failed to load " + url);
    });
}
function getProduct(url) {
    return __awaiter(this, void 0, void 0, function* () {
        var attempt = 1;
        var product;
        var error;
        while (attempt <= 3) {
            error = null;
            try {
                yield Core.browse({
                    url: url,
                    src: [Core.umbrellaSource]
                }, (window) => __awaiter(this, void 0, void 0, function* () {
                    product = yield extraPageInfo(window);
                }));
            }
            catch (err) {
                if (err instanceof error_1.ArgumentError) {
                    throw err;
                }
                error = err;
                winston.warn(`attempt ${attempt}: unable to process ${url}`, { source: "fptshop" });
            }
            if (!error) {
                break;
            }
            yield promise_1.wait(5000);
            attempt++;
        }
        if (error) {
            throw new error_1.OperationError("Failed to load " + url);
        }
        winston.info(`crawled ${product.name}`, { source: "fptshop", url: url });
        return product;
    });
}
exports.getProduct = getProduct;
function extraPageInfo(window) {
    return __awaiter(this, void 0, void 0, function* () {
        var u = window["u"];
        var location = window.location;
        if (u(".detail-name").length === 0) {
            throw new error_1.ArgumentError(`no product is in ${window.location.href}`);
        }
        var product = {
            url: u("link[rel=canonical]").nodes[0]["href"],
            name: u(".detail-name").text().trim(),
            source: "fptshop",
            priceUnit: "VND",
            prices: [{ date: null, value: parseInt(u(".chosen-color").data("price")) }],
            category: categoryMap["/" + location.pathname.split("/")[1]] || 0,
            media: yield getImages(window),
            specification: getSpecification(window),
            description: getDescription(window),
            groupId: null,
            createdDate: null,
            updatedDate: null
        };
        return product;
    });
}
function getSpecification(window) {
    var u = window["u"];
    var uspecTag = u(".detail-specification");
    if (!uspecTag.length) {
        return {};
    }
    var spec = {};
    //extractGroup($specTag.find(".detail-main-specification"));
    extractGroup(uspecTag.find(".more-specification"));
    return spec;
    function extractGroup(element) {
        var head = "#";
        var $lis = element.find("li");
        $lis.each(function (li) {
            var $li = u(li);
            if ($li.hasClass("specificationheader")) {
                head = $li.text().trim();
                spec[head] = {};
            }
            else {
                var key = $li.find("label").text().trim().replace(/\$|\./g, "");
                var value = $li.find("span").text().trim();
                spec[head][key] = value;
            }
        });
    }
}
function getDescription(window) {
    var u = window["u"];
    var description = "";
    var $features = u(".detail-highlight-item").not(".swiper-slide-duplicate");
    $features.each(function (feature) {
        var $feature = u(feature);
        description += "<h3>" + $feature.find(".detail-title").text().trim() + "</h3>";
        description += "<p>" + $feature.find("p").text().trim() + "</p>";
        description += `<img src="${Core.normalizeURL($feature.find("img").data("original"), "fptshop.com.vn")}"/>`;
    });
    return description;
}
function getImages(window) {
    return __awaiter(this, void 0, void 0, function* () {
        var u = window["u"];
        var $color = u(".chosen-color");
        var media = [];
        for (let index = 0; index < $color.length; index++) {
            let $colorVariant = u($color.nodes[index]);
            let id = $colorVariant.data("id");
            try {
                let data = (yield Core.get("http://fptshop.com.vn/Ajax/Product/GetPicturesProductVariant", { id: id }));
                let $imgs = u(data.toString("utf-8")).find("img");
                if ($imgs.length > 0) {
                    $imgs.each(function (img) {
                        var imgSrc = Core.normalizeURL(u(img).data("large-src"), "fptshop.com.vn");
                        media.push({
                            type: 2,
                            url: imgSrc
                        });
                    });
                }
                else {
                    var imgSrc = u("#slider-image img");
                    if (imgSrc.length === 0) {
                        imgSrc = u("#default_image");
                    }
                    media.push({
                        type: 2,
                        url: imgSrc.nodes[0]["src"]
                    });
                }
            }
            catch (error) {
            }
        }
        return media;
    });
}
function getAds() {
    return __awaiter(this, void 0, void 0, function* () {
        var ads = [];
        yield Core.browse({
            url: "http://fptshop.com.vn/",
            src: [Core.umbrellaSource],
        }, function (window) {
            return __awaiter(this, void 0, void 0, function* () {
                var u = window["u"];
                var items = u("#fowl1 a");
                items.each(function (item) {
                    var uitem = u(item);
                    var ad = {
                        source: "fptshop",
                        url: uitem.nodes[0]["href"],
                        image: uitem.find("img").nodes[0]["src"]
                    };
                    ads.push(ad);
                });
            });
        });
        winston.info(`crawled ${ads.length} ads`, { source: "fptshop" });
        return ads;
    });
}
exports.getAds = getAds;
var categoryMap = {
    "/dien-thoai": 1,
    "/may-tinh-bang": 2,
    "/may-tinh-xach-tay": 3,
};
//# sourceMappingURL=fptshop.js.map