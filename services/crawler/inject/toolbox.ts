export function toolbox(page) {
    return {
        wait: function (predicate : Function, maxTime : number, callback : (err? : Error) => void) {
            var interval = 100;
            var wastedTime = 0;
            var intervalId = setInterval(function () {
                wastedTime += interval;
                console.log(wastedTime);
                var hasBeenFound = page.evaluate(predicate);
                if (hasBeenFound === false
                    && wastedTime < maxTime) {
                    // wait for next interval
                    return;
                }

                clearInterval(intervalId);

                if (hasBeenFound) {
                    callback();
                } else {
                    callback(Error("timeout"));
                }
            }, interval);
        }
    }
}
