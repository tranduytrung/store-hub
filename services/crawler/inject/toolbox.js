"use strict";
function toolbox(page) {
    return {
        wait: function (predicate, maxTime, callback) {
            var interval = 100;
            var wastedTime = 0;
            var intervalId = setInterval(function () {
                wastedTime += interval;
                console.log(wastedTime);
                var hasBeenFound = page.evaluate(predicate);
                if (hasBeenFound === false
                    && wastedTime < maxTime) {
                    // wait for next interval
                    return;
                }
                clearInterval(intervalId);
                if (hasBeenFound) {
                    callback();
                }
                else {
                    callback(Error("timeout"));
                }
            }, interval);
        }
    };
}
exports.toolbox = toolbox;
//# sourceMappingURL=toolbox.js.map