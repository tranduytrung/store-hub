"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const Core = require("./core");
const error_1 = require("../../error");
const _ = require("lodash");
const winston = require("winston");
const promise_1 = require("../../utils/promise");
const config_1 = require("../../config");
function getAllProducts(callback) {
    return __awaiter(this, void 0, void 0, function* () {
        for (var index = 0; index < pageToCrawl.length; index++) {
            var url = pageToCrawl[index];
            var links = yield getLinksFromPage(url);
            yield promise_1.parallelEach(links, function (link) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        var product = yield getProduct(link);
                        yield callback(product);
                        global.gc();
                    }
                    catch (error) {
                        var message = (_.isString(error) ? error : _.get(error, "message")) || "unknown error";
                        winston.error(message, { source: "tiki", detail: error });
                    }
                });
            }, { limit: config_1.jsdom.maxConcurrency });
        }
    });
}
exports.getAllProducts = getAllProducts;
function getLinksFromPage(url) {
    return __awaiter(this, void 0, Promise, function* () {
        var attempt = 1;
        while (attempt <= 3) {
            var links;
            var error = null;
            try {
                yield Core.browse({
                    url: url,
                    src: [Core.umbrellaSource],
                }, (window) => __awaiter(this, void 0, void 0, function* () {
                    links = getLinks(window);
                }), { force: true });
            }
            catch (err) {
                error = err;
                winston.warn(`attempt ${attempt}: unable to process ${url}`, { source: "tiki" });
            }
            if (!error) {
                if (links.length > 0) {
                    winston.info(`crawled ${url}`, { source: "tiki" });
                }
                else {
                    winston.warn(`no link at ${url}`, { source: "tiki" });
                }
                return links;
            }
            yield promise_1.wait(5000);
            attempt++;
        }
        throw new error_1.OperationError("Failed to load " + url);
        function getLinks(window) {
            var u = window["u"];
            return _.map(u(".product-item>a").nodes, "href");
        }
    });
}
function getProduct(url) {
    return __awaiter(this, void 0, Promise, function* () {
        var attempt = 1;
        var product;
        var error;
        while (attempt <= 3) {
            error = null;
            try {
                yield Core.browse({
                    url: url,
                    src: [Core.umbrellaSource]
                }, (window) => __awaiter(this, void 0, void 0, function* () {
                    product = extraPageInfo(window);
                }));
            }
            catch (err) {
                if (err instanceof error_1.ArgumentError) {
                    throw err;
                }
                error = err;
                winston.warn(`attempt ${attempt}: unable to process ${url}`, { source: "tiki" });
            }
            if (!error) {
                break;
            }
            yield promise_1.wait(5000);
            attempt++;
        }
        if (error) {
            throw new error_1.OperationError("Failed to load " + url);
        }
        winston.info(`crawled ${product.name}`, { source: "tiki", url: url });
        return product;
    });
}
exports.getProduct = getProduct;
function extraPageInfo(window) {
    var u = window["u"];
    var ucategory = u("#productset_id");
    if (ucategory.length === 0) {
        throw new error_1.ArgumentError(`no product is in ${window.location.href}`);
    }
    var product = {
        category: categoryMap[ucategory.nodes[0]["value"]] || 0,
        createdDate: null,
        description: getDescription(window),
        groupId: null,
        media: getMedia(window),
        name: u(".item-name").text().trim(),
        priceUnit: "VND",
        prices: [{ date: null, value: Number(u("#product_price").nodes[0]["value"]) }],
        source: "tiki",
        url: u("link[rel=canonical]").nodes[0]["href"],
        updatedDate: null,
        specification: getSpecification(window)
    };
    return product;
}
const imagesRegex = /images\s*=\s*(\[.*?\])/;
function getMedia(window) {
    var u = window["u"];
    var scripts = u("script:not([src])");
    for (var index = 0; index < scripts.length; index++) {
        var script = scripts.nodes[index];
        var matches = script.innerHTML.match(imagesRegex);
        if (!matches || matches.length !== 2) {
            continue;
        }
        var data = JSON.parse(matches[1]);
        return _.map(data, function (item) {
            return {
                type: 2,
                url: item["href"]
            };
        });
    }
    return [];
}
function getSpecification(window) {
    var u = window["u"];
    var $table = u("#chi-tiet");
    // if no chi tiet
    if ($table.length === 0)
        return {};
    // go throught every row
    var $rows = $table.find("tr");
    var generalSpec = {};
    $rows.each(function (row) {
        var $row = u(row);
        var $cols = $row.find("td");
        // skip strange param
        if ($cols.length !== 2)
            return;
        var key = u($cols.nodes[0]).text().trim().replace(/\$|\./g, "");
        var value = u($cols.nodes[1]).text().trim();
        generalSpec[key] = value;
    });
    return {
        "Thông tin sản phẩm": generalSpec
    };
}
function getDescription(window) {
    var u = window["u"];
    var description = "";
    var $description = u("#gioi-thieu");
    // Note: p usually follows with a table with did bot have any plan to process yet
    var $ps = $description.find("p");
    $ps.each(function (p) {
        var $p = u(p);
        var contents = p.childNodes;
        var nChildren = contents.length;
        if (nChildren === 1 && contents[0].nodeName === "STRONG") {
            var text = $p.text().trim();
            description += `<h3>${text}</h3>`;
            return;
        }
        if (nChildren === 1 && contents[0].nodeName === "SPAN") {
            description += `<h2>${$p.text().trim()}</h2>`;
            return;
        }
        if (nChildren === 1 && contents[0].nodeName === "IMG") {
            description += `<img src="${$p.children().first()["src"]}"/>`;
            return;
        }
        description += `<p>${$p.text().trim()}</p>`;
    });
    return description;
}
function getAds() {
    return __awaiter(this, void 0, void 0, function* () {
        var ads = [];
        yield Core.browse({
            url: "https://tiki.vn/",
            src: [Core.umbrellaSource],
        }, function (window) {
            return __awaiter(this, void 0, void 0, function* () {
                var u = window["u"];
                var items = u(".home-slideshow-content .swiper-slide");
                items.each(function (item) {
                    var uitem = u(item);
                    var img = uitem.find("img");
                    var ad = {
                        source: "tiki",
                        url: uitem.find("a").nodes[0]["href"],
                        image: img.nodes[0]["src"] || img.data("src")
                    };
                    ads.push(ad);
                });
            });
        });
        winston.info(`crawled ${ads.length} ads`, { source: "tiki" });
        return ads;
    });
}
exports.getAds = getAds;
var categoryMap = {
    62: 0,
    58: 1,
    48: 2,
    53: 3
};
var pageToCrawl = [
    "https://tiki.vn/dien-thoai-di-dong/c1793?limit=1000000000",
    "https://tiki.vn/may-tinh-bang/c1794?limit=1000000000",
    "https://tiki.vn/laptop/c2742?limit=1000000000",
    "https://tiki.vn/macbook-imac/c2458?limit=1000000000",
];
//# sourceMappingURL=tiki.js.map