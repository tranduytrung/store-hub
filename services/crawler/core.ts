import * as fs from "fs"
import * as jsdom from "jsdom"
import {IProduct} from "../../models/product"
import {jsdom as jsdomConfig} from "../../config"
import {Semaphore} from "../../utils/promise"
import * as _ from "lodash"
import * as Wreck from "wreck"
import * as Uri from "urijs";

export var umbrellaSource = fs.readFileSync("services/crawler/inject/umbrella.min.js", "utf8")

export var defaultWindow = jsdom.jsdom(undefined, {
    features: {
        FetchExternalResources: false,
        ProcessExternalResources: ["script"]
    }
}).defaultView;

function initDefaultWindow() {
    return new Promise(function (resolve, reject) {
        var document = defaultWindow.document;
        var el = document.createElement("script");
        el.onload = function () {
            resolve();
        }

        el.onerror = function () {
            reject(new Error("unable to load umbrella"));
        }

        el.text = umbrellaSource;
        document.body.appendChild(el);
    });
}

export interface IOnEachProductCallback {
    (product: IProduct): Promise<void>
}

export interface IOnSavingContextCallback {
    (context): Promise<void>
}

interface IBrowseOptions {
    force?: boolean
}

export function init() {
    return initDefaultWindow();
}

export class RequestError extends Error {
    constructor(message : string, public code : number) {
        super(message);
    }
}

export function browse(jsdomConfig: jsdom.Config, processCallback: (window: Window) => Promise<void>, options?: IBrowseOptions): Promise<void> {
    return new Promise<void>(function (resolve, reject) {
        var config = _.extend({}, jsdomConfig, {
            done: function (errors, window) {
                if (errors) {
                    return reject(errors);
                }

                processCallback(window).then(resolve, reject).then(function () {
                    if (window) {
                        var u: IUmbrellaSelector = window["u"];
                        u("script").remove();
                        window.close();
                    }
                });
            }
        });

        jsdom.env(config);
        config = null;
    });
}

export function get(uri: string, payload?: Object) {
    if (payload) {
        uri = Uri(uri).search(payload).toString()
    }

    return new Promise<Buffer | Object>(function (resolve, reject) {
        Wreck.get(uri,
            {
                timeout: 10000,
                json: true
            },
            function (error, response, payload) {
                if (error) {
                    return reject(error);
                }
                
                if (response.statusCode >= 400) {
                    return reject(new RequestError(response.statusMessage, response.statusCode))
                }

                resolve(payload);
            });
    });
}

export function post(uri: string, payload: Object, options?: IRequestOptions) {
    return new Promise<Buffer | Object>(function (resolve, reject) {
        options = _.defaults<IRequestOptions, IRequestOptions>(options, {
            wwwFormUrlEncoded: false
        });

        var payloadAsString: string;
        var headers: Object;
        if (options.wwwFormUrlEncoded) {
            payloadAsString = Uri.buildQuery(payload);
            headers = {
                "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
            };
        } else {
            payloadAsString = JSON.stringify(payload);
            headers = {
                "Content-Type": "application/json"
            };
        }

        Wreck.post(uri,
            {
                timeout: 10000,
                json: true,
                payload: payloadAsString,
                headers
            },
            function (error, response, payload) {
                if (error) {
                    return reject(error);
                }
                
                if (response.statusCode >= 400) {
                    return reject(new RequestError(response.statusMessage, response.statusCode))
                }

                resolve(payload);
            });
    });
}

interface IRequestOptions {
    wwwFormUrlEncoded: boolean
}

export function normalizeURL(url : string, host : string, protocol : string = "http") {
    var uri = Uri(url);
    if (_.isEmpty(uri.host())) {
        uri.host(host);
    }

    if (_.isEmpty(uri.protocol())) {
        uri.protocol(protocol);
    }

    return uri.toString();
}