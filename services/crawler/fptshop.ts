import * as Core from "./core"
import { IProduct, IProductMedia } from "../../models/product"
import { IAd } from "../../models/ad"
import { ArgumentError, OperationError } from "../../error"
import * as _ from "lodash"
import * as winston from "winston"
import { wait, parallelEach } from "../../utils/promise"
import {jsdom as jsdomConfig} from "../../config"

export async function getAllProducts(callback: Core.IOnEachProductCallback) {
    for (let path in categoryMap) {
        var loaded = 1;
        do {
            var query = `http://fptshop.com.vn/Ajax/FilterProduct/ViewMore?page=${loaded}&url=http://fptshop.com.vn${path}&typeView=Hot`;
            var result = await getLinksFromPage(query);

            await parallelEach(result.links, async function (link) {
                try {
                    var product = await getProduct(link);
                    await callback(product);
                    global.gc();
                } catch (error) {
                    var message = (_.isString(error) ? error : _.get(error, "message")) || "unknown error";
                    winston.error(message, { source: "fptshop", detail: error });
                }
            }, { limit: jsdomConfig.maxConcurrency });

            loaded = result.loaded;
        } while (result.next)
    }
}

interface IGetLinksResult {
    links: string[]
    next: boolean
    loaded: number
}

async function getLinksFromPage(url: string): Promise<IGetLinksResult> {
    var window = Core.defaultWindow;
    var u: IUmbrellaSelector = window["u"];

    var maxAttemp = 3;
    var attempt = 1;
    while (attempt <= maxAttemp) {
        var links: string[] = [];
        var next: boolean;
        var loaded: number;
        var error: any = null;
        try {
            var data = await Core.get(url);
            var ulinks = u(data["content"]).find("a.p-link-prod,div.fshop-lplap-item >a[href]");
            ulinks.each(function (linkTag) {
                var href: string = linkTag["href"];
                links.push(href.startsWith("http") ? href : "http://fptshop.com.vn" + href);
            });

            next = data["next"];
            loaded = data["totalCurrent"];
        } catch (err) {
            error = err;
            winston.warn(`attempt ${attempt}: unable to process ${url}`, { source: "fptshop" });
        }

        if (!error) {
            if (links.length > 0) {
                winston.info(`crawled ${url}`, { source: "fptshop" });
            } else {
                winston.warn(`no link at ${url}`, { source: "fptshop" });
            }

            return {
                links,
                next,
                loaded
            };
        }

        if (error instanceof Core.RequestError) {
            var requestError = <Core.RequestError>error;
            if (requestError.code === 504) {
                maxAttemp++;
                winston.warn(`code=504 ${url}`, { source: "fptshop" });
            }
        }

        await wait(5000);
        attempt++;
    }

    throw new OperationError("Failed to load " + url);
}

export async function getProduct(url: string) {
    var attempt = 1;
    var product: IProduct;
    var error: any;

    while (attempt <= 3) {
        error = null;
        try {
            await Core.browse({
                url: url,
                src: [Core.umbrellaSource]
            }, async (window) => {
                product = await extraPageInfo(window);
            });
        } catch (err) {
            if (err instanceof ArgumentError) {
                throw err;
            }

            error = err;
            winston.warn(`attempt ${attempt}: unable to process ${url}`, { source: "fptshop" });
        }

        if (!error) {
            break;
        }

        await wait(5000);
        attempt++;
    }

    if (error) {
        throw new OperationError("Failed to load " + url);
    }

    winston.info(`crawled ${product.name}`, { source: "fptshop", url: url });
    return product;
}

async function extraPageInfo(window: Window) {
    var u: IUmbrellaSelector = window["u"];
    var location = window.location;

    if (u(".detail-name").length === 0) {
        throw new ArgumentError(`no product is in ${window.location.href}`);
    }

    var product: IProduct = {
        url: u("link[rel=canonical]").nodes[0]["href"],
        name: u(".detail-name").text().trim(),
        source: "fptshop",
        priceUnit: "VND",
        prices: [{ date: null, value: parseInt(u(".chosen-color").data("price")) }],
        category: categoryMap["/" + location.pathname.split("/")[1]] || 0,
        media: await getImages(window),
        specification: getSpecification(window),
        description: getDescription(window),
        groupId: null,
        createdDate: null,
        updatedDate: null
    }

    return product;
}

function getSpecification(window: Window) {
    var u: IUmbrellaSelector = window["u"];
    var uspecTag = u(".detail-specification");
    if (!uspecTag.length) {
        return {};
    }

    var spec = {};
    //extractGroup($specTag.find(".detail-main-specification"));
    extractGroup(uspecTag.find(".more-specification"));

    return spec;

    function extractGroup(element: umbrella.IUmbrellaNodes) {
        var head = "#";
        var $lis = element.find("li");
        $lis.each(function (li) {
            var $li = u(li);
            if ($li.hasClass("specificationheader")) {
                head = $li.text().trim();
                spec[head] = {};
            } else {
                var key = $li.find("label").text().trim().replace(/\$|\./g, "");
                var value = $li.find("span").text().trim();
                spec[head][key] = value;
            }
        });
    }
}

function getDescription(window: Window) {
    var u: IUmbrellaSelector = window["u"];

    var description: string = "";
    var $features = u(".detail-highlight-item").not(".swiper-slide-duplicate");

    $features.each(function (feature) {
        var $feature = u(feature);
        description += "<h3>" + $feature.find(".detail-title").text().trim() + "</h3>";
        description += "<p>" + $feature.find("p").text().trim() + "</p>";
        description += `<img src="${Core.normalizeURL($feature.find("img").data("original"), "fptshop.com.vn")}"/>`;
    });

    return description;
}

async function getImages(window: Window) {
    var u: IUmbrellaSelector = window["u"];
    var $color = u(".chosen-color");
    var media: IProductMedia[] = [];

    for (let index = 0; index < $color.length; index++) {
        let $colorVariant = u($color.nodes[index]);
        let id = $colorVariant.data("id");

        try {
            let data = <Buffer>(await Core.get("http://fptshop.com.vn/Ajax/Product/GetPicturesProductVariant", { id: id }));

            let $imgs = u(data.toString("utf-8")).find("img");
            if ($imgs.length > 0) {
                $imgs.each(function (img) {
                    var imgSrc = Core.normalizeURL(u(img).data("large-src"), "fptshop.com.vn");
                    media.push({
                        type: 2,
                        url: imgSrc
                    });
                });
            } else {
                var imgSrc = u("#slider-image img");
                if (imgSrc.length === 0) {
                    imgSrc = u("#default_image");
                }

                media.push({
                    type: 2,
                    url: imgSrc.nodes[0]["src"]
                });
            }
        } catch (error) {

        }
    }

    return media;
}

export async function getAds() {
    var ads: IAd[] = [];

    await Core.browse({
        url: "http://fptshop.com.vn/",
        src: [Core.umbrellaSource],
    }, async function (window) {
        var u: IUmbrellaSelector = window["u"];

        var items = u("#fowl1 a");
        items.each(function (item) {
            var uitem = u(item);
            var ad: IAd = {
                source: "fptshop",
                url: uitem.nodes[0]["href"],
                image: uitem.find("img").nodes[0]["src"]
            };
            ads.push(ad);
        });
    });

    winston.info(`crawled ${ads.length} ads`, { source: "fptshop" });
    return ads;
}

var categoryMap = {
    "/dien-thoai": 1,
    "/may-tinh-bang": 2,
    "/may-tinh-xach-tay": 3,
    //"/phu-kien": 0
}