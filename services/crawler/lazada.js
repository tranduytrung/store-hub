"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const Core = require("./core");
const error_1 = require("../../error");
const _ = require("lodash");
const winston = require("winston");
const promise_1 = require("../../utils/promise");
const config_1 = require("../../config");
function getAllProducts(callback, saveContext, categories = pageToCrawl, context) {
    return __awaiter(this, void 0, void 0, function* () {
        context = _.defaults(context, {
            categoryIndex: 0,
            pageIndex: 0
        });
        // loop through the category page list
        while (context.categoryIndex < categories.length) {
            var url = categories[context.categoryIndex];
            do {
                // build the request url
                var query = url + `?itemperpage=120&page=${context.pageIndex}`;
                // get product list of the page
                var links = yield getLinksFromPage(query);
                // parallel block
                yield promise_1.parallelEach(links, function (link) {
                    return __awaiter(this, void 0, void 0, function* () {
                        try {
                            // get product information in parallel
                            var product = yield getProduct(link);
                            // notify the crawled product
                            yield callback(product);
                            // clean up memory to prevent from overloading which cause application to crash
                            global.gc();
                        }
                        catch (error) {
                            var message = (_.isString(error) ? error : _.get(error, "message")) || "unknown error";
                            winston.error(message, { source: "lazada", detail: error });
                        }
                    });
                }, { limit: config_1.jsdom.maxConcurrency }); // set the parallel limitation
                context.pageIndex++;
                yield saveContext(context);
            } while (links.length > 0);
            context.pageIndex = 0;
            context.categoryIndex++;
            yield saveContext(context);
        }
        context.pageIndex = 0;
        context.categoryIndex = 0;
    });
}
exports.getAllProducts = getAllProducts;
function getLinksFromPage(url) {
    return __awaiter(this, void 0, Promise, function* () {
        var attempt = 1;
        while (attempt <= 3) {
            var links;
            var error = null;
            try {
                yield Core.browse({
                    url: url,
                    src: [Core.umbrellaSource],
                }, (window) => __awaiter(this, void 0, void 0, function* () {
                    links = getLinks(window);
                }), { force: true });
            }
            catch (err) {
                error = err;
                winston.warn(`attempt ${attempt}: unable to process ${url}`, { source: "lazada" });
            }
            if (!error) {
                if (links.length > 0) {
                    winston.info(`crawled ${url}`, { source: "lazada" });
                }
                else {
                    winston.warn(`no link at ${url}`, { source: "lazada" });
                }
                return links;
            }
            yield promise_1.wait(5000);
            attempt++;
        }
        throw new error_1.OperationError("Failed to load " + url);
        function getLinks(window) {
            var u = window["u"];
            //.outofstock is in stock
            //.outofstock1 is out of stock
            return _.map(u("div.product-card.outofstock").nodes, function (item) {
                return Core.normalizeURL(u(item).data("original"), "www.lazada.vn");
            });
        }
    });
}
function getProduct(url) {
    return __awaiter(this, void 0, Promise, function* () {
        var attempt = 1;
        var product;
        var error;
        while (attempt <= 3) {
            error = null;
            try {
                yield Core.browse({
                    url: url,
                    src: [Core.umbrellaSource]
                }, (window) => __awaiter(this, void 0, void 0, function* () {
                    product = extraPageInfo(window);
                }));
            }
            catch (err) {
                if (err instanceof error_1.ArgumentError) {
                    throw err;
                }
                error = err;
                winston.warn(`attempt ${attempt}: unable to process ${url}`, { source: "lazada" });
            }
            if (!error) {
                break;
            }
            yield promise_1.wait(5000);
            attempt++;
        }
        if (error) {
            throw new error_1.OperationError("Failed to load " + url);
        }
        winston.info(`crawled ${product.name}`, { source: "lazada", url: url });
        return product;
    });
}
exports.getProduct = getProduct;
function extraPageInfo(window) {
    var u = window["u"];
    var uname = u("#prod_title");
    if (uname.length === 0) {
        throw new error_1.ArgumentError(`no product is in ${window.location.href}`);
    }
    var product = {
        category: getCategory(window),
        createdDate: null,
        description: getDescription(window),
        groupId: null,
        media: getMedia(window),
        name: uname.text().trim(),
        priceUnit: "VND",
        prices: [{ date: null, value: getPrice(window) }],
        source: "lazada",
        url: u("link[rel=canonical]").first()["href"],
        updatedDate: null,
        specification: getSpecification(window),
    };
    return product;
}
function getPrice(window) {
    var u = window["u"];
    if (u("#AddToCart").length === 0) {
        return null;
    }
    var priceBox = u("#product-price-box");
    var price = Number(priceBox.find("#product_price").text());
    return price;
}
function getCategory(window) {
    var u = window["u"];
    var category = 0;
    _.forEachRight(u(".breadcrumb__item").nodes, function (tag) {
        var key = u(tag).text().trim().toLowerCase();
        var value = categoryMap[key];
        if (value) {
            category = value;
            return false;
        }
    });
    return category;
}
function getMedia(window) {
    var u = window["u"];
    var imgDivs = u(".prd-moreImages .productImage");
    var media = _.map(imgDivs.nodes, function (item) {
        var uitem = u(item);
        return {
            type: 2,
            url: Core.normalizeURL(uitem.data("big"), "www.lazada.vn")
        };
    });
    // thumbnail
    if (imgDivs.length > 0) {
        media.push({
            type: 1,
            url: Core.normalizeURL(imgDivs.data("swap-image"), "www.lazada.vn")
        });
    }
    // video
    var videoTag = u("#product-video-link");
    if (videoTag.length !== 0) {
        media.push({
            type: 3,
            url: Core.normalizeURL(videoTag.first()["href"], "www.lazada.vn")
        });
    }
    return media;
}
function getSpecification(window) {
    var u = window["u"];
    var spec = {};
    const generalKey = "Thông tin sản phẩm";
    spec[generalKey] = {};
    var inbox = u(".product-description__inbox");
    if (inbox.length !== 0) {
        let items = _.map(inbox.find(".inbox__item").nodes, function (item) {
            var uitem = u(item);
            return uitem.text().trim();
        });
        spec[generalKey]["Sản phẩm bao gồm"] = items.join("\n");
    }
    var table = u(".specification-table");
    if (table.length !== 0) {
        var props = table.find("tr");
        props.each(function (prop, i) {
            var uprop = u(prop);
            var ucoms = uprop.find("td");
            // skip if not as expected
            if (ucoms.length !== 2)
                return;
            var key = u(ucoms.nodes[0]).text().trim().replace(/\$|\./g, "");
            var value = u(ucoms.nodes[1]).text().trim();
            spec[generalKey][key] = value;
        });
    }
    return spec;
}
function getDescription(window) {
    var u = window["u"];
    function parse(element) {
        var str = "";
        var uitems = element.children();
        uitems.each(function (item) {
            var uitem = u(item);
            switch (item.nodeName) {
                case "H2":
                    str += `<h2>${uitem.text().trim()}</h2>`;
                    break;
                case "P":
                case "DIV":
                    if (uitem.find("img").length > 0) {
                        str += parse(uitem);
                    }
                    else {
                        let contents = uitem.children();
                        if (contents.length === 1 && (contents.nodes[0].nodeName === "B" || contents.nodes[0].nodeName === "STRONG")) {
                            str += `<h3>${uitem.text().trim()}</h3>`;
                        }
                        else {
                            str += `<p>${uitem.text().trim()}</p>`;
                        }
                    }
                    break;
                case "A":
                    var uimg = uitem.children("img");
                    if (uimg.length > 0) {
                        let imageUrl = uimg.data("original");
                        if (!_.isEmpty(imageUrl)) {
                            str += `<img src="${imageUrl}">`;
                        }
                    }
                    else {
                        str += `<a href="${uitem.first()["href"]}">${uitem.text().trim()}</a>`;
                    }
                    break;
                case "UL":
                    uitem.find("li").each(function (li) {
                        str += `<div>${u(li).text().trim()}</div>`;
                    });
                    break;
                case "IMG":
                    let imageUrl = uitem.data("original");
                    if (!_.isEmpty(imageUrl)) {
                        str += `<img src="${Core.normalizeURL(imageUrl, "www.lazada.vn")}">`;
                    }
                    break;
            }
        });
        return str;
    }
    var udescription = u("#productDetails >div");
    return parse(udescription);
}
function getAds() {
    return __awaiter(this, void 0, void 0, function* () {
        var ads = [];
        yield Core.browse({
            url: "http://www.lazada.vn/",
            src: [Core.umbrellaSource],
        }, function (window) {
            return __awaiter(this, void 0, void 0, function* () {
                var u = window["u"];
                var items = u(".c-mp-tabs-banner__tab-body[data-tab-id='1'] .c-promo-grid__cell");
                items.each(function (item) {
                    var uitem = u(item);
                    var uimage = uitem.find(".c-banner__background");
                    var uparams = JSON.parse(uimage.attr("data-js-component-params"));
                    if (!(uparams && uparams.src))
                        return;
                    var ad = {
                        source: "lazada",
                        url: uitem.find("a").nodes[0]["href"],
                        image: uparams.src
                    };
                    ads.push(ad);
                });
            });
        });
        winston.info(`crawled ${ads.length} ads`, { source: "lazada" });
        return ads;
    });
}
exports.getAds = getAds;
var categoryMap = {
    "điện thoại di động": 1,
    "máy tính bảng": 2,
    "laptop": 3
};
var pageToCrawl = [
    "http://www.lazada.vn/dien-thoai-di-dong",
    "http://www.lazada.vn/laptop",
    "http://www.lazada.vn/may-tinh-bang"
];
//# sourceMappingURL=lazada.js.map