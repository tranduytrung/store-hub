import * as Core from "./core"
import { IProduct, IProductMedia } from "../../models/product"
import { IAd } from "../../models/ad"
import { ArgumentError, OperationError } from "../../error"
import * as _ from "lodash"
import * as winston from "winston"
import { wait, parallelEach } from "../../utils/promise"
import {jsdom as jsdomConfig} from "../../config"

export async function getAllProducts(callback: Core.IOnEachProductCallback) {
    for (var index = 0; index < pageToCrawl.length; index++) {
        var url = pageToCrawl[index];
        var links = await getLinksFromPage(url);
        await parallelEach(links, async function (link) {
            try {
                var product = await getProduct(link);
                await callback(product);
                global.gc();
            } catch (error) {
                var message = (_.isString(error) ? error : _.get(error, "message")) || "unknown error";
                winston.error(message, { source: "tiki", detail: error });
            }
        }, { limit: jsdomConfig.maxConcurrency });
    }
}

async function getLinksFromPage(url: string): Promise<string[]> {
    var attempt = 1;
    while (attempt <= 3) {
        var links: string[];
        var error: any = null;
        try {
            await Core.browse({
                url: url,
                src: [Core.umbrellaSource],
            }, async (window) => {
                links = getLinks(window);
            }, { force: true });
        } catch (err) {
            error = err;
            winston.warn(`attempt ${attempt}: unable to process ${url}`, { source: "tiki" });
        }

        if (!error) {
            if (links.length > 0) {
                winston.info(`crawled ${url}`, { source: "tiki" });
            } else {
                winston.warn(`no link at ${url}`, { source: "tiki" });
            }

            return links;
        }

        await wait(5000);
        attempt++;
    }

    throw new OperationError("Failed to load " + url);

    function getLinks(window: Window): string[] {
        var u: IUmbrellaSelector = window["u"];
        return _.map<HTMLElement, string>(u(".product-item>a").nodes, "href");
    }
}

export async function getProduct(url: string): Promise<IProduct> {
    var attempt = 1;
    var product: IProduct;
    var error: any;

    while (attempt <= 3) {
        error = null;
        try {
            await Core.browse({
                url: url,
                src: [Core.umbrellaSource]
            }, async (window) => {
                product = extraPageInfo(window);
            });
        } catch (err) {
            if (err instanceof ArgumentError) {
                throw err;
            }

            error = err;
            winston.warn(`attempt ${attempt}: unable to process ${url}`, { source: "tiki" });
        }

        if (!error) {
            break;
        }

        await wait(5000);
        attempt++;
    }

    if (error) {
        throw new OperationError("Failed to load " + url);
    }

    winston.info(`crawled ${product.name}`, { source: "tiki", url: url });
    return product;
}

function extraPageInfo(window: Window) {
    var u: IUmbrellaSelector = window["u"];
    var ucategory = u("#productset_id");

    if (ucategory.length === 0) {
        throw new ArgumentError(`no product is in ${window.location.href}`);
    }

    var product: IProduct = {
        category: categoryMap[ucategory.nodes[0]["value"]] || 0,
        createdDate: null,
        description: getDescription(window),
        groupId: null,
        media: getMedia(window),
        name: u(".item-name").text().trim(),
        priceUnit: "VND",
        prices: [{ date: null, value: Number(u("#product_price").nodes[0]["value"]) }],
        source: "tiki",
        url: u("link[rel=canonical]").nodes[0]["href"],
        updatedDate: null,
        specification: getSpecification(window)
    };

    return product;
}

const imagesRegex = /images\s*=\s*(\[.*?\])/;
function getMedia(window: Window): IProductMedia[] {
    var u: IUmbrellaSelector = window["u"];

    var scripts = u("script:not([src])");

    for (var index = 0; index < scripts.length; index++) {
        var script = scripts.nodes[index];

        var matches = script.innerHTML.match(imagesRegex);
        if (!matches || matches.length !== 2) {
            continue;
        }

        var data = JSON.parse(matches[1]);
        return _.map<{}, IProductMedia>(data, function (item) {
            return {
                type: 2,
                url: item["href"]
            };
        });
    }

    return [];
}

function getSpecification(window: Window): Object {
    var u: IUmbrellaSelector = window["u"];

    var $table = u("#chi-tiet");
    // if no chi tiet
    if ($table.length === 0) return {};

    // go throught every row
    var $rows = $table.find("tr");
    var generalSpec = {};
    $rows.each(function (row) {
        var $row = u(row);
        var $cols = $row.find("td");
        // skip strange param
        if ($cols.length !== 2) return;
        var key = u($cols.nodes[0]).text().trim().replace(/\$|\./g, "")
        var value = u($cols.nodes[1]).text().trim();
        generalSpec[key] = value;
    });

    return {
        "Thông tin sản phẩm": generalSpec
    };
}

function getDescription(window: Window) {
    var u: IUmbrellaSelector = window["u"];
    var description = "";

    var $description = u("#gioi-thieu");

    // Note: p usually follows with a table with did bot have any plan to process yet
    var $ps = $description.find("p");
    $ps.each(function (p) {
        var $p = u(p)
        var contents = p.childNodes;
        var nChildren = contents.length;
        if (nChildren === 1 && contents[0].nodeName === "STRONG") {
            var text = $p.text().trim();
            description += `<h3>${text}</h3>`;
            return;
        }

        if (nChildren === 1 && contents[0].nodeName === "SPAN") {
            description += `<h2>${$p.text().trim()}</h2>`;
            return;
        }

        if (nChildren === 1 && contents[0].nodeName === "IMG") {
            description += `<img src="${$p.children().first()["src"]}"/>`;
            return;
        }

        description += `<p>${$p.text().trim()}</p>`
    });

    return description;
}

export async function getAds() {
    var ads: IAd[] = [];

    await Core.browse({
        url: "https://tiki.vn/",
        src: [Core.umbrellaSource],
    }, async function (window) {
        var u: IUmbrellaSelector = window["u"];

        var items = u(".home-slideshow-content .swiper-slide");
        items.each(function (item) {
            var uitem = u(item);
            var img = uitem.find("img");
            var ad: IAd = {
                source: "tiki",
                url: uitem.find("a").nodes[0]["href"],
                image: img.nodes[0]["src"] || img.data("src")
            };
            ads.push(ad);
        });
    });

    winston.info(`crawled ${ads.length} ads`, { source: "tiki" });
    return ads;
}

var categoryMap = {
    62: 0,
    58: 1,
    48: 2,
    53: 3
}

var pageToCrawl = [
    "https://tiki.vn/dien-thoai-di-dong/c1793?limit=1000000000",
    "https://tiki.vn/may-tinh-bang/c1794?limit=1000000000",
    "https://tiki.vn/laptop/c2742?limit=1000000000",
    "https://tiki.vn/macbook-imac/c2458?limit=1000000000",
];