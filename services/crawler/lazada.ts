import * as Core from "./core"
import { IProduct, IProductMedia } from "../../models/product"
import { IAd } from "../../models/ad"
import { ArgumentError, OperationError } from "../../error"
import * as _ from "lodash"
import * as winston from "winston"
import { wait, parallelEach } from "../../utils/promise"
import {jsdom as jsdomConfig} from "../../config"

interface ILazadaContext {
    categoryIndex: number
    pageIndex: number
}

export async function getAllProducts(callback: Core.IOnEachProductCallback, saveContext: Core.IOnSavingContextCallback,
    categories: string[] = pageToCrawl, context?: ILazadaContext) {
    context = _.defaults<ILazadaContext, ILazadaContext>(context, {
        categoryIndex: 0,
        pageIndex: 0
    });

    // loop through the category page list
    while (context.categoryIndex < categories.length) {
        var url = categories[context.categoryIndex];

        do {
            // build the request url
            var query = url + `?itemperpage=120&page=${context.pageIndex}`;
            // get product list of the page
            var links = await getLinksFromPage(query);

            // parallel block
            await parallelEach(links, async function (link) {
                try {
                    // get product information in parallel
                    var product = await getProduct(link);
                    // notify the crawled product
                    await callback(product);
                    // clean up memory to prevent from overloading which cause application to crash
                    global.gc();
                } catch (error) {
                    var message = (_.isString(error) ? error : _.get(error, "message")) || "unknown error";
                    winston.error(message, { source: "lazada", detail: error });
                }
            }, { limit: jsdomConfig.maxConcurrency }); // set the parallel limitation

            context.pageIndex++;
            await saveContext(context);
        } while (links.length > 0)

        context.pageIndex = 0;
        context.categoryIndex++;
        await saveContext(context);
    }

    context.pageIndex = 0;
    context.categoryIndex = 0;
}

async function getLinksFromPage(url: string): Promise<string[]> {
    var attempt = 1;
    while (attempt <= 3) {
        var links: string[];
        var error: any = null;
        try {
            await Core.browse({
                url: url,
                src: [Core.umbrellaSource],
            }, async (window) => {
                links = getLinks(window);
            }, { force: true });
        } catch (err) {
            error = err;
            winston.warn(`attempt ${attempt}: unable to process ${url}`, { source: "lazada" });
        }

        if (!error) {
            if (links.length > 0) {
                winston.info(`crawled ${url}`, { source: "lazada" });
            } else {
                winston.warn(`no link at ${url}`, { source: "lazada" });
            }

            return links;
        }

        await wait(5000);
        attempt++;
    }

    throw new OperationError("Failed to load " + url);

    function getLinks(window: Window): string[] {
        var u: IUmbrellaSelector = window["u"];

        //.outofstock is in stock
        //.outofstock1 is out of stock
        return _.map<HTMLElement, string>(u("div.product-card.outofstock").nodes, function (item) {
            return Core.normalizeURL(u(item).data("original"), "www.lazada.vn");
        });
    }
}

export async function getProduct(url: string): Promise<IProduct> {
    var attempt = 1;
    var product: IProduct;
    var error: any;

    while (attempt <= 3) {
        error = null;
        try {
            await Core.browse({
                url: url,
                src: [Core.umbrellaSource]
            }, async (window) => {
                product = extraPageInfo(window);
            });
        }
        catch (err) {
            if (err instanceof ArgumentError) {
                throw err;
            }

            error = err;
            winston.warn(`attempt ${attempt}: unable to process ${url}`, { source: "lazada" });
        }

        if (!error) {
            break;
        }

        await wait(5000);
        attempt++;
    }

    if (error) {
        throw new OperationError("Failed to load " + url);
    }

    winston.info(`crawled ${product.name}`, { source: "lazada", url: url });
    return product;
}

function extraPageInfo(window: Window) {
    var u: IUmbrellaSelector = window["u"];
    var uname = u("#prod_title");
    if (uname.length === 0) {
        throw new ArgumentError(`no product is in ${window.location.href}`);
    }

    var product: IProduct = {
        category: getCategory(window),
        createdDate: null,
        description: getDescription(window),
        groupId: null,
        media: getMedia(window),
        name: uname.text().trim(),
        priceUnit: "VND",
        prices: [{ date: null, value: getPrice(window) }],
        source: "lazada",
        url: u("link[rel=canonical]").first()["href"],
        updatedDate: null,
        specification: getSpecification(window),
    };

    return product;
}

function getPrice(window: Window): number {
    var u: IUmbrellaSelector = window["u"];
    if (u("#AddToCart").length === 0) {
        return null;
    }

    var priceBox = u("#product-price-box");
    var price = Number(priceBox.find("#product_price").text());

    return price;
}

function getCategory(window: Window): number {
    var u: IUmbrellaSelector = window["u"];
    var category: number = 0;
    _.forEachRight(u(".breadcrumb__item").nodes, function (tag) {
        var key = u(tag).text().trim().toLowerCase();
        var value = categoryMap[key];
        if (value) {
            category = value;
            return false;
        }
    });
    return category;
}

function getMedia(window: Window): IProductMedia[] {
    var u: IUmbrellaSelector = window["u"];
    var imgDivs = u(".prd-moreImages .productImage");
    var media = _.map<HTMLElement, IProductMedia>(imgDivs.nodes, function (item) {
        var uitem = u(item);
        return {
            type: 2,
            url: Core.normalizeURL(uitem.data("big"), "www.lazada.vn")
        };
    });

    // thumbnail
    if (imgDivs.length > 0) {
        media.push({
            type: 1,
            url: Core.normalizeURL(imgDivs.data("swap-image"), "www.lazada.vn")
        });
    }

    // video
    var videoTag = u("#product-video-link");
    if (videoTag.length !== 0) {
        media.push({
            type: 3,
            url: Core.normalizeURL(videoTag.first()["href"], "www.lazada.vn")
        });
    }

    return media;
}

function getSpecification(window: Window): Object {
    var u: IUmbrellaSelector = window["u"];
    var spec = {};
    const generalKey = "Thông tin sản phẩm";
    spec[generalKey] = {};

    var inbox = u(".product-description__inbox");
    if (inbox.length !== 0) {
        let items = _.map(inbox.find(".inbox__item").nodes, function (item) {
            var uitem = u(item);
            return uitem.text().trim();
        });

        spec[generalKey]["Sản phẩm bao gồm"] = items.join("\n");
    }

    var table = u(".specification-table");
    if (table.length !== 0) {
        var props = table.find("tr");
        props.each(function (prop, i) {
            var uprop = u(prop);
            var ucoms = uprop.find("td");
            // skip if not as expected
            if (ucoms.length !== 2) return;

            var key = u(ucoms.nodes[0]).text().trim().replace(/\$|\./g, "")
            var value = u(ucoms.nodes[1]).text().trim();

            spec[generalKey][key] = value;
        });
    }

    return spec;
}

function getDescription(window: Window) {
    var u: IUmbrellaSelector = window["u"];

    function parse(element: umbrella.IUmbrellaNodes) {
        var str = "";
        var uitems = element.children();
        uitems.each(function (item) {
            var uitem = u(item);
            switch (item.nodeName) {
                case "H2":
                    str += `<h2>${uitem.text().trim()}</h2>`;
                    break;
                case "P":
                case "DIV":
                    if (uitem.find("img").length > 0) {
                        str += parse(uitem);
                    } else {
                        let contents = uitem.children();
                        if (contents.length === 1 && (contents.nodes[0].nodeName === "B" || contents.nodes[0].nodeName === "STRONG")) {
                            str += `<h3>${uitem.text().trim()}</h3>`
                        } else {
                            str += `<p>${uitem.text().trim()}</p>`;
                        }
                    }
                    break;
                case "A":
                    var uimg = uitem.children("img");
                    if (uimg.length > 0) {
                        let imageUrl = uimg.data("original");
                        if (!_.isEmpty(imageUrl)) {
                            str += `<img src="${imageUrl}">`;
                        }
                    } else {
                        str += `<a href="${uitem.first()["href"]}">${uitem.text().trim()}</a>`;
                    }
                    break;
                case "UL":
                    uitem.find("li").each(function (li) {
                        str += `<div>${u(li).text().trim()}</div>`;
                    });
                    break;
                case "IMG":
                    let imageUrl = uitem.data("original");
                    if (!_.isEmpty(imageUrl)) {
                        str += `<img src="${Core.normalizeURL(imageUrl, "www.lazada.vn")}">`;
                    }
                    break;
            }
        });

        return str;
    }

    var udescription = u("#productDetails >div");
    return parse(udescription);
}

export async function getAds() {
    var ads: IAd[] = [];

    await Core.browse({
        url: "http://www.lazada.vn/",
        src: [Core.umbrellaSource],
    }, async function (window) {
        var u: IUmbrellaSelector = window["u"];

        var items = u(".c-mp-tabs-banner__tab-body[data-tab-id='1'] .c-promo-grid__cell");
        items.each(function (item) {
            var uitem = u(item);
            var uimage = uitem.find(".c-banner__background");
            var uparams = JSON.parse(uimage.attr("data-js-component-params"));
            if (!(uparams && uparams.src)) return;
            var ad: IAd = {
                source: "lazada",
                url: uitem.find("a").nodes[0]["href"],
                image: uparams.src
            };
            ads.push(ad);
        });
    });

    winston.info(`crawled ${ads.length} ads`, { source: "lazada" });
    return ads;
}

var categoryMap = {
    "điện thoại di động": 1,
    "máy tính bảng": 2,
    "laptop": 3
}

var pageToCrawl = [
    "http://www.lazada.vn/dien-thoai-di-dong",
    "http://www.lazada.vn/laptop",
    "http://www.lazada.vn/may-tinh-bang"
];