"use strict";
const fs = require("fs");
const jsdom = require("jsdom");
const _ = require("lodash");
const Wreck = require("wreck");
const Uri = require("urijs");
exports.umbrellaSource = fs.readFileSync("services/crawler/inject/umbrella.min.js", "utf8");
exports.defaultWindow = jsdom.jsdom(undefined, {
    features: {
        FetchExternalResources: false,
        ProcessExternalResources: ["script"]
    }
}).defaultView;
function initDefaultWindow() {
    return new Promise(function (resolve, reject) {
        var document = exports.defaultWindow.document;
        var el = document.createElement("script");
        el.onload = function () {
            resolve();
        };
        el.onerror = function () {
            reject(new Error("unable to load umbrella"));
        };
        el.text = exports.umbrellaSource;
        document.body.appendChild(el);
    });
}
function init() {
    return initDefaultWindow();
}
exports.init = init;
class RequestError extends Error {
    constructor(message, code) {
        super(message);
        this.code = code;
    }
}
exports.RequestError = RequestError;
function browse(jsdomConfig, processCallback, options) {
    return new Promise(function (resolve, reject) {
        var config = _.extend({}, jsdomConfig, {
            done: function (errors, window) {
                if (errors) {
                    return reject(errors);
                }
                processCallback(window).then(resolve, reject).then(function () {
                    if (window) {
                        var u = window["u"];
                        u("script").remove();
                        window.close();
                    }
                });
            }
        });
        jsdom.env(config);
        config = null;
    });
}
exports.browse = browse;
function get(uri, payload) {
    if (payload) {
        uri = Uri(uri).search(payload).toString();
    }
    return new Promise(function (resolve, reject) {
        Wreck.get(uri, {
            timeout: 10000,
            json: true
        }, function (error, response, payload) {
            if (error) {
                return reject(error);
            }
            if (response.statusCode >= 400) {
                return reject(new RequestError(response.statusMessage, response.statusCode));
            }
            resolve(payload);
        });
    });
}
exports.get = get;
function post(uri, payload, options) {
    return new Promise(function (resolve, reject) {
        options = _.defaults(options, {
            wwwFormUrlEncoded: false
        });
        var payloadAsString;
        var headers;
        if (options.wwwFormUrlEncoded) {
            payloadAsString = Uri.buildQuery(payload);
            headers = {
                "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
            };
        }
        else {
            payloadAsString = JSON.stringify(payload);
            headers = {
                "Content-Type": "application/json"
            };
        }
        Wreck.post(uri, {
            timeout: 10000,
            json: true,
            payload: payloadAsString,
            headers: headers
        }, function (error, response, payload) {
            if (error) {
                return reject(error);
            }
            if (response.statusCode >= 400) {
                return reject(new RequestError(response.statusMessage, response.statusCode));
            }
            resolve(payload);
        });
    });
}
exports.post = post;
function normalizeURL(url, host, protocol = "http") {
    var uri = Uri(url);
    if (_.isEmpty(uri.host())) {
        uri.host(host);
    }
    if (_.isEmpty(uri.protocol())) {
        uri.protocol(protocol);
    }
    return uri.toString();
}
exports.normalizeURL = normalizeURL;
//# sourceMappingURL=core.js.map