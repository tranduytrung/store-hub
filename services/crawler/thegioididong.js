"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const Core = require("./core");
const error_1 = require("../../error");
const _ = require("lodash");
const winston = require("winston");
const promise_1 = require("../../utils/promise");
const config_1 = require("../../config");
const jsdom_1 = require("jsdom");
const URI = require("urijs");
var host = "https://www.thegioididong.com";
function getAllProducts(callback) {
    return __awaiter(this, void 0, Promise, function* () {
        for (let categoryId in categoryMap) {
            var pageIndex = 0;
            do {
                var result = yield getProductsFromCategory(Number(categoryId), pageIndex);
                yield promise_1.parallelEach(result.products, function (product) {
                    return __awaiter(this, void 0, void 0, function* () {
                        try {
                            yield callback(product);
                        }
                        catch (error) {
                            var message = (_.isString(error) ? error : _.get(error, "message")) || "unknown error";
                            winston.error(message, { source: "thegioididong", detail: error });
                        }
                    });
                }, { limit: config_1.jsdom.maxConcurrency });
                pageIndex++;
                global.gc();
            } while (result.next);
        }
    });
}
exports.getAllProducts = getAllProducts;
function getProductsFromCategory(categoryId, pageIndex) {
    return __awaiter(this, void 0, Promise, function* () {
        var attempt = 1;
        while (attempt <= 3) {
            var result;
            var error = null;
            try {
                var buffer = (yield Core.post("https://www.thegioididong.com/aj/CategoryV3/More", {
                    Category: categoryId,
                    PageSize: 100,
                    PageIndex: pageIndex
                }, { wwwFormUrlEncoded: true }));
                var html = buffer.toString("utf-8");
                result = yield parseProductsResult(html);
                // modify category
                _.each(result.products, function (product) {
                    product.category = categoryMap[categoryId] || 0;
                });
            }
            catch (err) {
                error = err;
                winston.warn(`attempt ${attempt}: unable to process cateogryId=${categoryId} pageIndex=${pageIndex}`, { source: "thegioididong" });
            }
            if (!error) {
                if (result.products.length > 0) {
                    winston.info(`crawled cateogryId=${categoryId} pageIndex=${pageIndex}`, { source: "thegioididong" });
                }
                else {
                    winston.warn(`no link at cateogryId=${categoryId} pageIndex=${pageIndex}`, { source: "thegioididong" });
                }
                return result;
            }
            yield promise_1.wait(5000);
            attempt++;
        }
        throw new error_1.OperationError(`Failed to load cateogryId=${categoryId} pageIndex=${pageIndex}`);
    });
}
function parseProductsResult(html) {
    return __awaiter(this, void 0, Promise, function* () {
        var window = Core.defaultWindow;
        var u = window["u"];
        var $html = u(html);
        var next = $html.is(".viewmore");
        var products = [];
        var $tags = $html.first().children;
        for (var index = 0; index < $tags.length; index++) {
            try {
                var tag = $tags[index];
                var $tag = u(tag);
                var $abuy = $tag.children("a.buy");
                var $a = $tag.children("a").not($abuy);
                var url = host + $a.nodes[0]["href"];
                var thumbnail = $a.children("img").nodes[0]["src"] || $a.children("img").data("original");
                var name = $a.children("h3").text();
                var price = Number($a.children("strong").text().replace(/\D/g, ""));
                var id = ($abuy.length > 0 ? URI($abuy.nodes[0]["href"]).search(true)["ProductId"] : null)
                    || thumbnail.split("/")[6];
                var asyncResults = yield Promise.all([getSpecification(id), getDescription(id), getMedia(id)]);
                var product = {
                    groupId: null,
                    specification: asyncResults[0] ? asyncResults[0].specification : null,
                    createdDate: null,
                    updatedDate: null,
                    description: asyncResults[1],
                    source: "thegioididong",
                    url: url,
                    category: 0,
                    media: asyncResults[2],
                    name: name,
                    prices: [{
                            date: null,
                            value: price
                        }],
                    priceUnit: "VND"
                };
                if (asyncResults[0]) {
                    product.media.unshift(asyncResults[0].image);
                }
                product.media.unshift({ url: thumbnail, type: 1 });
                products.push(product);
                winston.info(`crawled ${url}`, { source: "thegioididong" });
            }
            catch (error) {
                winston.warn(`unable to process ${url}`, { source: "thegioididong" });
            }
        }
        return {
            products: products,
            next: next
        };
    });
}
function getProduct(url) {
    return __awaiter(this, void 0, Promise, function* () {
        var attempt = 1;
        var product;
        var error;
        while (attempt <= 3) {
            error = null;
            try {
                yield Core.browse({
                    url: url,
                    src: [Core.umbrellaSource]
                }, (window) => __awaiter(this, void 0, void 0, function* () {
                    product = yield extraPageInfo(window);
                }));
            }
            catch (err) {
                if (err instanceof error_1.ArgumentError) {
                    throw err;
                }
                error = err;
                winston.warn(`attempt ${attempt}: unable to process ${url}`, { source: "thegioididong" });
            }
            if (!error) {
                winston.info(`crawled ${product.name}`, { source: "thegioididong", url: url });
                return product;
            }
            yield promise_1.wait(5000);
            attempt++;
        }
        throw new error_1.OperationError("Failed to load " + url);
        function extraPageInfo(window) {
            return __awaiter(this, void 0, Promise, function* () {
                var u = window["u"];
                var $scripts = u("script:not([src])");
                var scriptText = "";
                $scripts.each(function (script) {
                    scriptText += script.innerHTML;
                });
                var id = scriptText.match(/identifier\s*:\s("|')(\d+)("|')/)[2], name = scriptText.match(/fn\s*:\s("|')(.+?)("|')/)[2], url = u("link[rel=canonical]").first()["href"], price = Number(scriptText.match(/price\s*:\s("|')(\d+)("|')/)[2]), categoryId = scriptText.match(/cateID\s*=\s*('|")(\d+)('|")/)[2], photo = scriptText.match(/photo\s*:\s("|')(.+?)("|')/)[2];
                if (!id) {
                    throw new error_1.ArgumentError(`no product is in ${window.location.href}`);
                }
                var asyncResults = yield Promise.all([getSpecification(id), getDescription(id), getMedia(id)]);
                var product = {
                    groupId: null,
                    source: "thegioididong",
                    name: name,
                    description: asyncResults[1],
                    url: url,
                    prices: [{
                            date: null,
                            value: price
                        }],
                    priceUnit: "VND",
                    category: categoryMap[categoryId] || 0,
                    specification: asyncResults[0] ? asyncResults[0].specification : null,
                    media: asyncResults[2],
                    createdDate: null,
                    updatedDate: null,
                };
                if (photo) {
                    product.media.unshift({
                        type: 1,
                        url: "https:" + photo
                    });
                }
                if (asyncResults[0]) {
                    product.media.unshift(asyncResults[0].image);
                }
                if (product.category === 0) {
                    product.prices = [{
                            date: null,
                            value: Number(u(".price >strong:not(.priceline)").text().replace(/\D/g, ""))
                        }];
                }
                return product;
            });
        }
    });
}
exports.getProduct = getProduct;
function getSpecification(id) {
    return __awaiter(this, void 0, Promise, function* () {
        var attempt = 1;
        while (attempt <= 3) {
            var error = null;
            var result;
            try {
                var data = (yield Core.post("https://www.thegioididong.com/aj/ProductV2/GetFullSpec", {
                    productID: id
                }, { wwwFormUrlEncoded: true }));
                result = {
                    specification: extractParameters(data["spec"]),
                    image: {
                        type: 2,
                        url: "https:" + data["imgKit"]
                    }
                };
            }
            catch (err) {
                error = err;
                winston.warn(`attempt ${attempt}: unable to process specification productId=${id}`, { source: "thegioididong" });
            }
            if (!error) {
                winston.info(`gathered specification productId=${id}`, { source: "thegioididong" });
                return result;
            }
            attempt++;
            yield promise_1.wait(5000);
        }
        return null;
        function extractParameters(html) {
            var window = Core.defaultWindow;
            var u = window["u"];
            var parameters = {};
            var currentGroupKey = "unspecified";
            var $tags = u(html);
            $tags.each(function (tag) {
                var $tag = u(tag);
                // GROUP
                var $group = $tag.find("label");
                if ($group.length > 0) {
                    currentGroupKey = $group.text().trim().replace(/\$|\./g, "");
                    parameters[currentGroupKey] = {};
                }
                else {
                    // KEY
                    var $key = $tag.children("span");
                    var key = $key.text().trim().replace(/\$|\./g, "");
                    // VALUES
                    var $values = $tag.children("div");
                    var values = [];
                    if ($values.children().length === 0) {
                        // plain text
                        values.push($values.html());
                    }
                    else {
                        // extract <a> tag
                        $values.find("a").each(function (tag) {
                            values.push(u(tag).text().trim());
                        });
                    }
                    parameters[currentGroupKey][key] = values.join();
                }
            });
            return parameters;
        }
    });
}
function getDescription(id) {
    return __awaiter(this, void 0, Promise, function* () {
        var attempt = 1;
        while (attempt <= 3) {
            var error = null;
            var result;
            try {
                var data = (yield Core.post("https://www.thegioididong.com/aj/ProductV2/GetArticleDetail_Popup", {
                    productID: id,
                    newsID: 0
                }, { wwwFormUrlEncoded: true }));
                result = extractDescription(data["res"]);
            }
            catch (err) {
                error = err;
                winston.warn(`attempt ${attempt}: unable to process description productId=${id}`, { source: "thegioididong" });
            }
            if (!error) {
                winston.info(`gathered description productId=${id}`, { source: "thegioididong" });
                return result;
            }
            attempt++;
            yield promise_1.wait(5000);
        }
        return null;
        function extractDescription(html) {
            var window = Core.defaultWindow;
            var u = window["u"];
            var $html = u(html);
            var description = "<h2>" + $html.find("h1").text() + "</h2>";
            var $p = $html.find("p");
            $p.each(function (tag) {
                var $tag = u(tag);
                if (tag.childNodes.length !== 0 && tag.childNodes[0].nodeType === 3) {
                    description += "<p>" + $tag.text() + "</p>";
                }
                else {
                    var $strong = $tag.children("strong");
                    if ($strong.length !== 0) {
                        description += "<h3>" + $strong.text() + "</h3>";
                    }
                    else {
                        var $i = $tag.children("i");
                        if ($i.length !== 0) {
                            description += "<i>" + $i.text() + "</i>";
                        }
                        else {
                            var $img = $tag.find("img");
                            if ($img.length !== 0) {
                                description += "<img src=\"" + $img.nodes[0]["src"] + "\"/>";
                            }
                            else {
                                description += "<p>" + $tag.text() + "</p>";
                            }
                        }
                    }
                }
            });
            return description;
        }
    });
}
function getMedia(id) {
    return __awaiter(this, void 0, Promise, function* () {
        var attempt = 1;
        while (attempt <= 3) {
            var error = null;
            var result;
            try {
                var data = (yield Core.post("https://www.thegioididong.com/aj/ProductV2/GetGallery", {
                    productID: id,
                    imageType: 1,
                    colorID: 0
                }, { wwwFormUrlEncoded: true }));
                result = extractMedia(data["lstImg"]);
            }
            catch (err) {
                error = err;
                winston.warn(`attempt ${attempt}: unable to process description productId=${id}`, { source: "thegioididong" });
            }
            if (!error) {
                winston.info(`gathered description productId=${id}`, { source: "thegioididong" });
                return result;
            }
            attempt++;
            yield promise_1.wait(5000);
        }
        return null;
        function extractMedia(html) {
            var window = Core.defaultWindow;
            var u = window["u"];
            var media = [];
            var $imgs = u(html).find("img");
            $imgs.each(function (img) {
                media.push({
                    type: 2,
                    url: u(img).data("src")
                });
            });
            return media;
        }
    });
}
var cookieJar = jsdom_1.createCookieJar();
function getAds() {
    return __awaiter(this, void 0, void 0, function* () {
        var ads = [];
        // expected to welcome screen
        yield Core.browse({
            url: "https://www.thegioididong.com/",
            cookieJar: cookieJar
        }, function (window) {
            return __awaiter(this, void 0, void 0, function* () { });
        });
        yield Core.browse({
            url: "https://www.thegioididong.com/",
            src: [Core.umbrellaSource],
            cookieJar: cookieJar
        }, function (window) {
            return __awaiter(this, void 0, void 0, function* () {
                var u = window["u"];
                var items = u("#sync1 .item");
                items.each(function (item) {
                    var uitem = u(item);
                    var img = uitem.find("img");
                    var ad = {
                        source: "thegioididong",
                        url: uitem.find("a").nodes[0]["href"],
                        image: img.nodes[0]["src"] || URI(img.data("src")).protocol("https").toString()
                    };
                    ads.push(ad);
                });
            });
        });
        winston.info(`crawled ${ads.length} ads`, { source: "thegioididong" });
        return ads;
    });
}
exports.getAds = getAds;
var categoryMap = {
    42: 1,
    522: 2,
    44: 3,
};
//# sourceMappingURL=thegioididong.js.map