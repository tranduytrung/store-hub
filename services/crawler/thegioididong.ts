import * as Core from "./core"
import { IProduct, IProductMedia } from "../../models/product"
import { IAd } from "../../models/ad"
import { ArgumentError, OperationError } from "../../error"
import * as _ from "lodash"
import * as winston from "winston"
import { wait, parallelEach } from "../../utils/promise"
import {jsdom as jsdomConfig} from "../../config"
import {createCookieJar} from "jsdom"
import * as URI from "urijs"

var host = "https://www.thegioididong.com";
export async function getAllProducts(callback: Core.IOnEachProductCallback): Promise<void> {
    for (let categoryId in categoryMap) {
        var pageIndex = 0;
        do {
            var result = await getProductsFromCategory(Number(categoryId), pageIndex);

            await parallelEach(result.products, async function (product) {
                try {
                    await callback(product);
                } catch (error) {
                    var message = (_.isString(error) ? error : _.get(error, "message")) || "unknown error";
                    winston.error(message, { source: "thegioididong", detail: error });
                }
            }, { limit: jsdomConfig.maxConcurrency });

            pageIndex++;
            global.gc();
        } while (result.next)
    }
}

interface IGetProductsResult {
    products: IProduct[]
    next: boolean
}

async function getProductsFromCategory(categoryId: number, pageIndex: number): Promise<IGetProductsResult> {
    var attempt = 1;
    while (attempt <= 3) {
        var result: IGetProductsResult;
        var error: any = null;
        try {
            var buffer = <Buffer>(await Core.post("https://www.thegioididong.com/aj/CategoryV3/More", {
                Category: categoryId,
                PageSize: 100,
                PageIndex: pageIndex
            }, { wwwFormUrlEncoded: true }));

            var html = buffer.toString("utf-8");
            result = await parseProductsResult(html);

            // modify category
            _.each(result.products, function (product) {
                product.category = categoryMap[categoryId] || 0
            });
        } catch (err) {
            error = err;
            winston.warn(`attempt ${attempt}: unable to process cateogryId=${categoryId} pageIndex=${pageIndex}`, { source: "thegioididong" });
        }

        if (!error) {
            if (result.products.length > 0) {
                winston.info(`crawled cateogryId=${categoryId} pageIndex=${pageIndex}`, { source: "thegioididong" });
            } else {
                winston.warn(`no link at cateogryId=${categoryId} pageIndex=${pageIndex}`, { source: "thegioididong" });
            }

            return result;
        }

        await wait(5000);
        attempt++;
    }

    throw new OperationError(`Failed to load cateogryId=${categoryId} pageIndex=${pageIndex}`);
}


async function parseProductsResult(html: string): Promise<IGetProductsResult> {
    var window = Core.defaultWindow;
    var u: IUmbrellaSelector = window["u"];

    var $html = u(html);
    var next = $html.is(".viewmore");
    var products: IProduct[] = [];
    var $tags = $html.first().children;

    for (var index = 0; index < $tags.length; index++) {
        try {
            var tag = $tags[index];
            var $tag = u(tag);
            var $abuy = $tag.children("a.buy");
            var $a = $tag.children("a").not($abuy);

            var url = host + $a.nodes[0]["href"];
            var thumbnail = $a.children("img").nodes[0]["src"] || $a.children("img").data("original");
            var name = $a.children("h3").text();
            var price = Number($a.children("strong").text().replace(/\D/g, ""));
            var id = ($abuy.length > 0 ? URI($abuy.nodes[0]["href"]).search(true)["ProductId"] : null)
                || thumbnail.split("/")[6];

            var asyncResults = await Promise.all([getSpecification(id), getDescription(id), getMedia(id)]);

            var product: IProduct = {
                groupId: null,
                specification: asyncResults[0] ? asyncResults[0].specification : null,
                createdDate: null,
                updatedDate: null,
                description: asyncResults[1],
                source: "thegioididong",
                url: url,
                category: 0,
                media: asyncResults[2],
                name: name,
                prices: [{
                    date: null,
                    value: price
                }],
                priceUnit: "VND"
            };

            if (asyncResults[0]) {
                product.media.unshift(asyncResults[0].image);
            }
            product.media.unshift({ url: thumbnail, type: 1 });

            products.push(product);
            winston.info(`crawled ${url}`, { source: "thegioididong" });
        } catch (error) {
            winston.warn(`unable to process ${url}`, { source: "thegioididong" });
        }
    }

    return {
        products,
        next
    };
}

export async function getProduct(url: string): Promise<IProduct> {
    var attempt = 1;
    var product: IProduct;
    var error: any;

    while (attempt <= 3) {
        error = null;
        try {
            await Core.browse({
                url: url,
                src: [Core.umbrellaSource]
            }, async (window) => {
                product = await extraPageInfo(window);
            });
        } catch (err) {
            if (err instanceof ArgumentError) {
                throw err;
            }

            error = err;
            winston.warn(`attempt ${attempt}: unable to process ${url}`, { source: "thegioididong" });
        }

        if (!error) {
            winston.info(`crawled ${product.name}`, { source: "thegioididong", url: url });
            return product;
        }

        await wait(5000);
        attempt++;
    }

    throw new OperationError("Failed to load " + url);

    async function extraPageInfo(window: Window): Promise<IProduct> {
        var u: IUmbrellaSelector = window["u"];
        var $scripts = u("script:not([src])");

        var scriptText = "";
        $scripts.each(function (script) {
            scriptText += script.innerHTML;
        });

        var id = scriptText.match(/identifier\s*:\s("|')(\d+)("|')/)[2],
            name = scriptText.match(/fn\s*:\s("|')(.+?)("|')/)[2],
            url = u("link[rel=canonical]").first()["href"],
            price = Number(scriptText.match(/price\s*:\s("|')(\d+)("|')/)[2]),
            categoryId = scriptText.match(/cateID\s*=\s*('|")(\d+)('|")/)[2],
            photo = scriptText.match(/photo\s*:\s("|')(.+?)("|')/)[2];

        if (!id) {
            throw new ArgumentError(`no product is in ${window.location.href}`);
        }

        var asyncResults = await Promise.all([getSpecification(id), getDescription(id), getMedia(id)]);

        var product: IProduct = {
            groupId: null,
            source: "thegioididong",
            name: name,
            description: asyncResults[1],
            url: url,
            prices: [{
                date: null,
                value: price
            }],
            priceUnit: "VND",
            category: categoryMap[categoryId] || 0,
            specification: asyncResults[0] ? asyncResults[0].specification : null,
            media: asyncResults[2],
            createdDate: null,
            updatedDate: null,
        }

        if (photo) {
            product.media.unshift({
                type: 1,
                url: "https:" + photo
            });
        }

        if (asyncResults[0]) {
            product.media.unshift(asyncResults[0].image);
        }

        if (product.category === 0) {
            product.prices = [{
                date: null,
                value: Number(u(".price >strong:not(.priceline)").text().replace(/\D/g, ""))
            }];
        }

        return product;
    }
}

async function getSpecification(id: string): Promise<ISpecificationWithImage> {
    var attempt = 1;
    while (attempt <= 3) {
        var error = null;
        var result: ISpecificationWithImage;
        try {
            var data = <Object>(await Core.post("https://www.thegioididong.com/aj/ProductV2/GetFullSpec", {
                productID: id
            }, { wwwFormUrlEncoded: true }));

            result = {
                specification: extractParameters(data["spec"]),
                image: {
                    type: 2,
                    url: "https:" + data["imgKit"]
                }
            };
        } catch (err) {
            error = err;
            winston.warn(`attempt ${attempt}: unable to process specification productId=${id}`, { source: "thegioididong" });
        }

        if (!error) {
            winston.info(`gathered specification productId=${id}`, { source: "thegioididong" });
            return result;
        }

        attempt++;
        await wait(5000);
    }

    return null;

    function extractParameters(html: string): Object {
        var window = Core.defaultWindow;
        var u: IUmbrellaSelector = window["u"];

        var parameters = {};
        var currentGroupKey = "unspecified";
        var $tags = u(html);
        $tags.each(function (tag) {
            var $tag = u(tag);

            // GROUP
            var $group = $tag.find("label");
            if ($group.length > 0) {
                currentGroupKey = $group.text().trim().replace(/\$|\./g, "");
                parameters[currentGroupKey] = {};
            } else {
                // KEY
                var $key = $tag.children("span");
                var key = $key.text().trim().replace(/\$|\./g, "");

                // VALUES
                var $values = $tag.children("div");
                var values = [];
                if ($values.children().length === 0) {
                    // plain text
                    values.push($values.html());
                } else {
                    // extract <a> tag
                    $values.find("a").each(function (tag) {
                        values.push(u(tag).text().trim());
                    });
                }

                parameters[currentGroupKey][key] = values.join();
            }
        });

        return parameters;
    }
}

async function getDescription(id: string): Promise<string> {
    var attempt = 1;
    while (attempt <= 3) {
        var error = null;
        var result: string;
        try {
            var data = <Object>(await Core.post("https://www.thegioididong.com/aj/ProductV2/GetArticleDetail_Popup", {
                productID: id,
                newsID: 0
            }, { wwwFormUrlEncoded: true }));

            result = extractDescription(data["res"]);
        } catch (err) {
            error = err;
            winston.warn(`attempt ${attempt}: unable to process description productId=${id}`, { source: "thegioididong" });
        }

        if (!error) {
            winston.info(`gathered description productId=${id}`, { source: "thegioididong" });
            return result;
        }

        attempt++;
        await wait(5000);
    }

    return null;

    function extractDescription(html: string) {
        var window = Core.defaultWindow;
        var u: IUmbrellaSelector = window["u"];

        var $html = u(html);
        var description = "<h2>" + $html.find("h1").text() + "</h2>";

        var $p = $html.find("p");
        $p.each(function (tag) {
            var $tag = u(tag);

            if (tag.childNodes.length !== 0 && tag.childNodes[0].nodeType === 3) {
                description += "<p>" + $tag.text() + "</p>";
            } else {
                var $strong = $tag.children("strong");
                if ($strong.length !== 0) {
                    description += "<h3>" + $strong.text() + "</h3>";
                } else {
                    var $i = $tag.children("i");
                    if ($i.length !== 0) {
                        description += "<i>" + $i.text() + "</i>";
                    } else {
                        var $img = $tag.find("img");
                        if ($img.length !== 0) {
                            description += "<img src=\"" + $img.nodes[0]["src"] + "\"/>";
                        } else {
                            description += "<p>" + $tag.text() + "</p>";
                        }
                    }
                }
            }
        });

        return description;
    }
}

async function getMedia(id: string): Promise<IProductMedia[]> {
    var attempt = 1;
    while (attempt <= 3) {
        var error = null;
        var result: IProductMedia[];
        try {
            var data = <Object>(await Core.post("https://www.thegioididong.com/aj/ProductV2/GetGallery", {
                productID: id,
                imageType: 1,
                colorID: 0
            }, { wwwFormUrlEncoded: true }));

            result = extractMedia(data["lstImg"]);
        } catch (err) {
            error = err;
            winston.warn(`attempt ${attempt}: unable to process description productId=${id}`, { source: "thegioididong" });
        }

        if (!error) {
            winston.info(`gathered description productId=${id}`, { source: "thegioididong" });
            return result;
        }

        attempt++;
        await wait(5000);
    }

    return null;

    function extractMedia(html: string) {
        var window = Core.defaultWindow;
        var u: IUmbrellaSelector = window["u"];

        var media: IProductMedia[] = [];
        var $imgs = u(html).find("img");

        $imgs.each(function (img) {
            media.push({
                type: 2,
                url: u(img).data("src")
            });
        });

        return media;
    }
}

interface ISpecificationWithImage {
    specification: {},
    image: IProductMedia
}

var cookieJar = createCookieJar();
export async function getAds() {
    var ads: IAd[] = [];

    // expected to welcome screen
    await Core.browse({
        url: "https://www.thegioididong.com/",
        cookieJar: cookieJar
    }, async function (window) { });

    await Core.browse({
        url: "https://www.thegioididong.com/",
        src: [Core.umbrellaSource],
        cookieJar: cookieJar
    }, async function (window) {
        var u: IUmbrellaSelector = window["u"];
        var items = u("#sync1 .item");
        items.each(function (item) {
            var uitem = u(item);
            var img = uitem.find("img");
            var ad: IAd = {
                source: "thegioididong",
                url: uitem.find("a").nodes[0]["href"],
                image: img.nodes[0]["src"] || URI(img.data("src")).protocol("https").toString()
            };
            ads.push(ad);
        });
    })

    winston.info(`crawled ${ads.length} ads`, { source: "thegioididong" });
    return ads;
}

var categoryMap = {
    42: 1,
    522: 2,
    44: 3,
    // 382: 0,
    // 86: 0,
    // 75: 0,
    // 56: 0,
    // 1823: 0,
    // 1363: 0,
    // 1662: 0,
    // 54: 0,
    // 58: 0,
    // 55: 0,
    // 57: 0,
    // 60: 0
}