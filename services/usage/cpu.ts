import {cpus} from "os"

interface ICPUUsage {
    uptime: number
    idle: number
}

export function getUsage() {
    var cpuList = cpus();
    var totalIdle = 0;
    var totalUptime = 0;
    var cpusUsage : ICPUUsage[] = new Array(cpuList.length);
    for (var index = 0; index < cpuList.length; index++) {
        var cpu = cpuList[index];
        cpusUsage[index] = {
            idle: cpu.times.idle,
            uptime: cpu.times.idle + cpu.times.irq + cpu.times.nice + cpu.times.sys + cpu.times.user
        };
    }
    
    return cpusUsage;
}