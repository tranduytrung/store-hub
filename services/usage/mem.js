"use strict";
const os_1 = require("os");
function getUsage() {
    return {
        total: os_1.totalmem(),
        free: os_1.freemem(),
        rss: process.memoryUsage().rss
    };
}
exports.getUsage = getUsage;
//# sourceMappingURL=mem.js.map