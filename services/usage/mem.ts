import {totalmem, freemem} from "os"

interface IMemoryUsage {
    total: number
    free: number
    rss: number
}

export function getUsage() : IMemoryUsage {
    return {
        total: totalmem(),
        free: freemem(),
        rss: process.memoryUsage().rss
    };
}