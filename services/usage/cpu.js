"use strict";
const os_1 = require("os");
function getUsage() {
    var cpuList = os_1.cpus();
    var totalIdle = 0;
    var totalUptime = 0;
    var cpusUsage = new Array(cpuList.length);
    for (var index = 0; index < cpuList.length; index++) {
        var cpu = cpuList[index];
        cpusUsage[index] = {
            idle: cpu.times.idle,
            uptime: cpu.times.idle + cpu.times.irq + cpu.times.nice + cpu.times.sys + cpu.times.user
        };
    }
    return cpusUsage;
}
exports.getUsage = getUsage;
//# sourceMappingURL=cpu.js.map