"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const mongodb_1 = require("mongodb");
const config = require("../../config");
let db;
function initialize() {
    return __awaiter(this, void 0, Promise, function* () {
        return db = yield mongodb_1.MongoClient.connect(config.database.uri);
    });
}
exports.initialize = initialize;
function getInstance() {
    return db;
}
exports.getInstance = getInstance;
//# sourceMappingURL=db.js.map