import {MongoClient, Db} from "mongodb"
import * as config from "../../config"

let db : Db

export async function initialize() : Promise<Db> {
    return db = await MongoClient.connect(config.database.uri);
}

export function getInstance() {
    return db;
}