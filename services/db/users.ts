import * as mongodb from "mongodb";
import * as Db from "./db";
import * as _ from "lodash";
import { Validator } from "jsonschema";
import {ArgumentError, OperationError, NotFoundError} from "../../error";
import {ConditionalVerbs, IWishItem, IUser} from "../../models/user";
import {spwanOnDemandTask} from "../task/spawn"

var db = Db.getInstance();
var collection = db.collection("users");

var conditionalVerbsSchema = {
    id: "/ConditionalVerbs",
    type: "number",
    required: true
}

var trackSchema = {
    id: "/ITrack",
    type: ["object", null],
    additionalProperties: false,
    properties: {
        cycle: {
            type: "number",
            required: true
        },
        conditionalVerb: {
            $ref: "/ConditionalVerbs"
        },
        thresholdValue: {
            type: "number",
            required: true
        },
        nextNotificationDate: {
            type: "date",
            required: true
        }
    }
}

var wishItemSchema = {
    id: "/IWishItem",
    type: "object",
    required: true,
    additionalProperties: false,
    properties: {
        productId: {
            type: "string",
            required: true
        },
        track: {
            $ref: "/ITrack"
        }
    }
}

var userSchema = {
    id: "/IUser",
    type: "object",
    required: true,
    additionalProperties: false,
    properties: {
        firstName: {
            type: "string"
        },
        lastName: {
            type: "string"
        },
        email: {
            type: "string",
            required: true
        },
        wishlist: {
            type: "array",
            items: {
                $ref: "/IWishItem"
            }
        },
        facebookId: {
            type: "string"
        },
        googleId: {
            type: "string"
        },
        onesignalIds: {
            type: "array",
            items: {
                type: "string",
                required: true
            },
            required: true
        }
    }
}

export async function insert(user: IUser): Promise<IUser> {
    validateUser(user);

    var setFields = _(user).omit(["email", "wishlist", "onesignalIds"]).omitBy(_.isNil).value();
    var result = await collection.findOneAndUpdate(
        {
            email: user.email
        },
        {
            $set: setFields
        },
        {
            upsert: true,
            returnOriginal: false
        }
    );

    if (!result.ok) {
        throw new OperationError("failed to create or update user");
    }

    return result.value;
}

export async function addOnesignal(userId: string, onesignalId: string): Promise<void> {
    if (!_.isString(onesignalId)) {
        throw new ArgumentError("oneSignalId must be a string");
    }

    var removeQuery = {
        onesignalIds: onesignalId
    };

    var removeUpdate = {
        $pull: {
            onesignalIds: onesignalId
        }
    };

    var removeResult = await collection.updateOne(removeQuery, removeUpdate);
    if (!removeResult.result.ok) {
        throw new OperationError("unable to remove OneSignal from old user");
    }

    var addQuery = {
        _id: new mongodb.ObjectID(userId)
    };

    var addUpdate = {
        $addToSet: {
            onesignalIds: onesignalId
        }
    };

    var addResult = await collection.updateOne(addQuery, addUpdate);
    if (!addResult.result.ok) {
        throw new OperationError("unable to add OneSignal to user");
    }
}

export async function getById(id: string): Promise<IUser> {
    if (_.isEmpty(id)) {
        throw new ArgumentError("id is empty");
    }

    var result = await collection.find({
        _id: new mongodb.ObjectID(id)
    }).limit(1).toArray();
    return _.isEmpty(result) ? null : result[0];
}

export async function getMany(ids: string[] | mongodb.ObjectID[]): Promise<IUser[]> {
    var objectIds: mongodb.ObjectID[] = [];
    for (let index = 0; index < ids.length; index++) {
        let id = ids[index];
        objectIds.push(_.isString(id) ? new mongodb.ObjectID(id) : id);
    }

    var result = await collection.find({
        _id: { $in: objectIds }
    }).toArray();
    return result;
}

export async function getByEmail(email: string): Promise<IUser> {
    if (_.isEmpty(email)) {
        throw new ArgumentError("email is empty");
    }

    var result = await collection.find({
        email: email
    }).limit(1).toArray();
    return _.isEmpty(result) ? null : result[0];
}

export async function getByFacebookId(id: string): Promise<IUser> {
    if (_.isEmpty(id)) {
        throw new ArgumentError("id is empty");
    }

    var result = await collection.find({
        facebookId: id
    }).limit(1).toArray();
    return _.isEmpty(result) ? null : result[0];
}

export function getForBroadcast(productId: string, price: number): Promise<IUser[]> {
    if (_.isEmpty(productId)) {
        throw new ArgumentError("id is empty");
    }

    var query = {
        wishlist: {
            $elemMatch: {
                "productId": new mongodb.ObjectID(productId),
                "track.nextNotificationDate": { $lte: new Date() },
                "track.thresholdValue": { $gte: price }
            }
        }
    };

    var result = <Promise<IUser[]>>collection.find(query).toArray();
    return result;
}

export function countByProductId(productId: string | mongodb.ObjectID): Promise<number> {
    if (_.isEmpty(productId)) {
        throw new ArgumentError("id is empty");
    }

    var query = {
        "wishlist.productId": productId instanceof mongodb.ObjectID ? productId : new mongodb.ObjectID(productId)
    };

    var result = collection.count(query);
    return <Promise<number>>result;
}

export async function addWishItem(id: string, wishItem: IWishItem) {
    if (_.isEmpty(id)) {
        throw new ArgumentError("id is empty");
    }

    if (_.isEmpty(wishItem)) {
        throw new ArgumentError("wishItem is empty");
    }

    if (wishItem.track) {
        wishItem.track.nextNotificationDate = new Date();
    }

    validateWishItem(wishItem);

    var result = await collection.updateOne(
        {
            _id: new mongodb.ObjectID(id),
            "wishlist.productId": {
                $ne: wishItem.productId
            }
        },
        {
            $push: {
                wishlist: wishItem
            }
        }
    );

    if (result.matchedCount < 1) {
        throw new NotFoundError("no such user or the item is already in wishlist");
    }

    if (result.modifiedCount < 1) {
        throw new OperationError("failed to update");
    }

    if (wishItem.track) {
        await spwanOnDemandTask(wishItem.productId);
    }
}

export async function updateWishItem(id: string, wishItem: IWishItem, resetNextNotificationDate?: boolean) {
    if (_.isEmpty(id)) {
        throw new ArgumentError("id is empty");
    }

    if (_.isEmpty(wishItem)) {
        throw new ArgumentError("wishItem is empty");
    }

    if (wishItem.track) {
        if (resetNextNotificationDate) {
            setNextNotificationDate(wishItem);
        } else {
            wishItem.track.nextNotificationDate = new Date();
        }
    }

    validateWishItem(wishItem);

    var setter = {
        "wishlist.$.track": wishItem.track,
    };

    var result = await collection.updateOne(
        {
            "_id": new mongodb.ObjectID(id),
            "wishlist.productId": wishItem.productId
        },
        {
            $set: setter
        }
    );

    if (result.matchedCount < 1) {
        throw new NotFoundError("no such user or the item is not in wishlist");
    }

    if (wishItem.track) {
        await spwanOnDemandTask(wishItem.productId);
    }
}

export async function removeWishItem(id: string, productId: string) {
    if (_.isEmpty(id)) {
        throw new ArgumentError("id is empty");
    }

    if (_.isEmpty(productId)) {
        throw new ArgumentError("wishItem is empty");
    }

    var productOjectId = new mongodb.ObjectID(productId);
    var result = await collection.updateOne(
        {
            _id: new mongodb.ObjectID(id)
        },
        {
            $pull: {
                wishlist: {
                    productId: productOjectId
                }
            }
        }
    );

    if (result.matchedCount < 1) {
        throw new NotFoundError("no such user is not in wishlist");
    }

    if (result.modifiedCount < 1) {
        throw new OperationError("failed to remove");
    }
}

var wishItemValidator = new Validator();
wishItemValidator.addSchema(conditionalVerbsSchema);
wishItemValidator.addSchema(trackSchema);

function validateWishItem(obj: {}): IWishItem {
    var result = wishItemValidator.validate(obj, wishItemSchema);
    if (!result.valid) {
        throw new ArgumentError(result.toString());
    }

    var item: IWishItem = <IWishItem>obj;
    item["productId"] = new mongodb.ObjectID(obj["productId"]);
    return item;
}

var userValidator = new Validator();
userValidator.addSchema(conditionalVerbsSchema);
userValidator.addSchema(trackSchema);
userValidator.addSchema(wishItemSchema);

function validateUser(obj: {}): IUser {
    var result = userValidator.validate(obj, userSchema);
    if (!result.valid) {
        throw new ArgumentError(result.toString());
    }

    return <IUser>obj;
}

function setNextNotificationDate(item: IWishItem) {
    if (!item.track) return;

    var next = new Date();
    next.setHours(next.getHours() + item.track.cycle);
    item.track.nextNotificationDate = next;
}