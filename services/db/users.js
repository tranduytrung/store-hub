"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const mongodb = require("mongodb");
const Db = require("./db");
const _ = require("lodash");
const jsonschema_1 = require("jsonschema");
const error_1 = require("../../error");
const spawn_1 = require("../task/spawn");
var db = Db.getInstance();
var collection = db.collection("users");
var conditionalVerbsSchema = {
    id: "/ConditionalVerbs",
    type: "number",
    required: true
};
var trackSchema = {
    id: "/ITrack",
    type: ["object", null],
    additionalProperties: false,
    properties: {
        cycle: {
            type: "number",
            required: true
        },
        conditionalVerb: {
            $ref: "/ConditionalVerbs"
        },
        thresholdValue: {
            type: "number",
            required: true
        },
        nextNotificationDate: {
            type: "date",
            required: true
        }
    }
};
var wishItemSchema = {
    id: "/IWishItem",
    type: "object",
    required: true,
    additionalProperties: false,
    properties: {
        productId: {
            type: "string",
            required: true
        },
        track: {
            $ref: "/ITrack"
        }
    }
};
var userSchema = {
    id: "/IUser",
    type: "object",
    required: true,
    additionalProperties: false,
    properties: {
        firstName: {
            type: "string"
        },
        lastName: {
            type: "string"
        },
        email: {
            type: "string",
            required: true
        },
        wishlist: {
            type: "array",
            items: {
                $ref: "/IWishItem"
            }
        },
        facebookId: {
            type: "string"
        },
        googleId: {
            type: "string"
        },
        onesignalIds: {
            type: "array",
            items: {
                type: "string",
                required: true
            },
            required: true
        }
    }
};
function insert(user) {
    return __awaiter(this, void 0, Promise, function* () {
        validateUser(user);
        var setFields = _(user).omit(["email", "wishlist", "onesignalIds"]).omitBy(_.isNil).value();
        var result = yield collection.findOneAndUpdate({
            email: user.email
        }, {
            $set: setFields
        }, {
            upsert: true,
            returnOriginal: false
        });
        if (!result.ok) {
            throw new error_1.OperationError("failed to create or update user");
        }
        return result.value;
    });
}
exports.insert = insert;
function addOnesignal(userId, onesignalId) {
    return __awaiter(this, void 0, Promise, function* () {
        if (!_.isString(onesignalId)) {
            throw new error_1.ArgumentError("oneSignalId must be a string");
        }
        var removeQuery = {
            onesignalIds: onesignalId
        };
        var removeUpdate = {
            $pull: {
                onesignalIds: onesignalId
            }
        };
        var removeResult = yield collection.updateOne(removeQuery, removeUpdate);
        if (!removeResult.result.ok) {
            throw new error_1.OperationError("unable to remove OneSignal from old user");
        }
        var addQuery = {
            _id: new mongodb.ObjectID(userId)
        };
        var addUpdate = {
            $addToSet: {
                onesignalIds: onesignalId
            }
        };
        var addResult = yield collection.updateOne(addQuery, addUpdate);
        if (!addResult.result.ok) {
            throw new error_1.OperationError("unable to add OneSignal to user");
        }
    });
}
exports.addOnesignal = addOnesignal;
function getById(id) {
    return __awaiter(this, void 0, Promise, function* () {
        if (_.isEmpty(id)) {
            throw new error_1.ArgumentError("id is empty");
        }
        var result = yield collection.find({
            _id: new mongodb.ObjectID(id)
        }).limit(1).toArray();
        return _.isEmpty(result) ? null : result[0];
    });
}
exports.getById = getById;
function getMany(ids) {
    return __awaiter(this, void 0, Promise, function* () {
        var objectIds = [];
        for (let index = 0; index < ids.length; index++) {
            let id = ids[index];
            objectIds.push(_.isString(id) ? new mongodb.ObjectID(id) : id);
        }
        var result = yield collection.find({
            _id: { $in: objectIds }
        }).toArray();
        return result;
    });
}
exports.getMany = getMany;
function getByEmail(email) {
    return __awaiter(this, void 0, Promise, function* () {
        if (_.isEmpty(email)) {
            throw new error_1.ArgumentError("email is empty");
        }
        var result = yield collection.find({
            email: email
        }).limit(1).toArray();
        return _.isEmpty(result) ? null : result[0];
    });
}
exports.getByEmail = getByEmail;
function getByFacebookId(id) {
    return __awaiter(this, void 0, Promise, function* () {
        if (_.isEmpty(id)) {
            throw new error_1.ArgumentError("id is empty");
        }
        var result = yield collection.find({
            facebookId: id
        }).limit(1).toArray();
        return _.isEmpty(result) ? null : result[0];
    });
}
exports.getByFacebookId = getByFacebookId;
function getForBroadcast(productId, price) {
    if (_.isEmpty(productId)) {
        throw new error_1.ArgumentError("id is empty");
    }
    var query = {
        wishlist: {
            $elemMatch: {
                "productId": new mongodb.ObjectID(productId),
                "track.nextNotificationDate": { $lte: new Date() },
                "track.thresholdValue": { $gte: price }
            }
        }
    };
    var result = collection.find(query).toArray();
    return result;
}
exports.getForBroadcast = getForBroadcast;
function countByProductId(productId) {
    if (_.isEmpty(productId)) {
        throw new error_1.ArgumentError("id is empty");
    }
    var query = {
        "wishlist.productId": productId instanceof mongodb.ObjectID ? productId : new mongodb.ObjectID(productId)
    };
    var result = collection.count(query);
    return result;
}
exports.countByProductId = countByProductId;
function addWishItem(id, wishItem) {
    return __awaiter(this, void 0, void 0, function* () {
        if (_.isEmpty(id)) {
            throw new error_1.ArgumentError("id is empty");
        }
        if (_.isEmpty(wishItem)) {
            throw new error_1.ArgumentError("wishItem is empty");
        }
        if (wishItem.track) {
            wishItem.track.nextNotificationDate = new Date();
        }
        validateWishItem(wishItem);
        var result = yield collection.updateOne({
            _id: new mongodb.ObjectID(id),
            "wishlist.productId": {
                $ne: wishItem.productId
            }
        }, {
            $push: {
                wishlist: wishItem
            }
        });
        if (result.matchedCount < 1) {
            throw new error_1.NotFoundError("no such user or the item is already in wishlist");
        }
        if (result.modifiedCount < 1) {
            throw new error_1.OperationError("failed to update");
        }
        if (wishItem.track) {
            yield spawn_1.spwanOnDemandTask(wishItem.productId);
        }
    });
}
exports.addWishItem = addWishItem;
function updateWishItem(id, wishItem, resetNextNotificationDate) {
    return __awaiter(this, void 0, void 0, function* () {
        if (_.isEmpty(id)) {
            throw new error_1.ArgumentError("id is empty");
        }
        if (_.isEmpty(wishItem)) {
            throw new error_1.ArgumentError("wishItem is empty");
        }
        if (wishItem.track) {
            if (resetNextNotificationDate) {
                setNextNotificationDate(wishItem);
            }
            else {
                wishItem.track.nextNotificationDate = new Date();
            }
        }
        validateWishItem(wishItem);
        var setter = {
            "wishlist.$.track": wishItem.track,
        };
        var result = yield collection.updateOne({
            "_id": new mongodb.ObjectID(id),
            "wishlist.productId": wishItem.productId
        }, {
            $set: setter
        });
        if (result.matchedCount < 1) {
            throw new error_1.NotFoundError("no such user or the item is not in wishlist");
        }
        if (wishItem.track) {
            yield spawn_1.spwanOnDemandTask(wishItem.productId);
        }
    });
}
exports.updateWishItem = updateWishItem;
function removeWishItem(id, productId) {
    return __awaiter(this, void 0, void 0, function* () {
        if (_.isEmpty(id)) {
            throw new error_1.ArgumentError("id is empty");
        }
        if (_.isEmpty(productId)) {
            throw new error_1.ArgumentError("wishItem is empty");
        }
        var productOjectId = new mongodb.ObjectID(productId);
        var result = yield collection.updateOne({
            _id: new mongodb.ObjectID(id)
        }, {
            $pull: {
                wishlist: {
                    productId: productOjectId
                }
            }
        });
        if (result.matchedCount < 1) {
            throw new error_1.NotFoundError("no such user is not in wishlist");
        }
        if (result.modifiedCount < 1) {
            throw new error_1.OperationError("failed to remove");
        }
    });
}
exports.removeWishItem = removeWishItem;
var wishItemValidator = new jsonschema_1.Validator();
wishItemValidator.addSchema(conditionalVerbsSchema);
wishItemValidator.addSchema(trackSchema);
function validateWishItem(obj) {
    var result = wishItemValidator.validate(obj, wishItemSchema);
    if (!result.valid) {
        throw new error_1.ArgumentError(result.toString());
    }
    var item = obj;
    item["productId"] = new mongodb.ObjectID(obj["productId"]);
    return item;
}
var userValidator = new jsonschema_1.Validator();
userValidator.addSchema(conditionalVerbsSchema);
userValidator.addSchema(trackSchema);
userValidator.addSchema(wishItemSchema);
function validateUser(obj) {
    var result = userValidator.validate(obj, userSchema);
    if (!result.valid) {
        throw new error_1.ArgumentError(result.toString());
    }
    return obj;
}
function setNextNotificationDate(item) {
    if (!item.track)
        return;
    var next = new Date();
    next.setHours(next.getHours() + item.track.cycle);
    item.track.nextNotificationDate = next;
}
//# sourceMappingURL=users.js.map