import * as Db from "./db"
import {ObjectID} from "mongodb"
import * as _ from "lodash";
import {ArgumentError, OperationError} from "../../error";
import {IAd} from "../../models/ad";

var db = Db.getInstance();
var collection = db.collection("ads");

export async function insert(ads : IAd[]) {
    var result = await collection.insertMany(ads);
    if (!result.result.ok) {
        throw new OperationError("cannot insert ads");
    }
}

export async function clear(source : string) {
    var result = await collection.deleteMany({source: source}); 
    if (!result.result.ok) {
        throw new OperationError("cannot insert ads");
    }
}

export async function getSources() : Promise<string[]> {
    var result = await collection.distinct("source", {});
    return result;
}

export async function getBySource(source : string = null) : Promise<IAd[]> {
    var result = await collection.find(source? {source: source} : {}).toArray();
    return result;
}