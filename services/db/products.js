"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const Db = require("./db");
const mongodb_1 = require("mongodb");
const _ = require("lodash");
const error_1 = require("../../error");
const date_1 = require("../../utils/date");
const similarity_1 = require("../similarity");
const product_1 = require("../../models/product");
const notification_1 = require("../notification");
const winston = require("winston");
var db = Db.getInstance();
exports.collection = db.collection("products");
var productBriefProjection = {
    source: true,
    name: true,
    url: true,
    prices: { $slice: 1 },
    priceUnit: true,
    media: true,
    category: true,
    groupId: true,
    updatedDate: true,
    deletedDate: true
};
var productBriefProjectionWithScore = _.extend(productBriefProjection, {
    score: {
        $meta: "textScore"
    }
});
var similarityTester = new similarity_1.ProductSimilarity();
var similarityThreshold = 0.5;
function getGroupId(product, query) {
    return __awaiter(this, void 0, Promise, function* () {
        var bestMatchingProduct = null;
        var bestScore = 0;
        query = query || {
            category: product.category
        };
        var cursor = exports.collection.find(query).batchSize(50);
        while (yield cursor.hasNext()) {
            var targetProduct = yield cursor.next();
            if (targetProduct._id.equals(product._id))
                continue;
            var score = similarityTester.test(product, targetProduct);
            if (score > bestScore) {
                bestScore = score;
                bestMatchingProduct = targetProduct;
                if (score === 1) {
                    break;
                }
            }
        }
        if (bestScore < similarityThreshold) {
            winston.info(`no matching product with ${product.name}`, { source: "db.products", url: product.url });
            return new mongodb_1.ObjectID();
        }
        winston.info(`${product.name} matches with ${bestMatchingProduct.name} at score ${bestScore}`, { source: "db.products", url: product.url, target: bestMatchingProduct.url });
        return bestMatchingProduct.groupId;
    });
}
exports.getGroupId = getGroupId;
function upsert(product) {
    return __awaiter(this, void 0, Promise, function* () {
        var queryResult = yield exports.collection.find({
            url: product.url
        }).limit(1).toArray();
        if (_.isEmpty(queryResult)) {
            // insert new
            let newValue = {
                category: product.category,
                url: product.url,
                description: product.description,
                media: product.media,
                name: product.name,
                prices: [{
                        date: new Date(),
                        value: product.prices[0].value || null
                    }],
                priceUnit: product.priceUnit,
                source: product.source,
                specification: product.specification,
                updatedDate: new Date(),
                createdDate: new Date(),
                deletedDate: null,
                groupId: yield getGroupId(product),
            };
            let result = yield exports.collection.insertOne(newValue);
            if (!result.result.ok) {
                throw new error_1.OperationError(`unable to insert ${newValue.url}`);
            }
            winston.info(`inserted ${product.name} to database`, { source: "db.products", operation: "insert" });
            return result.insertedId;
        }
        else {
            var now = new Date();
            var oldValue = queryResult[0];
            // update it        
            let updateQuery = {
                $set: {
                    category: product.category,
                    description: product.description,
                    media: product.media,
                    name: product.name,
                    source: product.source,
                    specification: product.specification,
                    updatedDate: now,
                    groupId: oldValue.name !== product.name ?
                        yield getGroupId(product) :
                        new mongodb_1.ObjectID(oldValue.groupId.toString())
                }
            };
            if (!date_1.diffInMiliseconds(oldValue.prices[0].date)) {
                updateQuery["$set"][`prices.0`] = {
                    date: now,
                    value: product.prices[0].value
                };
            }
            else {
                updateQuery["$push"] = {
                    prices: {
                        $each: [{
                                date: now,
                                value: product.prices[0].value || null
                            }],
                        $position: 0
                    }
                };
            }
            let result = yield exports.collection.findOneAndUpdate({
                _id: oldValue["_id"]
            }, updateQuery, { returnOriginal: false });
            if (!result.ok) {
                throw new error_1.OperationError(`unable to update ${product.url}`);
            }
            winston.info(`updated ${product.name} to database`, { source: "db.products", operation: "update" });
            yield notification_1.broadcastProductUpdate(result.value);
            return result.value._id;
        }
    });
}
exports.upsert = upsert;
function regroupAll() {
    return __awaiter(this, void 0, void 0, function* () {
        var pivotDate = new Date();
        var cursor = exports.collection.find({});
        var product;
        while (product = yield cursor.next()) {
            let groupId = yield getGroupId(product, {
                category: product.category,
                updatedDate: {
                    $gte: pivotDate
                }
            });
            var result = yield exports.collection.updateOne({
                _id: typeof product._id === "ObjectID" ? product._id : new mongodb_1.ObjectID(product._id.toString())
            }, {
                $set: {
                    groupId: groupId,
                    updatedDate: new Date()
                }
            });
            if (!result.result.ok) {
                throw new error_1.OperationError(`unable to update ${product["_id"]}`);
            }
        }
    });
}
exports.regroupAll = regroupAll;
function getNewArrivals(lastId = "ffffffffffffffffffffffff", limit = 20) {
    return __awaiter(this, void 0, Promise, function* () {
        var result = yield exports.collection
            .find({
            _id: { $lt: new mongodb_1.ObjectID(lastId) },
            deletedDate: null,
        })
            .project(productBriefProjection)
            .sort({
            _id: -1
        })
            .limit(limit)
            .toArray();
        var briefs = reshapeProductsBrief(result);
        return briefs;
    });
}
exports.getNewArrivals = getNewArrivals;
function get(id) {
    return __awaiter(this, void 0, Promise, function* () {
        var result = yield exports.collection.find({ _id: _.isString(id) ? new mongodb_1.ObjectID(id) : id }).limit(1).toArray();
        return _.isEmpty(result) ? null : result[0];
    });
}
exports.get = get;
function getBrief(id) {
    return __awaiter(this, void 0, Promise, function* () {
        var result = yield exports.collection.find({ _id: _.isString(id) ? new mongodb_1.ObjectID(id) : id }).project(productBriefProjection).limit(1).toArray();
        if (result.length !== 0) {
            var brief = reshapeProductBrief(result[0]);
            return brief;
        }
        return null;
    });
}
exports.getBrief = getBrief;
function getByUrl(url) {
    return __awaiter(this, void 0, Promise, function* () {
        var result = yield exports.collection.find({ url: url }).project(productBriefProjection).limit(1).toArray();
        if (result.length !== 0) {
            var brief = reshapeProductBrief(result[0]);
            return brief;
        }
        return null;
    });
}
exports.getByUrl = getByUrl;
function getGroupByProductIds(ids) {
    return __awaiter(this, void 0, void 0, function* () {
        if (!_.isArray(ids)) {
            throw new error_1.ArgumentError("ids is not array");
        }
        var productIds;
        try {
            productIds = _.map(ids, function (id) {
                return new mongodb_1.ObjectID(id);
            });
        }
        catch (error) {
            throw new error_1.ArgumentError(error.message);
        }
        var products = yield exports.collection.find({
            _id: { $in: productIds }
        }).project({
            groupId: true
        }).toArray();
        var groupIds = _.map(products, function (product) {
            return new mongodb_1.ObjectID(product.groupId);
        });
        var rawGroups = yield exports.collection.aggregate([])
            .match({
            groupId: groupIds
        })
            .group({
            _id: "$groupId",
            products: {
                $push: {
                    _id: "$_id",
                    url: "$url",
                    prices: "$prices",
                    priceUnit: "$priceUnit",
                    name: "$name",
                    source: "$source",
                    category: "$category",
                    media: "$media"
                }
            }
        })
            .toArray();
        var groups = reshapeProductGroups(rawGroups);
        groups.forEach(function (group) {
            var product = _.find(products, ["groupId", group._id]);
            var index = _.findIndex(group.products, ["_id", product["_id"]]);
            if (index === 0)
                return;
            var t = group.products[index];
            group.products[index] = group.products[0];
            group.products[0] = t;
        });
        return groups;
    });
}
exports.getGroupByProductIds = getGroupByProductIds;
function getMany(ids) {
    return __awaiter(this, void 0, Promise, function* () {
        if (!_.isArray(ids)) {
            throw new error_1.ArgumentError("ids is not array");
        }
        var objIds;
        try {
            objIds = _.map(ids, function (id) {
                return new mongodb_1.ObjectID(id);
            });
        }
        catch (error) {
            throw new error_1.ArgumentError(error.message);
        }
        var products = yield exports.collection
            .find({
            _id: {
                $in: objIds
            }
        })
            .project(productBriefProjection)
            .toArray();
        var briefs = reshapeProductsBrief(products);
        return briefs;
    });
}
exports.getMany = getMany;
function getProductsByGroupId(groupId, skip = 0, limit = 20) {
    return __awaiter(this, void 0, Promise, function* () {
        var objId = new mongodb_1.ObjectID(groupId);
        return yield exports.collection
            .find({
            groupId: objId,
            deletedDate: null
        })
            .sort({
            "prices.0.value": 1
        })
            .skip(skip)
            .limit(limit)
            .toArray();
    });
}
exports.getProductsByGroupId = getProductsByGroupId;
function search(text, options) {
    return __awaiter(this, void 0, Promise, function* () {
        options = _.defaults(options, {
            categories: null,
            sources: null,
            sort: product_1.ProductSortType.relevance,
            skip: 0,
            limit: 20
        });
        var query = {};
        var hasText = _.isString(text);
        if (hasText) {
            query = {
                deletedDate: null,
                $text: { $search: text }
            };
        }
        if (options.categories) {
            query["category"] = {
                $in: options.categories
            };
        }
        if (options.sources) {
            query["source"] = {
                $in: options.sources
            };
        }
        var sort = {};
        switch (options.sort) {
            case product_1.ProductSortType.priceAsc:
                sort = { lowestPrice: 1 };
                break;
            case product_1.ProductSortType.priceDesc:
                sort = { lowestPrice: -1 };
                break;
            case product_1.ProductSortType.newest:
                sort = { premiereDate: -1 };
                break;
            case product_1.ProductSortType.oldest:
                sort = { premiereDate: 1 };
                break;
            case product_1.ProductSortType.relevance:
            default:
                if (hasText)
                    sort = { score: -1 };
                else
                    sort = { premiereDate: -1 };
                break;
        }
        var result = yield exports.collection.aggregate([])
            .match(query)
            .unwind("$prices")
            .group({
            _id: "$_id",
            groupId: { $first: "$groupId" },
            url: { $first: "$url" },
            price: { $first: "$prices" },
            prices: { $push: "$prices" },
            priceUnit: { $first: "$priceUnit" },
            name: { $first: "$name" },
            source: { $first: "$source" },
            category: { $first: "$category" },
            media: { $first: "$media" },
            createdDate: { $first: "$createdDate" }
        })
            .group({
            _id: "$groupId",
            products: {
                $push: {
                    _id: "$_id",
                    url: "$url",
                    prices: "$prices",
                    priceUnit: "$priceUnit",
                    name: "$name",
                    source: "$source",
                    category: "$category",
                    media: "$media"
                }
            },
            score: {
                $max: { $meta: "textScore" }
            },
            lowestPrice: {
                $min: "$price.value"
            },
            premiereDate: {
                $min: "$createdDate"
            }
        })
            .sort(sort)
            .skip(options.skip)
            .limit(options.limit)
            .toArray();
        var briefs = reshapeProductGroups(result);
        return briefs;
    });
}
exports.search = search;
function deleteOutdated(days = 7) {
    return __awaiter(this, void 0, void 0, function* () {
        var milisec = days * 86400000;
        var query = {
            deletedDate: null,
            updatedDate: {
                $lt: date_1.getRelativeDateByMiliseconds(-milisec)
            }
        };
        var update = {
            $set: { deletedDate: new Date() }
        };
        var result = yield exports.collection.updateMany(query, update);
        if (!result.result.ok) {
            throw new error_1.OperationError("cannot delete outdated products");
        }
    });
}
exports.deleteOutdated = deleteOutdated;
function reshapeProductBrief(product) {
    var media = _.filter(product["media"], ["type", product_1.MediaType.thumbnail]);
    if (media.length === 0) {
        media = [product["media"][0]];
    }
    return {
        _id: product["_id"],
        url: product["url"],
        category: product["category"],
        name: product["name"],
        source: product["source"],
        priceUnit: product["priceUnit"],
        price: _.first(product["prices"])["value"],
        media: media || [],
        groupId: product["groupId"],
        updatedDate: product["updatedDate"],
        deletedDate: product["deletedDate"]
    };
}
function reshapeProductsBrief(products) {
    return _.map(products, function (product) {
        return reshapeProductBrief(product);
    });
}
function reshapeProductGroups(groups) {
    // NOTE:
    // in future when mongodb v3.2 is available, $arrayElemAt can use to get last price element
    // futher more, $filter in aggregation can use same as $elemMatch in query
    return _.map(groups, function (group) {
        var products = _.map(group.products, function (product) {
            return reshapeProductBrief(product);
        });
        return {
            _id: group["_id"],
            products: products
        };
    });
}
//# sourceMappingURL=products.js.map