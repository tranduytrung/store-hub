import * as Db from "./db"
import {ObjectID} from "mongodb"
import * as _ from "lodash"
import {ArgumentError, OperationError} from "../../error"
import {diffInMiliseconds, getRelativeDateByMiliseconds} from "../../utils/date";
import {ProductSimilarity} from "../similarity"
import {MediaType, IPricePoint, IProduct, IProductBrief, IProductGroup, IProductMedia, ProductCategory, ProductSortType} from "../../models/product"
import {broadcastProductUpdate} from "../notification"
import * as winston from "winston"

var db = Db.getInstance();
export var collection = db.collection("products");

var productBriefProjection = {
    source: true,
    name: true,
    url: true,
    prices: { $slice: 1 },
    priceUnit: true,
    media: true,
    category: true,
    groupId: true,
    updatedDate: true,
    deletedDate: true
};

var productBriefProjectionWithScore = _.extend(productBriefProjection, {
    score: {
        $meta: "textScore"
    }
});

var similarityTester = new ProductSimilarity();
var similarityThreshold = 0.5;
export async function getGroupId(product: IProduct, query?: any) : Promise<ObjectID> {
        var bestMatchingProduct: IProduct = null;
        var bestScore = 0;
        query = query || {
            category: product.category
        };

        var cursor = collection.find(query).batchSize(50);
        while (await cursor.hasNext()) {
            var targetProduct: IProduct = await cursor.next();
            if (targetProduct._id.equals(product._id)) continue;

            var score = similarityTester.test(product, targetProduct);
            if (score > bestScore) {
                bestScore = score;
                bestMatchingProduct = targetProduct;

                if (score === 1) {
                    break;
                }
            }
        }

        if (bestScore < similarityThreshold) {
            winston.info(`no matching product with ${product.name}`, { source: "db.products", url: product.url });
            return new ObjectID();
        }

        winston.info(`${product.name} matches with ${bestMatchingProduct.name} at score ${bestScore}`, 
        { source: "db.products", url: product.url, target: bestMatchingProduct.url });

        return bestMatchingProduct.groupId;
}

export async function upsert(product: IProduct): Promise<ObjectID> {
    var queryResult = await collection.find({
        url: product.url
    }).limit(1).toArray();

    if (_.isEmpty(queryResult)) {
        // insert new
        let newValue: IProduct = {
            category: product.category,
            url: product.url,
            description: product.description,
            media: product.media,
            name: product.name,
            prices: [{
                date: new Date(),
                value: product.prices[0].value || null
            }],
            priceUnit: product.priceUnit,
            source: product.source,
            specification: product.specification,
            updatedDate: new Date(),
            createdDate: new Date(),
            deletedDate: null,
            groupId: await getGroupId(product),
        };

        let result = await collection.insertOne(newValue);
        if (!result.result.ok) {
            throw new OperationError(`unable to insert ${newValue.url}`);
        }

        winston.info(`inserted ${product.name} to database`, { source: "db.products", operation: "insert" });

        return result.insertedId;
    } else {
        var now = new Date();
        var oldValue: IProduct = queryResult[0];
        // update it        
        let updateQuery = {
            $set: {
                category: product.category,
                description: product.description,
                media: product.media,
                name: product.name,
                source: product.source,
                specification: product.specification,
                updatedDate: now,
                groupId: oldValue.name !== product.name ?
                    await getGroupId(product) :
                    new ObjectID(oldValue.groupId.toString())
            }
        };

        if (!diffInMiliseconds(oldValue.prices[0].date)) {
            updateQuery["$set"][`prices.0`] = {
                date: now,
                value: product.prices[0].value
            };
        } else {
            updateQuery["$push"] = {
                prices: {
                    $each: [{
                        date: now,
                        value: product.prices[0].value || null
                    }],
                    $position: 0
                }
            };
        }

        let result = await collection.findOneAndUpdate({
            _id: oldValue["_id"]
        }, updateQuery, { returnOriginal: false });

        if (!result.ok) {
            throw new OperationError(`unable to update ${product.url}`);
        }

        winston.info(`updated ${product.name} to database`, { source: "db.products", operation: "update" });

        await broadcastProductUpdate(result.value);
        return result.value._id;
    }
}

export async function regroupAll() {
    var pivotDate = new Date();
    var cursor = collection.find({});

    var product: IProduct;
    while (product = await cursor.next()) {
        let groupId = await getGroupId(product, {
            category: product.category,
            updatedDate: {
                $gte: pivotDate
            }
        });

        var result = await collection.updateOne(
            {
                _id: typeof product._id === "ObjectID"? product._id : new ObjectID(product._id.toString())
            },
            {
                $set: {
                    groupId: groupId,
                    updatedDate: new Date()
                }
            }
        );

        if (!result.result.ok) {
            throw new OperationError(`unable to update ${product["_id"]}`);
        }
    }
}

export async function getNewArrivals(lastId: string = "ffffffffffffffffffffffff", limit: number = 20): Promise<IProductBrief[]> {
    var result = await collection
        .find({
            _id: { $lt: new ObjectID(lastId) },
            deletedDate: null,
        })
        .project(productBriefProjection)
        .sort({
            _id: -1
        })
        .limit(limit)
        .toArray();

    var briefs = reshapeProductsBrief(result);

    return briefs;
}

export async function get(id: string | ObjectID): Promise<IProduct> {
    var result = await collection.find({ _id: _.isString(id) ? new ObjectID(id) : id }).limit(1).toArray();
    return _.isEmpty(result) ? null : result[0];
}

export async function getBrief(id: string | ObjectID): Promise<IProductBrief> {
    var result = await collection.find({ _id: _.isString(id) ? new ObjectID(id) : id }).project(productBriefProjection).limit(1).toArray();
    if (result.length !== 0) {
        var brief = reshapeProductBrief(result[0]);
        return brief;
    }

    return null;
}

export async function getByUrl(url: string): Promise<IProductBrief> {
    var result = await collection.find({ url }).project(productBriefProjection).limit(1).toArray();
    if (result.length !== 0) {
        var brief = reshapeProductBrief(result[0]);
        return brief;
    }

    return null;
}

export async function getGroupByProductIds(ids: string[]) {
    if (!_.isArray(ids)) {
        throw new ArgumentError("ids is not array");
    }

    var productIds: ObjectID[];

    try {
        productIds = _.map(ids, function (id) {
            return new ObjectID(id);
        });
    } catch (error) {
        throw new ArgumentError(error.message);
    }

    var products = await collection.find({
        _id: { $in: productIds }
    }).project({
        groupId: true
    }).toArray();

    var groupIds = _.map(products, function (product) {
        return new ObjectID(product.groupId);
    });

    var rawGroups = await collection.aggregate([])
        .match({
            groupId: groupIds
        })
        .group({
            _id: "$groupId",
            products: {
                $push: {
                    _id: "$_id",
                    url: "$url",
                    prices: "$prices",
                    priceUnit: "$priceUnit",
                    name: "$name",
                    source: "$source",
                    category: "$category",
                    media: "$media"
                }
            }
        })
        .toArray();

    var groups = reshapeProductGroups(rawGroups);
    groups.forEach(function (group) {
        var product = _.find(products, ["groupId", group._id]);
        var index = _.findIndex(group.products, ["_id", product["_id"]]);

        if (index === 0) return;
        var t = group.products[index];
        group.products[index] = group.products[0];
        group.products[0] = t;
    });

    return groups;
}

export async function getMany(ids: string[]): Promise<IProductBrief[]> {
    if (!_.isArray(ids)) {
        throw new ArgumentError("ids is not array");
    }

    var objIds: ObjectID[];

    try {
        objIds = _.map(ids, function (id) {
            return new ObjectID(id);
        });
    } catch (error) {
        throw new ArgumentError(error.message);
    }

    var products: IProductBrief[] = await collection
        .find({
            _id: {
                $in: objIds
            }
        })
        .project(productBriefProjection)
        .toArray();

    var briefs = reshapeProductsBrief(products);

    return briefs;
}

export async function getProductsByGroupId(groupId: string, skip: number = 0, limit: number = 20): Promise<IProduct[]> {
    var objId = new ObjectID(groupId);

    return await collection
        .find({
            groupId: objId,
            deletedDate: null
        })
        .sort({
            "prices.0.value": 1
        })
        .skip(skip)
        .limit(limit)
        .toArray();
}

interface ISearchOptions {
    categories?: ProductCategory[]
    sources?: string[]
    sort?: ProductSortType
    skip?: number
    limit?: number
}

export async function search(text: string, options: ISearchOptions): Promise<IProductGroup[]> {
    options = _.defaults<ISearchOptions, ISearchOptions>(options, {
        categories: null,
        sources: null,
        sort: ProductSortType.relevance,
        skip: 0,
        limit: 20
    });

    var query = {}
    var hasText = _.isString(text);
    if (hasText) {
        query = {
            deletedDate: null,
            $text: { $search: text }
        };
    }

    if (options.categories) {
        query["category"] = {
            $in: options.categories
        };
    }

    if (options.sources) {
        query["source"] = {
            $in: options.sources
        };
    }

    var sort = {};
    switch (options.sort) {
        case ProductSortType.priceAsc:
            sort = { lowestPrice: 1 };
            break;
        case ProductSortType.priceDesc:
            sort = { lowestPrice: -1 };
            break;
        case ProductSortType.newest:
            sort = { premiereDate: -1 };
            break;
        case ProductSortType.oldest:
            sort = { premiereDate: 1 };
            break;
        case ProductSortType.relevance:
        default:
            if (hasText) sort = { score: -1 };
            else sort = { premiereDate: -1 };
            break;
    }

    var result = await collection.aggregate([])
        .match(query)
        .unwind("$prices")
        .group({
            _id: "$_id",
            groupId: { $first: "$groupId" },
            url: { $first: "$url" },
            price: { $first: "$prices" },
            prices: { $push: "$prices" },
            priceUnit: { $first: "$priceUnit" },
            name: { $first: "$name" },
            source: { $first: "$source" },
            category: { $first: "$category" },
            media: { $first: "$media" },
            createdDate: { $first: "$createdDate" }
        })
        .group({
            _id: "$groupId",
            products: {
                $push: {
                    _id: "$_id",
                    url: "$url",
                    prices: "$prices",
                    priceUnit: "$priceUnit",
                    name: "$name",
                    source: "$source",
                    category: "$category",
                    media: "$media"
                }
            },
            score: {
                $max: { $meta: "textScore" }
            },
            lowestPrice: {
                $min: "$price.value"
            },
            premiereDate: {
                $min: "$createdDate"
            }
        })
        .sort(sort)
        .skip(options.skip)
        .limit(options.limit)
        .toArray();

    var briefs = reshapeProductGroups(result);
    return briefs;
}

export async function deleteOutdated(days: number = 7) {
    var milisec = days * 86400000;
    var query = {
        deletedDate: null,
        updatedDate: {
            $lt: getRelativeDateByMiliseconds(-milisec)
        }
    };

    var update = {
        $set: { deletedDate: new Date() }
    };

    var result = await collection.updateMany(query, update);
    if (!result.result.ok) {
        throw new OperationError("cannot delete outdated products");
    }
}

function reshapeProductBrief(product): IProductBrief {
    var media = _.filter<any, IProductMedia>(product["media"], ["type", MediaType.thumbnail]);
    if (media.length === 0) {
        media = [product["media"][0]];
    }

    return {
        _id: product["_id"],
        url: product["url"],
        category: product["category"],
        name: product["name"],
        source: product["source"],
        priceUnit: product["priceUnit"],
        price: _.first(product["prices"])["value"],
        media: media || [],
        groupId: product["groupId"],
        updatedDate: product["updatedDate"],
        deletedDate: product["deletedDate"]
    };
}

function reshapeProductsBrief(products: any[]): IProductBrief[] {
    return _.map(products, function (product) {
        return reshapeProductBrief(product);
    });
}

function reshapeProductGroups(groups) {

    // NOTE:
    // in future when mongodb v3.2 is available, $arrayElemAt can use to get last price element
    // futher more, $filter in aggregation can use same as $elemMatch in query

    return _.map<any, IProductGroup>(groups, function (group) {
        var products = _.map<any, IProductBrief>(group.products, function (product) {
            return reshapeProductBrief(product);
        });

        return {
            _id: group["_id"],
            products: products
        };
    });
}
