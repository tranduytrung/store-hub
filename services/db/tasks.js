"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const Db = require("./db");
const mongodb_1 = require("mongodb");
const task_1 = require("../../models/task");
const _ = require("lodash");
const error_1 = require("../../error");
const config_1 = require("../../config");
var db = Db.getInstance();
var collection = db.collection("tasks");
function upsert(task) {
    return __awaiter(this, void 0, void 0, function* () {
        task = _.omitBy(task, _.isUndefined);
        var result = yield collection.updateOne({
            _id: task._id
        }, {
            $set: task
        }, {
            upsert: true
        });
        if (!result.result.ok) {
            throw new error_1.OperationError("unable to upsert a task");
        }
    });
}
exports.upsert = upsert;
function popNow(excludedIds) {
    return __awaiter(this, void 0, Promise, function* () {
        var query = {
            $or: [
                {
                    assignee: null,
                    state: task_1.TaskState.scheduled,
                },
                {
                    assignee: config_1.taskExecutor.name,
                    state: {
                        $in: [task_1.TaskState.scheduled, task_1.TaskState.running]
                    }
                }
            ],
            nextRun: {
                $lte: new Date()
            }
        };
        if (excludedIds && excludedIds.length > 0) {
            query["_id"] = { $nin: excludedIds };
        }
        var update = {
            $set: {
                assignee: config_1.taskExecutor.name,
                state: task_1.TaskState.running,
                lastRun: new Date()
            }
        };
        var result = yield collection.findOneAndUpdate(query, update, {
            returnOriginal: false,
            sort: { nextRun: 1 }
        });
        if (!result.ok) {
            throw new error_1.OperationError("cannot retrieve task");
        }
        return result.value;
    });
}
exports.popNow = popNow;
function get(id) {
    return __awaiter(this, void 0, Promise, function* () {
        var query = {
            _id: _.isString(id) ? new mongodb_1.ObjectID(id) : id
        };
        var result = yield collection.find(query).limit(1).toArray();
        return result[0];
    });
}
exports.get = get;
function getEssentialTasks() {
    var query = {
        executor: {
            $ne: "updateProduct"
        }
    };
    return collection.find(query).toArray();
}
exports.getEssentialTasks = getEssentialTasks;
//# sourceMappingURL=tasks.js.map