import * as Db from "./db"
import {ObjectID} from "mongodb"
import {ITask, TaskState} from "../../models/task"
import * as _ from "lodash"
import {ArgumentError, OperationError} from "../../error"
import {taskExecutor as taskExecutorConfig} from "../../config"

var db = Db.getInstance();
var collection = db.collection("tasks");

export async function upsert(task: ITask) {
    task = _.omitBy<ITask, any>(task, _.isUndefined);
    var result = await collection.updateOne(
        {
            _id: task._id
        },
        {
            $set: task
        },
        {
            upsert: true
        }
    );

    if (!result.result.ok) {
        throw new OperationError("unable to upsert a task");
    }
}

export async function popNow(excludedIds?: ObjectID[]): Promise<ITask> {
    var query = {
        $or: [
            {
                assignee: null,
                state: TaskState.scheduled,
            },
            {
                assignee: taskExecutorConfig.name,
                state: {
                    $in: [TaskState.scheduled, TaskState.running]
                }
            }
        ],
        nextRun: {
            $lte: new Date()
        }
    };

    if (excludedIds && excludedIds.length > 0) {
        query["_id"] = { $nin: excludedIds };
    }

    var update = {
        $set: {
            assignee: taskExecutorConfig.name,
            state: TaskState.running,
            lastRun: new Date()
        }
    };

    var result = await collection.findOneAndUpdate(query, update, {
        returnOriginal: false,
        sort: { nextRun: 1 }
    });

    if (!result.ok) {
        throw new OperationError("cannot retrieve task");
    }

    return result.value;
}

export async function get(id: ObjectID | string): Promise<ITask> {
    var query = {
        _id: _.isString(id) ? new ObjectID(id) : id
    };

    var result = await collection.find(query).limit(1).toArray();
    return result[0];
}

export function getEssentialTasks() {
    var query = {
        executor: {
            $ne: "updateProduct"
        }
    }

    return collection.find(query).toArray();
}