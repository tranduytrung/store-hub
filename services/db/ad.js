"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const Db = require("./db");
const error_1 = require("../../error");
var db = Db.getInstance();
var collection = db.collection("ads");
function insert(ads) {
    return __awaiter(this, void 0, void 0, function* () {
        var result = yield collection.insertMany(ads);
        if (!result.result.ok) {
            throw new error_1.OperationError("cannot insert ads");
        }
    });
}
exports.insert = insert;
function clear(source) {
    return __awaiter(this, void 0, void 0, function* () {
        var result = yield collection.deleteMany({ source: source });
        if (!result.result.ok) {
            throw new error_1.OperationError("cannot insert ads");
        }
    });
}
exports.clear = clear;
function getSources() {
    return __awaiter(this, void 0, Promise, function* () {
        var result = yield collection.distinct("source", {});
        return result;
    });
}
exports.getSources = getSources;
function getBySource(source = null) {
    return __awaiter(this, void 0, Promise, function* () {
        var result = yield collection.find(source ? { source: source } : {}).toArray();
        return result;
    });
}
exports.getBySource = getBySource;
//# sourceMappingURL=ad.js.map