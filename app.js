"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const Hapi = require("hapi");
const Db = require("./services/db/db");
const CrawlerCore = require("./services/crawler/core");
const config = require("./config");
const bootstrap_1 = require("./utils/bootstrap");
const parseArgv_1 = require("./utils/parseArgv");
const winston = require("winston");
if (process.env.NODE_ENV !== "development") {
    require('winston-loggly');
    winston.add(winston.transports.Loggly, {
        level: "verbose",
        token: "31d3a055-3b8c-464c-9d37-79c97e941e76",
        subdomain: "tranduytrung",
        tags: ["store-hub"],
        json: true
    });
}
else {
    winston.add(winston.transports.File, {
        level: "verbose",
        filename: "store-hub.log.json"
    });
}
var args = parseArgv_1.getArgs();
var modeArg = args["mode"];
var mode = bootstrap_1.BootstrapMode[process.env.storehub_mode] || bootstrap_1.BootstrapMode[modeArg] || bootstrap_1.BootstrapMode.Full;
function initialize() {
    return __awaiter(this, void 0, Promise, function* () {
        yield Db.initialize();
        let server = new Hapi.Server();
        server.connection(config.server.connection);
        yield server.register({
            register: require('good'),
            options: {
                reporters: {
                    consoleReporter: [{
                            module: "good-console"
                        }]
                }
            }
        });
        // bell auth
        yield server.register(require("bell"));
        server.auth.strategy('facebook', 'bell', false, {
            provider: 'facebook',
            password: config.cookieSecret,
            clientId: config.facebook.clientId,
            clientSecret: config.facebook.clientSecret,
            isSecure: false,
            scope: config.facebook.scope
        });
        // jwt auth
        yield server.register(require("hapi-auth-jwt2"));
        server.auth.strategy("jwt", "jwt", "required", {
            validateFunc: function (decoded, request, callback) {
                callback(null, true);
            },
            key: config.jwt.secretKey,
            verifyOptions: { algorithms: [config.jwt.algorithm] }
        });
        yield bootstrap_1.Bootstrap.registerAPIs(server, mode);
        yield bootstrap_1.Bootstrap.registerMethods(server);
        if (mode & bootstrap_1.BootstrapMode.Administration) {
            yield CrawlerCore.init();
            yield require("./services/task").initialize();
        }
        yield server.register(require("vision"));
        server.views({
            engines: { jade: require("jade") },
            path: "static/templates"
        });
        yield server.start();
        return server;
    });
}
initialize().then((server) => {
    winston.info(`Server started`, { source: "server", mode: bootstrap_1.BootstrapMode[mode] });
}).catch((err) => {
    winston.error(`Server error on starting`, err);
});
process.on('uncaughtException', (err) => {
    winston.error(`Caught exception: ${err}`);
});
//# sourceMappingURL=app.js.map