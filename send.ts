import * as Db from "./services/db/db"

async function go() {
    await Db.initialize();
    var Products = require("./services/db/products");
    var Notification = require("./services/notification");
    var product = await Products.get("57ad63c1418c6bcc36860564");
    await Notification.broadcastProductUpdate(product, ["5788f309f79408de06fcdb3a"]);
}

go().then(function() {
    console.log("done");
}).catch(function(e) {
    console.error(e);
})