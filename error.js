"use strict";
const _ = require("lodash");
const Boom = require("boom");
const winston = require("winston");
/**
* NotFoundError
* Represent no data is found
*/
class NotFoundError extends Error {
    constructor(message) {
        super(message);
    }
}
exports.NotFoundError = NotFoundError;
/**
* ArgumentError
* Represent invalid argument passed into a function
*/
class ArgumentError extends Error {
    constructor(message) {
        super(message);
    }
}
exports.ArgumentError = ArgumentError;
/**
 * OperationError
 * Error while operation is processing
 */
class OperationError extends Error {
    constructor(message) {
        super(message);
    }
}
exports.OperationError = OperationError;
function handleError(reply, error) {
    if (error.isBoom) {
        return reply(error);
    }
    var instanceName = error.constructor.name;
    switch (instanceName) {
        case "NotFoundError":
            reply(Boom.notFound(error.message));
            break;
        case "ArgumentError":
            reply(Boom.badData(error.message));
            break;
        case "OperationError":
            reply(Boom.expectationFailed(error.message));
            break;
        case "String":
            reply(Boom.badImplementation(error.message));
            break;
        default:
            reply(Boom.badImplementation("unexpected error", error));
            break;
    }
    winston.error(error);
}
exports.handleError = handleError;
function parseNumber(name, value, required = false) {
    if (_.isNil(value) && !required)
        return undefined;
    if (_.isString(value)) {
        var n = Number(value);
        if (!isNaN(n))
            return n;
    }
    throw new ArgumentError(`Invalid data type. ${name} must be a number`);
}
exports.parseNumber = parseNumber;
function parseString(name, value, required = false) {
    if (_.isNil(value) && !required)
        return undefined;
    if (_.isString(value)) {
        return value;
    }
    throw new ArgumentError(`Invalid data type. ${name} must be a string`);
}
exports.parseString = parseString;
function parseStringArray(name, value, required = false) {
    if (_.isNil(value) && !required)
        return undefined;
    if (_.isString(value)) {
        return [value];
    }
    if (_.isArray(value)) {
        var index = 0;
        while (index < value.length) {
            var element = value[index];
            if (!_.isString(element))
                break;
            index++;
        }
        if (index === value.length) {
            return value;
        }
    }
    throw new ArgumentError(`Invalid data type. ${name} must be a string array`);
}
exports.parseStringArray = parseStringArray;
function parseNumberArray(name, value, required = false) {
    if (_.isNil(value) && !required)
        return undefined;
    if (_.isString(value)) {
        var num = Number(value);
        if (!isNaN(num))
            return [num];
    }
    if (_.isArray(value)) {
        var index = 0;
        var numArray = [];
        while (index < value.length) {
            var element = Number(value[index]);
            if (isNaN(element))
                break;
            numArray.push(element);
            index++;
        }
        if (index === value.length) {
            return value;
        }
    }
    throw new ArgumentError(`Invalid data type. ${name} must be a number array`);
}
exports.parseNumberArray = parseNumberArray;
//# sourceMappingURL=error.js.map