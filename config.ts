export let server = {
    connection: {
        host: process.env.OPENSHIFT_NODEJS_IP || process.env.VCAP_APP_HOST || "0.0.0.0",
        port: process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 1337,
        routes: {
            cors: true
        }
    }
}

export let database = {
    uri: process.env.NODE_ENV === "development" ?
        "mongodb://localhost:27017/storehub" : "mongodb://tranduytrung:sbcbE045d17@ds011168.mlab.com:11168/storehub"
}

export let cookieSecret = "r9UpwgzM3V3BLJz26nrq38AH8LRtvKSchwGnGWeQ24a4fqPWSaL2FFw4nKUyKhd5b585xV87cYZyUuRqTvRHvWAt7QCACTjHtcUAHGUveQtPSnTerpdrYMkjgkU3ty8Ty3w6u7Pp9tr76vLHT2U7ttWYUtvxUCKZYGeECdGDsqrqTdfuL8Z4NVjbz4y3HL5SXNXCXaundSwkA39YwtAA5xqbvXXTGLKb52MVDLkZUs8mauvH9czePpdKZuYJSxNk";

export let facebook = process.env.NODE_ENV === "development" ?
    {
        clientId: "593543674142570",
        clientSecret: "20c094bf1c4d63da4fdee5b0cbfb1fb3",
        scope: ["email"]
    } :
    {
        clientId: "552186041611667",
        clientSecret: "56691d3abe55e156c8364489db845bcc",
        scope: ["email"]
    }

export let jwt = {
    secretKey: "Rd9yQp8fkYPrNPmth1Wt0yN0HOAGNbbmcYApxhFPzVVF63rVijfTPBt3hhY6IQtPXStk3GYKbNfbo2gYgplgVQYuyH4GE1TCJSMrlYX1VhXdAmFcl7Vz6Kk162L5qXww67Pq1Z1NFtkJG973wHW6CRVzQsB7Pp9qQnrtUrRpFJDO4flCVWy4sDljALuzbSx0d3QpgrknU6kZXYjhG9Rnv3rLYfrubR6VBNwNHxVEmFF9pgI8yLsn7gwwF3dVeNMXIb8EWK8DVbN7q",
    algorithm: "HS256",
    expiresIn: "60d"
}

export let jsdom = {
    maxConcurrency: 4
}

export let taskExecutor = {
    maxConcurrency: 2,
    name: process.env.storehub_instance_name
}

export let onesignal = {
    appId: "3ff09ec7-cf28-487c-9141-edd25759e85f",
    appKey: "YjhiZmVhODItNTE1MS00MDA3LTlmOTYtNTRmYTY0ODJlNDg0"
}