declare module "wreck" {
    import * as http from 'http'
    import * as stream from 'stream'
    
    interface IResponseCallback {
        (err: any, response: http.IncomingMessage) : void
    }
    
    interface IResponseCallbackWithPayload {
        (err: any, response: http.IncomingMessage, payload : Buffer | Object) : void
    }
    
    interface IBeforeRedirectCallback {
        (redirectMethod : string, statusCode : number, location : string, redirectOptions : IWreckOptions)
    }
    
    interface IRedirectedCallback {
        (statusCode : number, location : string, request : http.ClientRequest)
    }
    
    interface IWreckOptions {
        baseUrl? : string,
        payload? : string | Buffer | stream.Readable,
        headers? : Object,
        json?: boolean | string,
        rejectUnauthorized? : boolean,
        redirects? : number,
        beforeRedirect? : IBeforeRedirectCallback,
        redirected? : IRedirectedCallback,
        agent? : http.Agent,
        timeout? : number,
        secureProtocol? : boolean | string
    }

    interface Wreck {
        defaults(options: IWreckOptions) : Wreck;

        request(method: string, uri: string, options? : IWreckOptions, callback?: IResponseCallback) : http.ClientRequest;

        read(response: http.IncomingMessage, options: IWreckOptions, callback: (err: any, payload: any) => void) : void;

        get(uri: string, callback: IResponseCallbackWithPayload) : http.ClientRequest;
        get(uri: string, options: IWreckOptions, callback: IResponseCallbackWithPayload) : http.ClientRequest;
        post(uri: string, callback: IResponseCallbackWithPayload) : http.ClientRequest;
        post(uri: string, options: IWreckOptions, callback: IResponseCallbackWithPayload) : http.ClientRequest;
        patch(uri: string, callback: IResponseCallbackWithPayload) : http.ClientRequest;
        patch(uri: string, options: IWreckOptions, callback: IResponseCallbackWithPayload) : http.ClientRequest;
        put(uri: string, callback: IResponseCallbackWithPayload) : http.ClientRequest;
        put(uri: string, options: IWreckOptions, callback: IResponseCallbackWithPayload) : http.ClientRequest;
        delete(uri: string, callback: IResponseCallbackWithPayload) : http.ClientRequest;
        delete(uri: string, options: IWreckOptions, callback: IResponseCallbackWithPayload) : http.ClientRequest;

        toReadableStream(payload: any, encoding?: string) : stream.Readable;

        parseCacheControl(field: string) : Object;

        agents: {
            http: http.Agent,
            https: http.Agent
        };
    }    
    
    var wreck : Wreck;
    export = wreck;
}
