declare module umbrella {
    interface IUmbrellaSelector {
        (selector : string) :  IUmbrellaNodes
        (element : Element) :  IUmbrellaNodes
        (element : HTMLElement) :  IUmbrellaNodes
        (elements : ArrayLike<HTMLElement>) :  IUmbrellaNodes
        (elements : IUmbrellaNodes) :  IUmbrellaNodes
    }
    
    interface IUmbrellaAjax {
        <T>(action : string, options : IAjaxOptions, after : ICallback<T>, before? : INonErrorCallback<XMLHttpRequest>) : XMLHttpRequest
    }
    
    interface IUmbrellaNodes {
        nodes : HTMLElement[]
        length : number
        
        addClass(...classNames : string[])
        addClass(...classNames : Array<string>[])
        addClass(...funcs : IInvoke<string>[])
        
        after(html : string) : IUmbrellaNodes
        after(htmlElement : HTMLElement) : IUmbrellaNodes
        after(htmlElement : HTMLElement[]) : IUmbrellaNodes
        after(umbrellaElement : IUmbrellaNodes) : IUmbrellaNodes
        after(func : IInvoke<string | HTMLElement | HTMLElement[] | IUmbrellaNodes>) : IUmbrellaNodes
        after<T>(func : IMapFunction<T, string | HTMLElement | HTMLElement[] | IUmbrellaNodes>, items : T[]) : IUmbrellaNodes
        
        ajax<T>(after : ICallback<T>, before? : INonErrorCallback<XMLHttpRequest>)
        
        append(html : string) : IUmbrellaNodes
        append(htmlElement : HTMLElement) : IUmbrellaNodes
        append(htmlElement : HTMLElement[]) : IUmbrellaNodes
        append(umbrellaElement : IUmbrellaNodes) : IUmbrellaNodes
        append(func : IInvoke<string | HTMLElement | HTMLElement[] | IUmbrellaNodes>) : IUmbrellaNodes
        append<T>(func : IMapFunction<T, string | HTMLElement | HTMLElement[] | IUmbrellaNodes>, items : T[]) : IUmbrellaNodes
        
        array() : string[]
        array(callback : INonErrorCallback<string>)
        
        attr(name : string) : string
        attr(name : string, value : string | number) : IUmbrellaNodes
        attr(map : IAttributeMap) : IUmbrellaNodes
        
        before(html : string) : IUmbrellaNodes
        before(htmlElement : HTMLElement) : IUmbrellaNodes
        before(htmlElement : HTMLElement[]) : IUmbrellaNodes
        before(umbrellaElement : IUmbrellaNodes) : IUmbrellaNodes
        before(func : IInvoke<string | HTMLElement | HTMLElement[] | IUmbrellaNodes>) : IUmbrellaNodes
        before<T>(func : IMapFunction<T, string | HTMLElement | HTMLElement[] | IUmbrellaNodes>, items : T[]) : IUmbrellaNodes
        
        children() : IUmbrellaNodes
        children(filter : string) : IUmbrellaNodes
        children(filter : IUmbrellaNodes) : IUmbrellaNodes
        children(filter : IFilter<HTMLElement>) : IUmbrellaNodes
        
        closest(filter : string) : IUmbrellaNodes
        closest(filter : IUmbrellaNodes) : IUmbrellaNodes
        closest(filter : IFilter<HTMLElement>) : IUmbrellaNodes
        
        data(name : string) : string
        data(name : string, value : string | number) : IUmbrellaNodes
        data(map : IAttributeMap) : IUmbrellaNodes
        
        each(callback : IIterate<HTMLElement>) : IUmbrellaNodes
        
        filter(filter : string) : IUmbrellaNodes
        filter(filter : IUmbrellaNodes) : IUmbrellaNodes
        filter(filter : IFilter<HTMLElement>) : IUmbrellaNodes
        
        find(filter : string) : IUmbrellaNodes
        find(filter : IUmbrellaNodes) : IUmbrellaNodes
        find(filter : IFilter<HTMLElement>) : IUmbrellaNodes
        
        first() : HTMLElement
        
        handle(eventNames : string, listener : IEventListener)
        handle(eventNames : string[], listener : IEventListener)
        
        hasClass(...classNames : string[])
        hasClass(...classNames : Array<string>[])
        hasClass(...funcs : IInvoke<string>[])
        
        html() : string
        html(html : string) : IUmbrellaNodes
        
        is(selector : string) : boolean
        is(selector : IUmbrellaNodes) : boolean
        is(selector : IFilter<HTMLElement>) : boolean
        
        last() : HTMLElement
        
        not(selector : string) : IUmbrellaNodes
        not(selector : IUmbrellaNodes) : IUmbrellaNodes
        not(selector : IFilter<HTMLElement>) : IUmbrellaNodes
        
        off(eventNames : string)
        off(eventNames : string[])

        on(eventNames : string, listener : IEventListener)
        on(eventNames : string[], listener : IEventListener)
        
        parent() : IUmbrellaNodes
        parent(selector : string) : IUmbrellaNodes
        parent(selector : IUmbrellaNodes) : IUmbrellaNodes
        parent(selector : IFilter<HTMLElement>) : IUmbrellaNodes
        
        remove() : IUmbrellaNodes
        
        removeClass(...classNames : string[])
        removeClass(...classNames : Array<string>[])
        removeClass(...funcs : IInvoke<string>[])
        
        replace(html : string)
        replace(func : IInvoke<string>)
        
        scroll() : IUmbrellaNodes
        
        serialize() : string
        
        siblings() : IUmbrellaNodes
        siblings(selector : string) : IUmbrellaNodes
        siblings(selector : IUmbrellaNodes) : IUmbrellaNodes
        siblings(selector : IFilter<HTMLElement>) : IUmbrellaNodes
        
        size() : ISizeObject
        
        text() : string
        text(str : string) : IUmbrellaNodes
        
        toggleClass(classNames : string, forceAddOrRemove? : boolean)
        toggleClass(classNames : string[], forceAddOrRemove? : boolean)
        
        trigger(eventNames : string, ...args : any[])
        trigger(eventNames : string[], ...args : any[])
        
        wrap(html : string)
    }
    
    interface INonErrorCallback<T> {
        (value : T) : void
    }
    
    interface ICallback<T> {
        (error : any, value : T) : void
    }
    
    interface IAttributeMap {
        [key : string] : string | number
    }
    
    interface IMapFunction<T, R> {
        (value : T, index : number) : R 
    }
    
    interface IFilter<T> extends IMapFunction<T, boolean> { }
    interface IIterate<T> extends IMapFunction<T, void> { }
    
    interface IInvoke<T> {
        () : T
    }
    
    interface IAjaxOptions {
        method? : string
        body? : string | number | Object
        headers? : Object
    }
    
    interface ISizeObject {
        left : number
        right : number
        top : number
        bottom : number
        width : number
        height : number
    }
    
    interface IEventArg {
        currentTarget : HTMLElement
        preventDefault : () => void
        details : any[]
    }
    
    interface IEventListener {
        <T>(e : IEventArg, arg : T)
        <T1, T2>(e : IEventArg, arg1 : T1, arg2 : T2)
        <T1, T2, T3>(e : IEventArg, arg1 : T1, arg2 : T2, arg3 : T3)
        (e : IEventArg, ...args : any[])
    }
}

declare interface IUmbrellaSelector extends umbrella.IUmbrellaSelector {}
declare interface IUmbrellaAjax extends umbrella.IUmbrellaAjax {}