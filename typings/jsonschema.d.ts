
declare module "jsonschema" {
    interface IValidationOptions {
        throwError? : boolean
    }
    
    interface IValidatationResult {
        valid : boolean
        toString() : string
    }
    
    /**
     * Validator
     */
    class Validator {
        addSchema(schema : {}, uri?: string)
        validate(data: any, schema : {}, options? : IValidationOptions) : IValidatationResult
    }
}