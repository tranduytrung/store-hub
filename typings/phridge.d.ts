
declare module "phridge" {
    import * as child_process from "child_process";
    
    /**Spawns a new PhantomJS process with the given config. Read the PhantomJS documentation for all available config options. 
     * Use camelCase style for option names. The promise will be fulfilled with an instance of Phantom. */
    export function spawn(config? : {}) : PromiseLike<Phantom>;
    /**Terminates all PhantomJS processes that have been spawned. The promise will be fulfilled when all child processes emitted an exit-event. */
    export function disposeAll() :  PromiseLike<void>;
    
    /** phridge extends the ChildProcess-instance by a new stream called cleanStdout. 
     * This stream is piped to process.stdout by default. It provides all data not dedicated to phridge. 
     * Streaming data is considered to be dedicated to phridge when the new line is preceded by the classifier string "message to node: ". */
    export interface IPhantomProcess extends child_process.ChildProcess {
        cleanStdout : NodeJS.ReadableStream
    }
    
    export interface IDispatchingFunction0<R> {
        (resolve: (result : R) => void, reject: (err: Error) => void) : void;
    }
    
    export interface IDispatchingFunction1<R, A0> {
        (arg0: A0, resolve: (result : R) => void, reject: (err: Error) => void) : void;
    }
    
    export interface IDispatchingFunction2<R, A0, A1> {
        (arg0: A0, arg1: A1, resolve: (result : R) => void, reject: (err: Error) => void) : void;
    }
    
    export interface IDispatchingFunction3<R, A0, A1, A2> {
        (arg0: A0, arg1: A1, arg2: A2, resolve: (result : R) => void, reject: (err: Error) => void) : void;
    }
    
    export interface IDispatchingFunction4<R, A0, A1, A2, A3> {
        (arg0: A0, arg1: A1, arg2: A2, arg3: A3, resolve: (result : R) => void, reject: (err: Error) => void) : void;
    }
    
    export interface IDispatchingFunction5<R, A0, A1, A2, A3, A4> {
        (arg0: A0, arg1: A1, arg2: A2, arg3: A3, arg4 : A4, resolve: (result : R) => void, reject: (err: Error) => void) : void;
    }
    
    /**Stringifies fn, sends it to PhantomJS and executes it there again. args... are stringified using JSON.stringify() and passed to fn again. fn may simply return a result or throw an error or call resolve() or reject() respectively if it is asynchronous. phridge compares fn.length with the given number of arguments to determine whether fn is sync or async. The returned promise will be resolved with the result or rejected with the error. */
    export interface IRun {
        <R>(func: IDispatchingFunction0<R>) : PromiseLike<R>;
        <R, A0>(arg0: A0, func: IDispatchingFunction1<R, A0>) : PromiseLike<R>;
        <R, A0, A1>(arg0: A0, arg1: A1, func: IDispatchingFunction2<R, A0, A1>) : PromiseLike<R>;
        <R, A0, A1, A2>(arg0: A0, arg1: A1, arg2: A2, func: IDispatchingFunction3<R, A0, A1, A2>) : PromiseLike<R>;
        <R, A0, A1, A2, A3>(arg0: A0, arg1: A1, arg2: A2, arg3: A3, func: IDispatchingFunction4<R, A0, A1, A2, A3>) : PromiseLike<R>;
        <R, A0, A1, A2, A3, A4>(arg0: A0, arg1: A1, arg2: A2, arg3: A3, arg4 : A4, func: IDispatchingFunction5<R, A0, A1, A2, A3, A4>) : PromiseLike<R>;
        <R>(...args: any[]) : PromiseLike<R>;
    }

    
    export class Phantom {
        /**A reference to the ChildProcess-instance. */
        childProcess : IPhantomProcess;
        run : IRun;
        createPage() : Page;
        dispose() : PromiseLike<void>;
    }
    
    export class Page {
        phantom: Phantom;
        run : IRun;
        dispose() : PromiseLike<void>;
    }
}