"use strict";

import * as fs from "fs";
import * as path from "path";
import { Server, Request, IReply, IRouteConfiguration } from "hapi";
import * as PromiseUtility from "./promise";
import * as _ from "lodash";

let readdirAsync = PromiseUtility.promisify(fs.readdir);

let mainModule = <NodeModule>require["main"];
let appDir = path.dirname(mainModule.filename);

export enum BootstrapMode {
    Web = 1,
    Administration = 2,
    Full = 3
}

export module Bootstrap {
    export let apisPath = "./apis";
    export let methodsPath = "./methods";
    export let registerAPIs = async (server: Server, mode?: BootstrapMode) => {
        mode = mode || BootstrapMode.Full;
        var items = await readdirAsync(apisPath);
        items = items.map((item) => {
            return path.resolve(appDir, apisPath, item);
        });

        var jsFiles = items.filter((item) => fs.statSync(item).isFile()
            && path.extname(item) === ".js");

        jsFiles.forEach((file) => {
            var entries: IRouteConfiguration[] = require(file);
            
            var predicate = _.bind(_.isEqual, null, "administration");
            var selectedEntries = mode === BootstrapMode.Full ? entries :
                _.filter(entries, function(entry) {
                    var entryTags = _.get<string[]>(entry, "config.tags") || [];
                    return mode === BootstrapMode.Administration? 
                        _.some(entryTags, predicate) : 
                        !_.some(entryTags, predicate);
                });

            server.route(selectedEntries);
        });
    }

    export let registerMethods = async (server: Server) => {
        var items = await readdirAsync(methodsPath);
        items = items.map((item) => {
            return path.resolve(appDir, methodsPath, item);
        });

        var jsFiles = items.filter((item) => fs.statSync(item).isFile()
            && path.extname(item) === ".js");

        jsFiles.forEach((file) => {
            server.method(require(file));
        });
    }
}

export default Bootstrap;