"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const fs = require("fs");
const path = require("path");
const PromiseUtility = require("./promise");
const _ = require("lodash");
let readdirAsync = PromiseUtility.promisify(fs.readdir);
let mainModule = require["main"];
let appDir = path.dirname(mainModule.filename);
(function (BootstrapMode) {
    BootstrapMode[BootstrapMode["Web"] = 1] = "Web";
    BootstrapMode[BootstrapMode["Administration"] = 2] = "Administration";
    BootstrapMode[BootstrapMode["Full"] = 3] = "Full";
})(exports.BootstrapMode || (exports.BootstrapMode = {}));
var BootstrapMode = exports.BootstrapMode;
var Bootstrap;
(function (Bootstrap) {
    Bootstrap.apisPath = "./apis";
    Bootstrap.methodsPath = "./methods";
    Bootstrap.registerAPIs = (server, mode) => __awaiter(this, void 0, void 0, function* () {
        mode = mode || BootstrapMode.Full;
        var items = yield readdirAsync(Bootstrap.apisPath);
        items = items.map((item) => {
            return path.resolve(appDir, Bootstrap.apisPath, item);
        });
        var jsFiles = items.filter((item) => fs.statSync(item).isFile()
            && path.extname(item) === ".js");
        jsFiles.forEach((file) => {
            var entries = require(file);
            var predicate = _.bind(_.isEqual, null, "administration");
            var selectedEntries = mode === BootstrapMode.Full ? entries :
                _.filter(entries, function (entry) {
                    var entryTags = _.get(entry, "config.tags") || [];
                    return mode === BootstrapMode.Administration ?
                        _.some(entryTags, predicate) :
                        !_.some(entryTags, predicate);
                });
            server.route(selectedEntries);
        });
    });
    Bootstrap.registerMethods = (server) => __awaiter(this, void 0, void 0, function* () {
        var items = yield readdirAsync(Bootstrap.methodsPath);
        items = items.map((item) => {
            return path.resolve(appDir, Bootstrap.methodsPath, item);
        });
        var jsFiles = items.filter((item) => fs.statSync(item).isFile()
            && path.extname(item) === ".js");
        jsFiles.forEach((file) => {
            server.method(require(file));
        });
    });
})(Bootstrap = exports.Bootstrap || (exports.Bootstrap = {}));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Bootstrap;
//# sourceMappingURL=bootstrap.js.map