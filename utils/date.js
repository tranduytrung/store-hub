"use strict";
function dateWithoutTime(date = new Date()) {
    var d = new Date(date.getTime());
    d.setHours(0, 0, 0, 0);
    return d;
}
exports.dateWithoutTime = dateWithoutTime;
function diffInMiliseconds(lhs, rhs = new Date()) {
    return dateWithoutTime(lhs).getTime() - dateWithoutTime(rhs).getTime();
}
exports.diffInMiliseconds = diffInMiliseconds;
function getRelativeDateByMiliseconds(miliseconds, base) {
    var date = base ? new Date(base.getTime()) : new Date();
    date.setTime(date.getTime() + miliseconds);
    return date;
}
exports.getRelativeDateByMiliseconds = getRelativeDateByMiliseconds;
//# sourceMappingURL=date.js.map