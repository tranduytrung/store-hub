
export function getArgs(): { [key: string]: any } {
    var argv = process.argv;
    var args = {};
    for (var index = 2; index < argv.length; index++) {
        var arg = argv[index];
        var parts = arg.split("=");
        if (parts.length > 2) {
            throw new Error(`invalid argument ${arg}`);
        }

        var key = parts[0];
        var value = parts[1];

        if (!value) {
            args[key] = true;
        } else {
            var num = Number(value);
            if (isNaN(num)) {
                args[key] = value;
            } else {
                args[key] = num;
            }
        }
    }

    return args;
}