
export function dateWithoutTime(date: Date = new Date()) {
    var d = new Date(date.getTime());
    d.setHours(0, 0, 0, 0);
    return d;
}

export function diffInMiliseconds(lhs: Date, rhs: Date = new Date()) {
    return dateWithoutTime(lhs).getTime() - dateWithoutTime(rhs).getTime();
}

export function getRelativeDateByMiliseconds(miliseconds: number, base?: Date) {
    var date: Date = base ? new Date(base.getTime()) : new Date();
    date.setTime(date.getTime() + miliseconds);
    return date;
}