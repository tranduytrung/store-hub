"use strict";
const _ = require("lodash");
function generateMethodKey() {
    let key = '';
    for (let i = 0; i < arguments.length; ++i) {
        const arg = arguments[i];
        var com;
        if (_.isNil(arg)) {
            com = "?";
        }
        else if (_.isArray(arg)) {
            com = encodeURIComponent(arg.join(","));
        }
        else if (typeof arg === 'string' ||
            typeof arg === 'number' ||
            typeof arg === 'boolean') {
            com = encodeURIComponent(arg.toString());
        }
        else {
            return null;
        }
        key = key + (i ? ':' : '') + com;
    }
    return key;
}
exports.generateMethodKey = generateMethodKey;
;
//# sourceMappingURL=generator.js.map