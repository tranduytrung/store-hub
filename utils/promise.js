"use strict";
const _ = require("lodash");
function promisify(func, context) {
    var thisArg = context || this;
    return function (...args) {
        var promise = new Promise((resolve, reject) => {
            // callback arg
            args.push((err, result) => {
                if (err) {
                    return reject(err);
                }
                resolve(result);
            });
            func.apply(thisArg, args);
        });
        return promise;
    };
}
exports.promisify = promisify;
function wait(miliseconds) {
    return new Promise(function (resolve) {
        setTimeout(resolve, miliseconds);
    });
}
exports.wait = wait;
function parallelEach(items, func, options) {
    options = _.defaults(options, {
        limit: items.length,
        ignoreError: false
    });
    return new Promise(function (resolve, reject) {
        var invoked = 0;
        var finished = 0;
        var results = [];
        var isError = false;
        if (items.length <= 0) {
            return resolve([]);
        }
        var seedNum = options.limit < items.length ? options.limit : items.length;
        while (invoked < seedNum) {
            invoked++;
            func(items[invoked - 1])
                .then(complete, error)
                .then(next);
            ;
        }
        function next() {
            finished++;
            if (finished >= items.length && !isError) {
                return resolve(results);
            }
            if (invoked >= items.length)
                return;
            invoked++;
            func(items[invoked - 1])
                .then(complete, error)
                .then(next);
        }
        function complete(result) {
            results.push(result);
        }
        function error(exception) {
            if (options.ignoreError)
                return;
            if (isError)
                return;
            isError = true;
            reject(exception);
        }
    });
}
exports.parallelEach = parallelEach;
class Semaphore {
    constructor(entries) {
        this.entries = entries;
    }
    lock(func, options) {
        options = _.defaults(options, {
            sleepTime: 100
        });
        return new Promise((resolve, reject) => {
            var tryToFire = () => {
                if (this.entries <= 0) {
                    return setTimeout(tryToFire, options.sleepTime);
                }
                this.entries--;
                func().then(result => {
                    this.entries++;
                    resolve(result);
                }, error => {
                    this.entries++;
                    reject(error);
                });
            };
            tryToFire();
        });
    }
}
exports.Semaphore = Semaphore;
/**
 * Task
 */
class ReportivePromise {
    constructor(arg) {
        this.isCompleted = false;
        if (_.isFunction(arg)) {
            var innerExecutor = (fulfill, error) => {
                arg(fulfill, error, (status) => {
                    this.status = status;
                });
            };
            this._promise = new Promise(innerExecutor);
        }
        else {
            this._promise = arg;
        }
        this._promise.then(() => {
            this.isCompleted = true;
        }, err => {
            this.error = err;
            this.isCompleted = true;
        });
    }
    then(onfulfilled, onrejected) {
        return this._promise.then(onfulfilled, onrejected);
    }
}
exports.ReportivePromise = ReportivePromise;
//# sourceMappingURL=promise.js.map