import * as _ from "lodash"

export interface ICallback<R> {
    (err: Error | any, result?: R): any
}

export interface IAsync0<R> {
    (): PromiseLike<R>;
}

export interface IAsync1<R, A0> {
    (arg0: A0): PromiseLike<R>;
}

export interface IAsync2<R, A0, A1> {
    (arg0: A0, arg1: A1): PromiseLike<R>;
}

export interface IAsync3<R, A0, A1, A2> {
    (arg0: A0, arg1: A1, arg2: A2): PromiseLike<R>;
}

export interface IAsync4<R, A0, A1, A2, A3> {
    (arg0: A0, arg1: A1, arg2: A2, arg3: A3): PromiseLike<R>;
}

export interface IAsyncN<R> {
    (...args: any[]): PromiseLike<R>;
}

export function promisify<R>(func: (callback: (ICallback<R>)) => any, context?: any): IAsync0<R>
export function promisify<R, A0>(func: (arg0: A0, callback: ICallback<R>) => any, context?: any): IAsync1<R, A0>;
export function promisify<R, A0, A1>(func: (arg0: A0, arg1: A1, callback: ICallback<R>) => any, context?: any): IAsync2<R, A0, A1>;
export function promisify<R, A0, A1, A2>(func: (arg0: A0, arg1: A1, arg2: A2, callback: ICallback<R>) => any, context?: any): IAsync3<R, A0, A1, A2>;
export function promisify<R>(func: (...args: any[]) => any, context?: any): IAsyncN<R> {
    var thisArg = context || this;

    return function (...args: any[]): PromiseLike<R> {
        var promise = new Promise<R>((resolve, reject) => {
            // callback arg
            args.push((err: Error, result: R) => {
                if (err) {
                   return reject(err);
                }

                resolve(result);
            });

            func.apply(thisArg, args);
        });

        return promise;
    }
}

export function wait(miliseconds: number): Promise<void> {
    return new Promise<void>(function (resolve) {
        setTimeout(resolve, miliseconds);
    });
}

interface IParallelFunction<A, R> {
    (item: A): Promise<R>
}

interface IParallelOptions {
    limit?: number
    ignoreError?: boolean
}

export function parallelEach<A, R>(items: ArrayLike<A>, func: IParallelFunction<A, R>, options?: IParallelOptions): Promise<R[]> {
    options = _.defaults(options, {
        limit: items.length,
        ignoreError: false
    });

    return new Promise<R[]>(function (resolve, reject) {
        var invoked = 0;
        var finished = 0;
        var results: R[] = [];
        var isError: boolean = false;

        if (items.length <= 0) {
            return resolve([]);
        }

        var seedNum = options.limit < items.length ? options.limit : items.length;
        while (invoked < seedNum) {
            invoked++;
            func(items[invoked - 1])
                .then(complete, error)
                .then(next);;
        }

        function next() {
            finished++;
            if (finished >= items.length && !isError) {
                return resolve(results);
            }

            if (invoked >= items.length) return;

            invoked++;
            func(items[invoked - 1])
                .then(complete, error)
                .then(next);
        }

        function complete(result: R) {
            results.push(result);
        }

        function error(exception: any) {
            if (options.ignoreError) return;
            if (isError) return;

            isError = true;
            reject(exception);
        }
    });
}

interface ISemaphoreFunction<R> {
    (): Promise<R>
}

interface ISemaphoreOptions {
    sleepTime?: number
}

export class Semaphore {
    entries: number

    constructor(entries: number) {
        this.entries = entries;
    }

    lock<R>(func: ISemaphoreFunction<R>, options?: ISemaphoreOptions): Promise<R> {
        options = _.defaults(options, {
            sleepTime: 100
        });

        return new Promise<R>((resolve, reject) => {
            var tryToFire = () => {
                if (this.entries <= 0) {
                    return setTimeout(tryToFire, options.sleepTime);
                }

                this.entries--;
                func().then(result => {
                    this.entries++;
                    resolve(result);
                }, error => {
                    this.entries++;
                    reject(error);
                });
            }

            tryToFire();
        });
    }
}

interface ITaskExecutor<T> {
    (resolve: (value?: T | PromiseLike<T>) => void, reject: (reason?: any) => void, report: (status: string) => void)
}

/**
 * Task
 */
export class ReportivePromise<T> implements PromiseLike<T> {
    private _promise: PromiseLike<T>
    status: string
    error: any
    isCompleted: boolean = false

    constructor(promise: PromiseLike<T>)
    constructor(executor: ITaskExecutor<T>)
    constructor(arg: PromiseLike<T> | ITaskExecutor<T>) {
        if (_.isFunction(arg)) {
            var innerExecutor = (fulfill: (value?: T | PromiseLike<T>) => void, error: (reason?: any) => void) => {
                arg(fulfill, error, (status) => {
                    this.status = status;
                });
            }

            this._promise = new Promise(innerExecutor);
        } else {
            this._promise = arg;
        }

        this._promise.then(() => {
            this.isCompleted = true;
        }, err => {
            this.error = err;
            this.isCompleted = true;
        });
    }

    then<TResult>(
        onfulfilled?: (value: T) => TResult | PromiseLike<TResult>,
        onrejected?: (reason: any) => TResult | PromiseLike<TResult>): PromiseLike<TResult> {
        return this._promise.then(onfulfilled, onrejected);
    }
}