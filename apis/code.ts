import * as Hapi from "hapi"
import * as _ from "lodash"
import {handleError} from "../error"

var codes: string[] = []

var routes: Hapi.IRouteConfiguration[] = [
    {
        path: "/code/redeem",
        method: "GET",
        config: {
            auth: false
        },
        handler: function (request, reply) {
            (async () => {
                if (codes.length > 0) {
                    var code = codes.shift();
                    reply.redirect(`http://go.microsoft.com/fwlink/?LinkId=532540&mstoken=${code}`);
                } else {
                    reply(null, "all codes have been used");
                }
            })().catch(error => {
                handleError(reply, error);
            });
        }
    },
    {
        path: "/code",
        config: {
            auth: false
        },
        method: "GET",
        handler: function (request, reply) {
            (async () => {
                reply.view("code", { codes: codes });
            })().catch(error => {
                handleError(reply, error);
            });
        }
    },
    {
        path: "/code",
        config: {
            auth: false
        },
        method: "POST",
        handler: function (request, reply) {
            (async () => {
                var str: string = request.payload.codes;
                codes = _.filter(str.split("\r\n"), _.negate(_.isEmpty));
                reply.redirect("/code");
            })().catch(error => {
                handleError(reply, error);
            });
        }
    }
];

export = routes;
