"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const error_1 = require("../error");
const os_1 = require("os");
var routes = [
    {
        path: "/",
        method: "GET",
        config: {
            auth: {
                mode: "try",
                strategy: "jwt"
            }
        },
        handler: function (request, reply) {
            (() => __awaiter(this, void 0, void 0, function* () {
                reply.view("home");
            }))().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    },
    {
        path: "/memstat",
        config: {
            auth: false
        },
        method: "GET",
        handler: function (request, reply) {
            (() => __awaiter(this, void 0, void 0, function* () {
                var info = {
                    usage: 1 - os_1.freemem() / os_1.totalmem(),
                    freemem: os_1.freemem(),
                    totalmem: os_1.totalmem(),
                    rss: process.memoryUsage().rss
                };
                reply(null, info);
            }))().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    }
];
module.exports = routes;
//# sourceMappingURL=home.js.map