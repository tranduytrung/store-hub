"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const _ = require("lodash");
const mongodb_1 = require("mongodb");
const task_1 = require("../models/task");
const tasks_1 = require("../services/db/tasks");
const error_1 = require("../error");
var routes = [
    {
        path: "/task/view",
        method: "GET",
        config: {
            auth: false,
            tags: ["administration"]
        },
        handler: function (request, reply) {
            (function () {
                return __awaiter(this, void 0, void 0, function* () {
                    var tasks = yield tasks_1.getEssentialTasks();
                    var context = {
                        tasks: tasks,
                        TaskState: task_1.TaskState
                    };
                    reply.view("task", context);
                });
            })().catch((err) => {
                error_1.handleError(reply, err);
            });
        }
    },
    {
        path: "/task/setState",
        method: "POST",
        config: {
            auth: false,
            tags: ["administration"]
        },
        handler: function (request, reply) {
            (function () {
                return __awaiter(this, void 0, void 0, function* () {
                    var taskId = request.payload.id;
                    if (!_.isString(taskId)) {
                        throw new error_1.ArgumentError('id must be a string');
                    }
                    var state = Number(request.payload.state);
                    if (isNaN(state)) {
                        throw new error_1.ArgumentError("state must be a number");
                    }
                    yield tasks_1.upsert({
                        _id: new mongodb_1.ObjectID(taskId),
                        executor: undefined,
                        params: undefined,
                        nextRun: state === task_1.TaskState.scheduled ? new Date() : undefined,
                        state: state,
                        assignee: undefined
                    });
                    reply.redirect("/task/view");
                });
            })().catch((err) => {
                error_1.handleError(reply, err);
            });
        }
    }
];
module.exports = routes;
//# sourceMappingURL=task.js.map