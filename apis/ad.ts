import {IAd} from "../models/ad"
import * as Hapi from "hapi"
import * as _ from "lodash"
import {promisify} from "../utils/promise"
import {ArgumentError, handleError, NotFoundError} from "../error"

var routes: Hapi.IRouteConfiguration[] = [
    {
        path: "/ad/getSourceEnumeration",
        config: {
            auth: false
        },
        method: "GET",
        handler: function (request, reply) {
            (async function () {
                var getSourceEnumeration: any = request.server.methods["ad"]["getSourceEnumeration"];
                var getSourceEnumerationAsync = promisify<string[]>(getSourceEnumeration);

                let sources = await getSourceEnumerationAsync();
                reply(null, sources);
            })().catch(error => {
                handleError(reply, error);
            });
        }
    },
    {
        path: "/ad/bySource",
        config: {
            auth: false
        },
        method: "GET",
        handler: function (request, reply) {
            (async function () {
                var getBySource: any = request.server.methods["ad"]["getBySource"];
                var getBySourceAsync = promisify<IAd[], string>(getBySource);

                let source = request.query.source;
                let ads = await getBySourceAsync(source? source : "");
                reply(null, ads);
            })().catch(error => {
                handleError(reply, error);
            });
        }
    }
];

export = routes;