"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const _ = require("lodash");
const JWT = require("jsonwebtoken");
const URI = require("urijs");
const Users = require("../services/db/users");
const config_1 = require("../config");
const error_1 = require("../error");
var routes = [
    {
        path: "/auth/facebook",
        config: {
            auth: {
                mode: "try",
                strategy: "facebook"
            }
        },
        method: "GET",
        handler: function (request, reply) {
            (() => __awaiter(this, void 0, void 0, function* () {
                var redirect = _.get(request.state, "bell-facebook.query.redirect");
                if (!redirect) {
                    throw new error_1.ArgumentError("no redirect in query");
                }
                var redirectUri = URI(redirect);
                if (!request.auth.isAuthenticated) {
                    redirectUri.addQuery({
                        error: "Authentication failed: " + request.auth.error.message
                    });
                    return reply.redirect(redirectUri.valueOf());
                }
                var credentials = request.auth.credentials;
                var user = _.omit(yield Users.insert({
                    email: credentials.profile.email,
                    facebookId: credentials.profile.id,
                    firstName: credentials.profile.name.first,
                    lastName: credentials.profile.name.last,
                    onesignalIds: []
                }), "wishlist", "onesignalIds");
                var token = yield signCredentials(user);
                redirectUri.addQuery({
                    token: token
                });
                reply.redirect(redirectUri.valueOf());
            }))().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    },
    {
        path: "/auth/renew",
        method: "POST",
        handler: function (request, reply) {
            (() => __awaiter(this, void 0, void 0, function* () {
                var token = yield signCredentials(request.auth.credentials);
                reply(null, token);
            }))().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    },
    {
        path: "/auth/status",
        method: "GET",
        config: {
            auth: {
                mode: "try",
                strategy: "jwt"
            }
        },
        handler: function (request, reply) {
            (() => __awaiter(this, void 0, void 0, function* () {
                if (!request.auth.isAuthenticated) {
                    throw new error_1.ArgumentError("token is not valid");
                }
                reply(null, request.auth.credentials["exp"] * 1000);
            }))().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    }
];
function signCredentials(credentials) {
    return new Promise(function (resolve) {
        JWT.sign(credentials, config_1.jwt.secretKey, {
            algorithm: config_1.jwt.algorithm,
            expiresIn: config_1.jwt.expiresIn
        }, function (token) {
            resolve(token);
        });
    });
}
module.exports = routes;
//# sourceMappingURL=auth.js.map