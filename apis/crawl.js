"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const Crawler = require("../services/crawler");
const error_1 = require("../error");
var routes = [
    {
        path: "/thegioididong/getProduct",
        method: "GET",
        config: {
            auth: false,
            tags: ["administration"]
        },
        handler: function (request, reply) {
            (function () {
                return __awaiter(this, void 0, void 0, function* () {
                    var url = request.query.url;
                    if (!url) {
                        throw new error_1.ArgumentError('url is not provided');
                    }
                    var product = yield Crawler.getProductFromThegioididong(url);
                    reply(null, product);
                });
            })().catch((err) => {
                error_1.handleError(reply, err);
            });
        }
    },
    {
        path: "/fptshop/getProduct",
        method: "GET",
        config: {
            auth: false,
            tags: ["administration"]
        },
        handler: function (request, reply) {
            (function () {
                return __awaiter(this, void 0, void 0, function* () {
                    var url = request.query.url;
                    if (!url) {
                        throw new error_1.ArgumentError('url is not provided');
                    }
                    var product = yield Crawler.getProductFromFptshop(url);
                    reply(null, product);
                });
            })().catch((err) => {
                error_1.handleError(reply, err);
            });
        }
    },
    {
        path: "/tiki/getProduct",
        method: "GET",
        config: {
            auth: false,
            tags: ["administration"]
        },
        handler: function (request, reply) {
            (function () {
                return __awaiter(this, void 0, void 0, function* () {
                    var url = request.query.url;
                    if (!url) {
                        throw new error_1.ArgumentError('url is not provided');
                    }
                    var product = yield Crawler.getProductFromTiki(url);
                    reply(null, product);
                });
            })().catch((err) => {
                error_1.handleError(reply, err);
            });
        }
    },
    {
        path: "/lazada/getProduct",
        method: "GET",
        config: {
            auth: false,
            tags: ["administration"]
        },
        handler: function (request, reply) {
            (function () {
                return __awaiter(this, void 0, void 0, function* () {
                    var url = request.query.url;
                    if (!url) {
                        throw new error_1.ArgumentError('url is not provided');
                    }
                    var product = yield Crawler.getProductFromLazada(url);
                    reply(null, product);
                });
            })().catch((err) => {
                error_1.handleError(reply, err);
            });
        }
    },
    {
        path: "/thegioididong/getAds",
        method: "GET",
        config: {
            auth: false,
            tags: ["administration"]
        },
        handler: function (request, reply) {
            (function () {
                return __awaiter(this, void 0, void 0, function* () {
                    var ads = yield Crawler.getAdsFromThegioididong();
                    reply(null, ads);
                });
            })().catch((err) => {
                error_1.handleError(reply, err);
            });
        }
    },
    {
        path: "/fptshop/getAds",
        method: "GET",
        config: {
            auth: false,
            tags: ["administration"]
        },
        handler: function (request, reply) {
            (function () {
                return __awaiter(this, void 0, void 0, function* () {
                    var ads = yield Crawler.getAdsFromFptshop();
                    reply(null, ads);
                });
            })().catch((err) => {
                error_1.handleError(reply, err);
            });
        }
    },
    {
        path: "/tiki/getAds",
        method: "GET",
        config: {
            auth: false,
            tags: ["administration"]
        },
        handler: function (request, reply) {
            (function () {
                return __awaiter(this, void 0, void 0, function* () {
                    var ads = yield Crawler.getAdsFromTiki();
                    reply(null, ads);
                });
            })().catch((err) => {
                error_1.handleError(reply, err);
            });
        }
    }
];
module.exports = routes;
//# sourceMappingURL=crawl.js.map