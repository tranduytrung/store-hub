"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const _ = require("lodash");
const Users = require("../services/db/users");
const promise_1 = require("../utils/promise");
const error_1 = require("../error");
var routes = [
    {
        path: "/user",
        method: "GET",
        handler: function (request, reply) {
            (() => __awaiter(this, void 0, void 0, function* () {
                var id = request.auth.credentials._id;
                var user = yield Users.getById(id);
                if (_.isEmpty(user)) {
                    throw new error_1.NotFoundError("no such user");
                }
                reply(null, user);
            }))().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    },
    {
        path: "/product/count/byProductId",
        config: {
            auth: false
        },
        method: "GET",
        handler: function (request, reply) {
            var countByProductId = request.server.methods["users"]["countByProductId"];
            var countByProductIdAsync = promise_1.promisify(countByProductId);
            (function () {
                return __awaiter(this, void 0, void 0, function* () {
                    let id = request.query.id;
                    if (!_.isString(id)) {
                        throw new error_1.ArgumentError("id must be a string");
                    }
                    let count = yield countByProductIdAsync(id);
                    reply(null, count);
                });
            })().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    },
    {
        path: "/user/addWishItem",
        method: "POST",
        handler: function (request, reply) {
            (() => __awaiter(this, void 0, void 0, function* () {
                var id = request.auth.credentials._id;
                var wishItem = request.payload["wishItem"];
                yield Users.addWishItem(id, wishItem);
                reply(null, null);
            }))().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    },
    {
        path: "/user/updateWishItem",
        method: "POST",
        handler: function (request, reply) {
            (() => __awaiter(this, void 0, void 0, function* () {
                var id = request.auth.credentials._id;
                var wishItem = request.payload["wishItem"];
                if (!_.isEmpty(wishItem["nextNotificationDate"])) {
                    wishItem["nextNotificationDate"] = new Date(wishItem["nextNotificationDate"]);
                }
                yield Users.updateWishItem(id, wishItem);
                reply(null, null);
            }))().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    },
    {
        path: "/user/removeWishItem",
        method: "POST",
        handler: function (request, reply) {
            (() => __awaiter(this, void 0, void 0, function* () {
                var id = request.auth.credentials._id;
                var productId = request.payload["productId"];
                yield Users.removeWishItem(id, productId);
                reply(null, null);
            }))().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    },
    {
        path: "/user/addOnesignal",
        method: "POST",
        handler: function (request, reply) {
            (() => __awaiter(this, void 0, void 0, function* () {
                var id = request.auth.credentials._id;
                var signaloneId = request.payload["onesignalId"];
                yield Users.addOnesignal(id, signaloneId);
                reply(null, null);
            }))().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    }
];
module.exports = routes;
//# sourceMappingURL=user.js.map