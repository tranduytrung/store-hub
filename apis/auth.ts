import * as _ from "lodash";
import * as Hapi from "hapi";
import * as JWT from "jsonwebtoken";
import * as URI from "urijs";
import * as Users from "../services/db/users";
import {IUser} from "../models/user";
import {jwt as JWTConfig} from "../config";
import { ArgumentError, OperationError, handleError } from "../error";

var routes: Hapi.IRouteConfiguration[] = [
    {
        path: "/auth/facebook",
        config: {
            auth: {
                mode: "try",
                strategy: "facebook"
            }
        },
        method: "GET",
        handler: function (request, reply) {
            (async () => {
                var redirect: string = _.get<string>(request.state, "bell-facebook.query.redirect");
                if (!redirect) {
                    throw new ArgumentError("no redirect in query");
                }

                var redirectUri = URI(redirect);

                if (!request.auth.isAuthenticated) {
                    redirectUri.addQuery({
                        error: "Authentication failed: " + request.auth.error.message
                    });

                    return reply.redirect(redirectUri.valueOf());
                }

                var credentials = request.auth.credentials;

                var user = <IUser>_.omit(await Users.insert({
                    email: credentials.profile.email,
                    facebookId: credentials.profile.id,
                    firstName: credentials.profile.name.first,
                    lastName: credentials.profile.name.last,
                    onesignalIds: []
                }), "wishlist", "onesignalIds");

                var token = await signCredentials(user);

                redirectUri.addQuery({
                    token: token
                });

                reply.redirect(redirectUri.valueOf());
            })().catch(error => {
                handleError(reply, error);
            });
        }
    },
    {
        path: "/auth/renew",
        method: "POST",
        handler: function (request, reply) {
            (async () => {
                var token = await signCredentials(request.auth.credentials);
                reply(null, token);
            })().catch(error => {
                handleError(reply, error);
            });
        }
    },
    {
        path: "/auth/status",
        method: "GET",
        config: {
            auth: {
                mode: "try",
                strategy: "jwt"
            }
        },
        handler: function (request, reply) {
            (async () => {
                if (!request.auth.isAuthenticated) {
                    throw new ArgumentError("token is not valid");
                }

                reply(null, request.auth.credentials["exp"]*1000);
            })().catch(error => {
                handleError(reply, error);
            });
        }
    }
];

export = routes;

function signCredentials(credentials) {
    return new Promise(function (resolve) {
        JWT.sign(credentials, JWTConfig.secretKey, {
            algorithm: JWTConfig.algorithm,
            expiresIn: JWTConfig.expiresIn
        }, function (token) {
            resolve(token);
        });
    });
}
