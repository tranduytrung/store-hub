"use strict";

import * as _ from "lodash";
import * as Hapi from "hapi";
import * as Users from "../services/db/users";
import {promisify} from "../utils/promise";
import {ArgumentError, NotFoundError, handleError} from "../error";
import {IWishItem} from "../models/user";

var routes: Hapi.IRouteConfiguration[] = [
    {
        path: "/user",
        method: "GET",
        handler: function(request, reply) {
            (async () => {
                var id : string = request.auth.credentials._id;

                var user = await Users.getById(id);
                if (_.isEmpty(user)) {
                    throw new NotFoundError("no such user");
                }

                reply(null, user);
            })().catch(error => {
                handleError(reply, error);
            });
        }
    },
    {
        path: "/product/count/byProductId",
        config: {
            auth: false
        },
        method: "GET",
        handler: function(request, reply) {
            var countByProductId: any = request.server.methods["users"]["countByProductId"];
            var countByProductIdAsync = promisify<number, string>(countByProductId);

            (async function() {
                let id = request.query.id;
                if (!_.isString(id)) {
                    throw new ArgumentError("id must be a string");
                }
                
                let count = await countByProductIdAsync(id);
                reply(null, count);
            })().catch(error => {
                handleError(reply, error);
            });
        }
    },
    {
        path: "/user/addWishItem",
        method: "POST",
        handler: function(request, reply) {
            (async () => {
                var id : string = request.auth.credentials._id;

                var wishItem: IWishItem = request.payload["wishItem"];
                await Users.addWishItem(id, wishItem);
                reply(null, null);
            })().catch(error => {
                handleError(reply, error);
            });
        }
    },
    {
        path: "/user/updateWishItem",
        method: "POST",
        handler: function(request, reply) {
            (async () => {
                var id : string = request.auth.credentials._id;

                var wishItem = request.payload["wishItem"];
                if (!_.isEmpty(wishItem["nextNotificationDate"])) {
                    wishItem["nextNotificationDate"] = new Date(wishItem["nextNotificationDate"]);
                }               
                 
                await Users.updateWishItem(id, wishItem);
                reply(null, null);
            })().catch(error => {
                handleError(reply, error);
            });
        }
    },
    {
        path: "/user/removeWishItem",
        method: "POST",
        handler: function(request, reply) {
            (async () => {
                var id : string = request.auth.credentials._id;

                var productId: string = request.payload["productId"];
                await Users.removeWishItem(id, productId);
                reply(null, null);
            })().catch(error => {
                handleError(reply, error);
            });
        }
    },
    {
        path: "/user/addOnesignal",
        method: "POST",
        handler: function(request, reply) {
            (async () => {
                var id : string = request.auth.credentials._id;

                var signaloneId: string = request.payload["onesignalId"];
                await Users.addOnesignal(id, signaloneId);
                reply(null, null);
            })().catch(error => {
                handleError(reply, error);
            });
        }
    }
];

export = routes;