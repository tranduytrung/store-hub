import * as Hapi from "hapi"
import * as notification from "../services/notification"
import {OperationError, handleError} from "../error"
import {totalmem, freemem} from "os"

var routes: Hapi.IRouteConfiguration[] = [
    {
        path: "/",
        method: "GET",
        config: {
            auth: {
                mode: "try",
                strategy: "jwt"
            }
        },
        handler: function (request, reply) {
            (async () => {
                reply.view("home");
            })().catch(error => {
                handleError(reply, error);
            });
        }
    },
    {
        path: "/memstat",
        config: {
            auth: false
        },
        method: "GET",
        handler: function (request, reply) {
            (async () => {
                var info = {
                    usage: 1 - freemem() / totalmem(),
                    freemem: freemem(),
                    totalmem: totalmem(),
                    rss: process.memoryUsage().rss
                }
                reply(null, info);
            })().catch(error => {
                handleError(reply, error);
            });
        }
    }
];

export = routes;
