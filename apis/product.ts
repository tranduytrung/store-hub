import * as Products from "../services/db/products"
import {IProduct, IProductGroup, IProductBrief, ProductCategory} from "../models/product"
import * as Hapi from "hapi"
import * as _ from "lodash"
import {promisify} from "../utils/promise"
import {ArgumentError, handleError, NotFoundError, parseNumber, parseString, parseStringArray, parseNumberArray} from "../error"

var routes: Hapi.IRouteConfiguration[] = [
    {
        path: "/product/getNewArrivals",
        config: {
            auth: false
        },
        method: "GET",
        handler: function (request, reply) {
            (async function () {
                var getNewArrivals: any = request.server.methods["products"]["getNewArrivals"];
                var getNewArrivalsAsync = promisify<IProductBrief[], string, number>(getNewArrivals);

                let pageSize = Number(request.query.pageSize) || 20;
                let products = await getNewArrivalsAsync(request.query.lastId || "ffffffffffffffffffffffff", pageSize);
                reply(null, products);
            })().catch(error => {
                handleError(reply, error);
            });
        }
    },
    {
        path: "/product",
        method: "GET",
        config: {
            auth: false
        },
        handler: function (request, reply) {
            (async function () {
                let id = request.query.id;
                let ids = request.query.ids;

                if (!_.isEmpty(id)) {
                    let getProduct: any = request.server.methods["products"]["get"];
                    let getProductAsync = promisify<IProduct, string>(getProduct);

                    let product = await getProductAsync(id);
                    if (!product) {
                        throw new NotFoundError(`not found product with id ${id}`);
                    }

                    reply(null, product);
                } else if (!_.isEmpty(ids)) {
                    if (_.isString(ids)) {
                        ids = [ids];
                    }

                    if (!_.isArray(ids)) {
                        throw new ArgumentError("ids is not an array.");
                    }

                    let products = await Products.getMany(ids);
                    reply(null, products);
                } else {
                    throw new ArgumentError("no id or ids params");
                }
            })().catch(error => {
                handleError(reply, error);
            });
        }
    },
    {
        path: "/product/byUrl",
        method: "GET",
        config: {
            auth: false
        },
        handler: function (request, reply) {
            (async function () {
                let url = request.query.url;

                if (!_.isEmpty(url)) {
                    let getByUrl: any = request.server.methods["products"]["getByUrl"];
                    let getByUrlAsync = promisify<IProductBrief, string>(getByUrl);

                    let product = await getByUrlAsync(url);
                    reply(null, product);
                } else {
                    throw new ArgumentError("no url param");
                }
            })().catch(error => {
                handleError(reply, error);
            });
        }
    },
    {
        path: "/product/byGroupId",
        method: "GET",
        config: {
            auth: false
        },
        handler: function (request, reply) {
            (async function () {
                let id = request.query.id;
                if (_.isEmpty(id)) {
                    throw new ArgumentError("no id param");
                }

                let pageIndex = Number(request.query.pageIndex) || 0;
                if (_.isNaN(pageIndex)) {
                    throw new ArgumentError("pageIndex must be a number.");
                }

                let pageSize = Number(request.query.pageSize) || 20;
                if (_.isNaN(pageSize)) {
                    throw new ArgumentError("pageSize must be a number.");
                }

                let getProductsByGroupId: any = request.server.methods["products"]["getByGroupId"];
                let getProductsByGroupIdAsync = promisify<IProduct[], string, number, number>(getProductsByGroupId);

                let products = await getProductsByGroupIdAsync(id, pageIndex * pageSize, pageSize);
                reply(null, products);
            })().catch(error => {
                handleError(reply, error);
            });
        }
    },
    {
        path: "/product/search",
        method: "GET",
        config: {
            auth: false
        },
        handler: function (request, reply) {
            (async function () {
                var search = <any>request.server.methods["products"]["search"];
                var searchAsync = <(...args) => Promise<IProductGroup[]>>promisify(search);

                let q = parseString("q", request.query.q) || null;
                let limit = parseNumber("limit", request.query.limit);
                let skip = parseNumber("skip", request.query.skip);
                let sort = parseNumber("sort", request.query.sort);
                let sources = parseStringArray("sources", request.query.sources);
                let categories = parseNumberArray("categories", request.query.categories);

                let products = await searchAsync(q, categories, sources, sort, skip, limit);
                reply(null, products);
            })().catch(error => {
                handleError(reply, error);
            });
        }
    }
];

export = routes;