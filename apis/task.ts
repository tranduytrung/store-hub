import * as _ from "lodash"
import * as Hapi from "hapi"
import {ObjectID} from "mongodb"
import {TaskState, ITask} from "../models/task"
import {getEssentialTasks, upsert as updateTask} from "../services/db/tasks"
import { ArgumentError, OperationError, handleError } from "../error"

var routes: Hapi.IRouteConfiguration[] = [
    {
        path: "/task/view",
        method: "GET",
        config: {
            auth: false,
            tags: ["administration"]
        },
        handler: function (request, reply) {
            (async function () {
                var tasks = await getEssentialTasks();
                var context = {
                    tasks,
                    TaskState
                };

                reply.view("task", context);
            })().catch((err) => {
                handleError(reply, err);
            });
        }
    },
    {
        path: "/task/setState",
        method: "POST",
        config: {
            auth: false,
            tags: ["administration"]
        },
        handler: function (request, reply) {
            (async function () {
                var taskId = request.payload.id;
                if (!_.isString(taskId)) {
                    throw new ArgumentError('id must be a string');
                }

                var state = Number(request.payload.state);
                if (isNaN(state)) {
                    throw new ArgumentError("state must be a number");
                }

                await updateTask({
                    _id: new ObjectID(taskId),
                    executor: undefined,
                    params: undefined,
                    nextRun: state === TaskState.scheduled? new Date() : undefined,
                    state: state,
                    assignee: undefined
                });
                
                reply.redirect("/task/view");
            })().catch((err) => {
                handleError(reply, err);
            });
        }
    }
];

export = routes;