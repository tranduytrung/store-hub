"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const Products = require("../services/db/products");
const _ = require("lodash");
const promise_1 = require("../utils/promise");
const error_1 = require("../error");
var routes = [
    {
        path: "/product/getNewArrivals",
        config: {
            auth: false
        },
        method: "GET",
        handler: function (request, reply) {
            (function () {
                return __awaiter(this, void 0, void 0, function* () {
                    var getNewArrivals = request.server.methods["products"]["getNewArrivals"];
                    var getNewArrivalsAsync = promise_1.promisify(getNewArrivals);
                    let pageSize = Number(request.query.pageSize) || 20;
                    let products = yield getNewArrivalsAsync(request.query.lastId || "ffffffffffffffffffffffff", pageSize);
                    reply(null, products);
                });
            })().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    },
    {
        path: "/product",
        method: "GET",
        config: {
            auth: false
        },
        handler: function (request, reply) {
            (function () {
                return __awaiter(this, void 0, void 0, function* () {
                    let id = request.query.id;
                    let ids = request.query.ids;
                    if (!_.isEmpty(id)) {
                        let getProduct = request.server.methods["products"]["get"];
                        let getProductAsync = promise_1.promisify(getProduct);
                        let product = yield getProductAsync(id);
                        if (!product) {
                            throw new error_1.NotFoundError(`not found product with id ${id}`);
                        }
                        reply(null, product);
                    }
                    else if (!_.isEmpty(ids)) {
                        if (_.isString(ids)) {
                            ids = [ids];
                        }
                        if (!_.isArray(ids)) {
                            throw new error_1.ArgumentError("ids is not an array.");
                        }
                        let products = yield Products.getMany(ids);
                        reply(null, products);
                    }
                    else {
                        throw new error_1.ArgumentError("no id or ids params");
                    }
                });
            })().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    },
    {
        path: "/product/byUrl",
        method: "GET",
        config: {
            auth: false
        },
        handler: function (request, reply) {
            (function () {
                return __awaiter(this, void 0, void 0, function* () {
                    let url = request.query.url;
                    if (!_.isEmpty(url)) {
                        let getByUrl = request.server.methods["products"]["getByUrl"];
                        let getByUrlAsync = promise_1.promisify(getByUrl);
                        let product = yield getByUrlAsync(url);
                        reply(null, product);
                    }
                    else {
                        throw new error_1.ArgumentError("no url param");
                    }
                });
            })().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    },
    {
        path: "/product/byGroupId",
        method: "GET",
        config: {
            auth: false
        },
        handler: function (request, reply) {
            (function () {
                return __awaiter(this, void 0, void 0, function* () {
                    let id = request.query.id;
                    if (_.isEmpty(id)) {
                        throw new error_1.ArgumentError("no id param");
                    }
                    let pageIndex = Number(request.query.pageIndex) || 0;
                    if (_.isNaN(pageIndex)) {
                        throw new error_1.ArgumentError("pageIndex must be a number.");
                    }
                    let pageSize = Number(request.query.pageSize) || 20;
                    if (_.isNaN(pageSize)) {
                        throw new error_1.ArgumentError("pageSize must be a number.");
                    }
                    let getProductsByGroupId = request.server.methods["products"]["getByGroupId"];
                    let getProductsByGroupIdAsync = promise_1.promisify(getProductsByGroupId);
                    let products = yield getProductsByGroupIdAsync(id, pageIndex * pageSize, pageSize);
                    reply(null, products);
                });
            })().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    },
    {
        path: "/product/search",
        method: "GET",
        config: {
            auth: false
        },
        handler: function (request, reply) {
            (function () {
                return __awaiter(this, void 0, void 0, function* () {
                    var search = request.server.methods["products"]["search"];
                    var searchAsync = promise_1.promisify(search);
                    let q = error_1.parseString("q", request.query.q) || null;
                    let limit = error_1.parseNumber("limit", request.query.limit);
                    let skip = error_1.parseNumber("skip", request.query.skip);
                    let sort = error_1.parseNumber("sort", request.query.sort);
                    let sources = error_1.parseStringArray("sources", request.query.sources);
                    let categories = error_1.parseNumberArray("categories", request.query.categories);
                    let products = yield searchAsync(q, categories, sources, sort, skip, limit);
                    reply(null, products);
                });
            })().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    }
];
module.exports = routes;
//# sourceMappingURL=product.js.map