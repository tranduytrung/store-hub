import * as _ from "lodash"
import * as Hapi from "hapi"
import {ObjectID} from "mongodb"
import {TaskState} from "../models/task"
import * as Crawler from "../services/crawler"
import { ArgumentError, OperationError, handleError } from "../error"

var routes: Hapi.IRouteConfiguration[] = [
    {
        path: "/thegioididong/getProduct",
        method: "GET",
        config: {
            auth: false,
            tags: ["administration"]
        },
        handler: function(request, reply) {
            (async function() {
                var url = request.query.url;
                if (!url) {
                    throw new ArgumentError('url is not provided');
                }

                var product = await Crawler.getProductFromThegioididong(url);
                reply(null, product);
            })().catch((err) => {
                handleError(reply, err);
            });
        }
    },
    {
        path: "/fptshop/getProduct",
        method: "GET",
        config: {
            auth: false,
            tags: ["administration"]
        },
        handler: function(request, reply) {
            (async function() {
                var url = request.query.url;
                if (!url) {
                    throw new ArgumentError('url is not provided');
                }

                var product = await Crawler.getProductFromFptshop(url);
                reply(null, product);
            })().catch((err) => {
                handleError(reply, err);
            });
        }
    },
    {
        path: "/tiki/getProduct",
        method: "GET",
        config: {
            auth: false,
            tags: ["administration"]
        },
        handler: function(request, reply) {
            (async function() {
                var url = request.query.url;
                if (!url) {
                    throw new ArgumentError('url is not provided');
                }

                var product = await Crawler.getProductFromTiki(url);
                reply(null, product);
            })().catch((err) => {
                handleError(reply, err);
            });
        }
    },
    {
        path: "/lazada/getProduct",
        method: "GET",
        config: {
            auth: false,
            tags: ["administration"]
        },
        handler: function(request, reply) {
            (async function() {
                var url = request.query.url;
                if (!url) {
                    throw new ArgumentError('url is not provided');
                }

                var product = await Crawler.getProductFromLazada(url);
                reply(null, product);
            })().catch((err) => {
                handleError(reply, err);
            });
        }
    },
    {
        path: "/thegioididong/getAds",
        method: "GET",
        config: {
            auth: false,
            tags: ["administration"]
        },
        handler: function(request, reply) {
            (async function() {
                var ads = await Crawler.getAdsFromThegioididong();
                
                reply(null, ads);
            })().catch((err) => {
                handleError(reply, err);
            });
        }
    },
    {
        path: "/fptshop/getAds",
        method: "GET",
        config: {
            auth: false,
            tags: ["administration"]
        },
        handler: function(request, reply) {
            (async function() {
                var ads = await Crawler.getAdsFromFptshop();
                
                reply(null, ads);
            })().catch((err) => {
                handleError(reply, err);
            });
        }
    },
    {
        path: "/tiki/getAds",
        method: "GET",
        config: {
            auth: false,
            tags: ["administration"]
        },
        handler: function(request, reply) {
            (async function() {
                var ads = await Crawler.getAdsFromTiki();
                
                reply(null, ads);
            })().catch((err) => {
                handleError(reply, err);
            });
        }
    }
];

export = routes;