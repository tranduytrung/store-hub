"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const promise_1 = require("../utils/promise");
const error_1 = require("../error");
var routes = [
    {
        path: "/ad/getSourceEnumeration",
        config: {
            auth: false
        },
        method: "GET",
        handler: function (request, reply) {
            (function () {
                return __awaiter(this, void 0, void 0, function* () {
                    var getSourceEnumeration = request.server.methods["ad"]["getSourceEnumeration"];
                    var getSourceEnumerationAsync = promise_1.promisify(getSourceEnumeration);
                    let sources = yield getSourceEnumerationAsync();
                    reply(null, sources);
                });
            })().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    },
    {
        path: "/ad/bySource",
        config: {
            auth: false
        },
        method: "GET",
        handler: function (request, reply) {
            (function () {
                return __awaiter(this, void 0, void 0, function* () {
                    var getBySource = request.server.methods["ad"]["getBySource"];
                    var getBySourceAsync = promise_1.promisify(getBySource);
                    let source = request.query.source;
                    let ads = yield getBySourceAsync(source ? source : "");
                    reply(null, ads);
                });
            })().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    }
];
module.exports = routes;
//# sourceMappingURL=ad.js.map