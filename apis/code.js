"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const _ = require("lodash");
const error_1 = require("../error");
var codes = [];
var routes = [
    {
        path: "/code/redeem",
        method: "GET",
        config: {
            auth: false
        },
        handler: function (request, reply) {
            (() => __awaiter(this, void 0, void 0, function* () {
                if (codes.length > 0) {
                    var code = codes.shift();
                    reply.redirect(`http://go.microsoft.com/fwlink/?LinkId=532540&mstoken=${code}`);
                }
                else {
                    reply(null, "all codes have been used");
                }
            }))().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    },
    {
        path: "/code",
        config: {
            auth: false
        },
        method: "GET",
        handler: function (request, reply) {
            (() => __awaiter(this, void 0, void 0, function* () {
                reply.view("code", { codes: codes });
            }))().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    },
    {
        path: "/code",
        config: {
            auth: false
        },
        method: "POST",
        handler: function (request, reply) {
            (() => __awaiter(this, void 0, void 0, function* () {
                var str = request.payload.codes;
                codes = _.filter(str.split("\r\n"), _.negate(_.isEmpty));
                reply.redirect("/code");
            }))().catch(error => {
                error_1.handleError(reply, error);
            });
        }
    }
];
module.exports = routes;
//# sourceMappingURL=code.js.map