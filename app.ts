import * as Hapi from "hapi";
import * as Db from "./services/db/db";
import * as CrawlerCore from "./services/crawler/core";
import * as PromiseUtility from "./utils/promise";
import * as config from "./config";
import {Bootstrap, BootstrapMode} from "./utils/bootstrap";
import {getArgs} from "./utils/parseArgv";
import * as winston from "winston";

if (process.env.NODE_ENV !== "development") {
    require('winston-loggly');
    winston.add(winston.transports.Loggly, {
        level: "verbose",
        token: "31d3a055-3b8c-464c-9d37-79c97e941e76",
        subdomain: "tranduytrung",
        tags: ["store-hub"],
        json: true
    });
} else {
    winston.add(winston.transports.File, {
        level: "verbose",
        filename: "store-hub.log.json"
    });
}

var args = getArgs();
var modeArg: string = args["mode"];
var mode: BootstrapMode = BootstrapMode[process.env.storehub_mode] || BootstrapMode[modeArg] || BootstrapMode.Full;

async function initialize(): Promise<Hapi.Server> {
    await Db.initialize();

    let server = new Hapi.Server();

    server.connection(config.server.connection);

    await server.register({
        register: require('good'),
        options: {
            reporters: {
                consoleReporter: [{
                    module: "good-console"
                }]
            }
        }
    });

    // bell auth
    await server.register(require("bell"));
    server.auth.strategy('facebook', 'bell', false, {
        provider: 'facebook',
        password: config.cookieSecret,
        clientId: config.facebook.clientId,
        clientSecret: config.facebook.clientSecret,
        isSecure: false,     // Terrible idea but required if not using HTTPS especially if developing locally
        scope: config.facebook.scope
    });

    // jwt auth
    await server.register(require("hapi-auth-jwt2"));
    server.auth.strategy("jwt", "jwt", "required", {
        validateFunc: function (decoded, request, callback) {
            callback(null, true);
        },
        key: config.jwt.secretKey,
        verifyOptions: { algorithms: [config.jwt.algorithm] }
    });

    await Bootstrap.registerAPIs(server, mode);
    await Bootstrap.registerMethods(server);

    if (mode & BootstrapMode.Administration) {
        await CrawlerCore.init();
        await require("./services/task").initialize();
    }

    await server.register(require("vision"));
    server.views({
        engines: { jade: require("jade") },
        path: "static/templates"
    });

    await server.start();
    return server;
}

initialize().then((server) => {
    winston.info(`Server started`, { source: "server", mode: BootstrapMode[mode] });
}).catch((err) => {
    winston.error(`Server error on starting`, err);
});

process.on('uncaughtException', (err) => {
    winston.error(`Caught exception: ${err}`);
});