"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const Db = require("./services/db/db");
function go() {
    return __awaiter(this, void 0, void 0, function* () {
        yield Db.initialize();
        var Products = require("./services/db/products");
        var Notification = require("./services/notification");
        var product = yield Products.get("57ad63c1418c6bcc36860564");
        yield Notification.broadcastProductUpdate(product, ["5788f309f79408de06fcdb3a"]);
    });
}
go().then(function () {
    console.log("done");
}).catch(function (e) {
    console.error(e);
});
//# sourceMappingURL=send.js.map