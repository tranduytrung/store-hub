import * as Hapi from "hapi";
import * as Users from "../services/db/users";

var methods : {name: string; method: Hapi.IServerMethod; options?: Hapi.IServerMethodOptions}[] = [
    {
        name: "users.countByProductId",
        method: function(productId : string) {
            return Users.countByProductId(productId);
        },
        options: {
            callback: false,
            cache: {
                expiresIn: 86400000, // 1 day
                generateTimeout: 20000 // 20 seconds
            }
        }
    }
];

export = methods;