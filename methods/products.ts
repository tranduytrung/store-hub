import {IServerMethod, IServerMethodOptions} from "hapi"
import {diffInMiliseconds} from "../utils/date"
import {generateMethodKey} from "../utils/generator"
import * as Products from "../services/db/products"
import * as Crawl from "../services/crawler"
import * as _ from "lodash"
import {MediaType, IPricePoint, IProduct, IProductBrief, IProductGroup, IProductMedia, ProductCategory, ProductSortType} from "../models/product"

var methods: { name: string; method: IServerMethod; options?: IServerMethodOptions }[] = [
    {
        name: "products.getNewArrivals",
        method: function (lastId: string, pageSize: number) {
            return Products.getNewArrivals(lastId, pageSize);
        },
        options: {
            callback: false,
            cache: {
                expiresIn: 3600000, // 1h
                generateTimeout: 20000 // 20 seconds
            }
        }
    },
    {
        name: "products.get",
        method: function (id: string) {
            return Products.get(id);
        },
        options: {
            callback: false,
            cache: {
                expiresIn: 60000, // 60 seconds
                generateTimeout: 20000 // 20 seconds
            }
        }
    },
    {
        name: "products.getByGroupId",
        method: function (id: string, skip: number, limit: number) {
            return Products.getProductsByGroupId(id, skip, limit);
        },
        options: {
            callback: false,
            cache: {
                expiresIn: 60000, // 60 seconds
                generateTimeout: 20000 // 20 seconds
            }
        }
    },
    {
        name: "products.getByUrl",
        method: async function (url: string) {
            var product = await Products.getByUrl(url);
            if (!(product && diffInMiliseconds(product.updatedDate))) {
                let id = await Products.upsert(await Crawl.getProduct(url));
                product = await Products.getBrief(id);
            }

            return product;
        },
        options: {
            callback: false,
            cache: {
                expiresIn: 60000, // 60 seconds
                generateTimeout: 20000 // 20 seconds
            }
        }
    },
    {
        name: "products.search",
        method: function (text: string, categories, sources, sort, skip, limit) {
            return Products.search(text, {
                categories,
                limit: limit,
                skip: skip,
                sort: sort,
                sources: sources
            });
        },
        options: {
            callback: false,
            generateKey: generateMethodKey,
            cache: {
                expiresIn: 3600000, // 60 seconds
                generateTimeout: 20000 // 20 seconds
            }
        }
    }
];

export = methods;