import * as Hapi from "hapi";
import * as Ad from "../services/db/ad";

var methods : {name: string; method: Hapi.IServerMethod; options?: Hapi.IServerMethodOptions}[] = [
    {
        name: "ad.getSourceEnumeration",
        method: function() {
            return Ad.getSources();
        },
        options: {
            callback: false,
            cache: {
                expiresIn: 360000, // 1 hour
                generateTimeout: 20000 // 20 seconds
            }
        }
    },
    {
        name: "ad.getBySource",
        method: function(source : string) {
            return Ad.getBySource(source);
        },
        options: {
            callback: false,
            cache: {
                expiresIn: 360000, // 1 hour
                generateTimeout: 20000 // 20 seconds
            }
        }
    }
];

export = methods;