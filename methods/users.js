"use strict";
const Users = require("../services/db/users");
var methods = [
    {
        name: "users.countByProductId",
        method: function (productId) {
            return Users.countByProductId(productId);
        },
        options: {
            callback: false,
            cache: {
                expiresIn: 86400000,
                generateTimeout: 20000 // 20 seconds
            }
        }
    }
];
module.exports = methods;
//# sourceMappingURL=users.js.map