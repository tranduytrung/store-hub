"use strict";
const Ad = require("../services/db/ad");
var methods = [
    {
        name: "ad.getSourceEnumeration",
        method: function () {
            return Ad.getSources();
        },
        options: {
            callback: false,
            cache: {
                expiresIn: 360000,
                generateTimeout: 20000 // 20 seconds
            }
        }
    },
    {
        name: "ad.getBySource",
        method: function (source) {
            return Ad.getBySource(source);
        },
        options: {
            callback: false,
            cache: {
                expiresIn: 360000,
                generateTimeout: 20000 // 20 seconds
            }
        }
    }
];
module.exports = methods;
//# sourceMappingURL=ad.js.map