"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const date_1 = require("../utils/date");
const generator_1 = require("../utils/generator");
const Products = require("../services/db/products");
const Crawl = require("../services/crawler");
var methods = [
    {
        name: "products.getNewArrivals",
        method: function (lastId, pageSize) {
            return Products.getNewArrivals(lastId, pageSize);
        },
        options: {
            callback: false,
            cache: {
                expiresIn: 3600000,
                generateTimeout: 20000 // 20 seconds
            }
        }
    },
    {
        name: "products.get",
        method: function (id) {
            return Products.get(id);
        },
        options: {
            callback: false,
            cache: {
                expiresIn: 60000,
                generateTimeout: 20000 // 20 seconds
            }
        }
    },
    {
        name: "products.getByGroupId",
        method: function (id, skip, limit) {
            return Products.getProductsByGroupId(id, skip, limit);
        },
        options: {
            callback: false,
            cache: {
                expiresIn: 60000,
                generateTimeout: 20000 // 20 seconds
            }
        }
    },
    {
        name: "products.getByUrl",
        method: function (url) {
            return __awaiter(this, void 0, void 0, function* () {
                var product = yield Products.getByUrl(url);
                if (!(product && date_1.diffInMiliseconds(product.updatedDate))) {
                    let id = yield Products.upsert(yield Crawl.getProduct(url));
                    product = yield Products.getBrief(id);
                }
                return product;
            });
        },
        options: {
            callback: false,
            cache: {
                expiresIn: 60000,
                generateTimeout: 20000 // 20 seconds
            }
        }
    },
    {
        name: "products.search",
        method: function (text, categories, sources, sort, skip, limit) {
            return Products.search(text, {
                categories: categories,
                limit: limit,
                skip: skip,
                sort: sort,
                sources: sources
            });
        },
        options: {
            callback: false,
            generateKey: generator_1.generateMethodKey,
            cache: {
                expiresIn: 3600000,
                generateTimeout: 20000 // 20 seconds
            }
        }
    }
];
module.exports = methods;
//# sourceMappingURL=products.js.map