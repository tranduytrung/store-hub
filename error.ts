import * as _ from "lodash"
import * as Boom from "boom"
import { IReply } from "hapi"
import * as winston from "winston"

/**
* NotFoundError
* Represent no data is found
*/
export class NotFoundError extends Error {
    constructor(message: string) {
        super(message)
    }
}

/**
* ArgumentError
* Represent invalid argument passed into a function
*/
export class ArgumentError extends Error {
    constructor(message: string) {
        super(message)
    }
}

/**
 * OperationError
 * Error while operation is processing
 */
export class OperationError extends Error {
    constructor(message: string) {
        super(message)
    }
}

export function handleError(reply: IReply, error: any) {
    if (error.isBoom) {
        return reply(error);
    }

    var instanceName = error.constructor.name;
    switch (instanceName) {
        case "NotFoundError":
            reply(Boom.notFound(error.message));
            break;
        case "ArgumentError":
            reply(Boom.badData(error.message));
            break;
        case "OperationError":
            reply(Boom.expectationFailed(error.message));
            break;
        case "String":
            reply(Boom.badImplementation(error.message));
            break;
        default:
            reply(Boom.badImplementation("unexpected error", error));
            break;
    }
    winston.error(error);
}

export function parseNumber(name: string, value: string, required = false) {
    if (_.isNil(value) && !required) return undefined;
    if (_.isString(value)) {
        var n = Number(value);
        if (!isNaN(n)) return n;
    }

    throw new ArgumentError(`Invalid data type. ${name} must be a number`);
}

export function parseString(name: string, value: string, required = false) {
    if (_.isNil(value) && !required) return undefined;
    if (_.isString(value)) {
        return value;
    }

    throw new ArgumentError(`Invalid data type. ${name} must be a string`);
}

export function parseStringArray(name: string, value: any, required = false) : string[] {
    if (_.isNil(value) && !required) return undefined;
    if (_.isString(value)) {
        return [value];
    }

    if (_.isArray(value)) {
        var index = 0;
        while (index < value.length) {
            var element = value[index];
            if (!_.isString(element)) break;
            index++;
        }

        if (index === value.length) {
            return value;
        }
    }

    throw new ArgumentError(`Invalid data type. ${name} must be a string array`);
}

export function parseNumberArray(name: string, value: any, required = false) : number[] {
    if (_.isNil(value) && !required) return undefined;
    if (_.isString(value)) {
        var num = Number(value);
        if (!isNaN(num))
            return [num];
    }

    if (_.isArray(value)) {
        var index = 0;
        var numArray = [];
        while (index < value.length) {
            var element = Number(value[index]);
            if (isNaN(element)) break;
            numArray.push(element);
            index++;
        }

        if (index === value.length) {
            return value;
        }
    }

    throw new ArgumentError(`Invalid data type. ${name} must be a number array`);
}