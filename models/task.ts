
import {ObjectID} from "mongodb"

export enum TaskState {
    scheduled,
    running,
    suspended,
    completed,
    error
}

export interface ITask {
    _id: ObjectID
    state: TaskState
    lastRun?: Date
    nextRun: Date
    executor: string
    params: any[],
    assignee: string
}