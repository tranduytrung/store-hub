"use strict";
(function (TaskState) {
    TaskState[TaskState["scheduled"] = 0] = "scheduled";
    TaskState[TaskState["running"] = 1] = "running";
    TaskState[TaskState["suspended"] = 2] = "suspended";
    TaskState[TaskState["completed"] = 3] = "completed";
    TaskState[TaskState["error"] = 4] = "error";
})(exports.TaskState || (exports.TaskState = {}));
var TaskState = exports.TaskState;
//# sourceMappingURL=task.js.map