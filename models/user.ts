import {ObjectID} from "mongodb"

export enum ConditionalVerbs {
    Below,
    ReducedBy
}

export interface ITrack {
    cycle: number
    conditionalVerb: ConditionalVerbs
    thresholdValue: number
    nextNotificationDate: Date
}

export interface IWishItem {
    productId: ObjectID
    track?: ITrack
}

export interface IUser {
    firstName?: string
    lastName?: string
    email: string
    wishlist?: IWishItem[]
    facebookId?: string
    googleId?: string
    onesignalIds: string[]
}
