import {ObjectID} from "mongodb"

export interface IAd {
    _id? : ObjectID,
    url : string,
    source : string,
    image : string
}