"use strict";
(function (ProductCategory) {
    ProductCategory[ProductCategory["other"] = 0] = "other";
    ProductCategory[ProductCategory["phone"] = 1] = "phone";
    ProductCategory[ProductCategory["tablet"] = 2] = "tablet";
    ProductCategory[ProductCategory["laptop"] = 3] = "laptop";
})(exports.ProductCategory || (exports.ProductCategory = {}));
var ProductCategory = exports.ProductCategory;
(function (MediaType) {
    MediaType[MediaType["other"] = 0] = "other";
    MediaType[MediaType["thumbnail"] = 1] = "thumbnail";
    MediaType[MediaType["image"] = 2] = "image";
    MediaType[MediaType["video"] = 3] = "video";
})(exports.MediaType || (exports.MediaType = {}));
var MediaType = exports.MediaType;
(function (ProductSortType) {
    ProductSortType[ProductSortType["relevance"] = 0] = "relevance";
    ProductSortType[ProductSortType["priceAsc"] = 1] = "priceAsc";
    ProductSortType[ProductSortType["priceDesc"] = 2] = "priceDesc";
    ProductSortType[ProductSortType["newest"] = 3] = "newest";
    ProductSortType[ProductSortType["oldest"] = 4] = "oldest";
})(exports.ProductSortType || (exports.ProductSortType = {}));
var ProductSortType = exports.ProductSortType;
//# sourceMappingURL=product.js.map