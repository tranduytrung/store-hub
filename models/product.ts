import {ObjectID} from "mongodb"

export enum ProductCategory {
    other, phone, tablet, laptop
}

export enum MediaType {
    other, thumbnail, image, video
}

export interface IProductMedia {
    url: string
    type: MediaType
}

export interface IPricePoint {
    date: Date
    value: number
}

export interface IProductBrief {
    _id: string
    source: string
    name: string
    url: string
    price: number
    priceUnit: string
    media: IProductMedia[]
    category: ProductCategory
    groupId: string
    updatedDate: Date
    deletedDate: Date
}

export interface IProduct {
    _id?: ObjectID
    groupId: ObjectID
    source: string
    name: string
    url: string
    prices: IPricePoint[]
    priceUnit: string
    media: IProductMedia[]
    category: ProductCategory
    description: string
    specification: Object
    createdDate: Date
    updatedDate: Date
    deletedDate?: Date
}

export interface IProductGroup {
    _id: string,
    products: IProductBrief[]
}

export enum ProductSortType {
    relevance, priceAsc, priceDesc, newest, oldest
}