import {MongoClient, Db} from "mongodb"
import {IProduct} from "./models/product"
import * as _ from "lodash"

async function run() {
    var db = await MongoClient.connect("mongodb://tranduytrung:sbcbE045d17@ds011168.mlab.com:11168/storehub");
    var collection = db.collection("products");

    var query = {
        source: "tiki",
        url: /^https/i
    }

    var httpsProducts: IProduct[] = await collection.find(query).toArray();
    for (var index = 0; index < httpsProducts.length; index++) {
        var httpsProduct = httpsProducts[index];
        var result = await collection.findOneAndDelete({ url: "http" + httpsProduct.url.substring(5) });
        if (!result.value) continue;

        var httpProduct: IProduct = result.value;
        console.log(httpProduct.name);
        var prices = _.orderBy(httpsProduct.prices.concat(httpProduct.prices), ["date"], ["desc"]);
        collection.updateOne({ _id: httpsProduct["_id"] }, { $set: { prices } });
    }
}

run().then(function() {
    console.log("done");
}).catch(function(e) {
    console.error(e);
});